from optparse import OptionParser
from scipy.signal import hilbert
import HelperFunctions as rdhelp
from CSTransformation import CSTransformation
import numpy as np
import cPickle
import re
import os

"""
This tool parses the ZHaireS text output and fills it into python objects.
Each event will be one stored in one pickle file containing an zhaires_event object.
This object contains the following members:
zhaires_event.primParticle
zhaires_event.zenith
zhaires_event.azimuth
zhaires_event.xmax
zhaires_event.energy
zhaires_event.antenna_position
zhaires_event.power
zhaires_event.magnetic_field_inclination
zhaires_event.magnetic_field_declination
zhaires_event.magnetic_field_strength
zhaires_event.n
zhaires_event.E_gamma
zhaires_event.E_ep_ion
zhaires_event.E_ep_cut
zhaires_event.E_emag
zhaires_event.antenna_position
zhaires_event.energy_density
zhaires_event.injection_height
zhaires_event.obs_level
"""

conversion_fieldstrength_cgs_to_SI = 2.99792458e4
conversion_factor_integrated_signal = 2.65441729e-3 * 6.24150934e18

class zhaires_sim():

    """ Class that stores all zhaires event information """

    def __init__(self, eventid):
        self.id = eventid


def parse_sry_file(filename):
    lines = open(filename, 'r').readlines()

    start_index = lines.index(">>>>          PER SHOWER BALANCE OF ENERGY\n")
    end_index = lines.index(">>>>          PARTICLES REACHING GROUND LEVEL\n")

    prim_energy = float(
        re.search(r"\((.*)\seV\)", lines[start_index + 4]).group(1))

    regex = r"^.*:\s*(\d\.\d*)\s*.*"  # find first digit (mean) of a line
    for i in range(start_index, end_index):
        if "Stack 1" in lines[i]:
            gamma = float(
                re.search(regex, lines[i + 1]).group(1)) * prim_energy
            gamma += float(
                re.search(regex, lines[i + 2]).group(1)) * prim_energy
            gamma += float(
                re.search(regex, lines[i + 3]).group(1)) * prim_energy
            gamma += float(
                re.search(regex, lines[i + 4]).group(1)) * prim_energy
        if "Stack 2" in lines[i]:
            ep_ion = float(
                re.search(regex, lines[i + 1]).group(1)) * prim_energy
            ep_ion += float(
                re.search(regex, lines[i + 2]).group(1)) * prim_energy
            ep_cut = float(
                re.search(regex, lines[i + 3]).group(1)) * prim_energy
            ep_cut += float(
                re.search(regex, lines[i + 4]).group(1)) * prim_energy

    for line in lines:
        match = re.search(
            r"Sl\.\sdepth\sof\smax\.\s\(g/cm2\):\s*(\d*\.\d*)\s*.", line)
        if match:
            xmax = float(match.group(1))

    return prim_energy, gamma, ep_ion, ep_cut, xmax


def ProcessData(indir, outdir, evtnumber):
    pickname = os.path.join(outdir, "event%i" % evtnumber)
    if os.path.exists(pickname):
        print "%s already exits, skipping event" % pickname
        return

    lgfname = os.path.join(indir, "Event%i.lgf" % evtnumber)
    flgf = open(lgfname, 'r')
    lgflines = flgf.readlines()
    flgf.close()

    event = 0
    zhaires_event = zhaires_sim(event)

    # get infos from the log file
    for line in lgflines:
        match = re.search(r"Primary\sparticle:\s*(.*)\s*", line)
        if match:
            zhaires_event.primParticle = match.group(1)

        match = re.search(r"Primary\szenith\sangle:\s*(\d*\.\d*)\sdeg", line)
        if match:
            zhaires_event.zenith = np.deg2rad(float(match.group(1)))

        match = re.search(r"Primary\sazimuth\sangle:\s*(-?\d*\.\d*)\sdeg", line)
        if match:
            zhaires_event.azimuth = np.deg2rad(float(match.group(1)) + 90)

        #match = re.search(r"Primary\senergy:\s*(\d*\.\d*)\sEeV", line)
        #if match:
        #    zhaires_event.energy = float(match.group(1))

        match = re.search(r"Intensity:\s*(\d*\.\d*)\s*uT", line)
        if match:
            zhaires_event.magnetic_field_strength = float(match.group(1))

        match = re.search(r"I:\s*([-]\d*\.\d*)\s*deg", line)
        if match:
            zhaires_event.magnetic_field_inclination = np.deg2rad(
                float(match.group(1)))

        match = re.search(r"D:\s*(\d*\.\d*)\s*deg", line)
        if match:
            zhaires_event.magnetic_field_declination = np.deg2rad(
                float(match.group(1)))

        match = re.search(
            r"Refraction\sindex\sat\ssea\slevel:\s*(\d\.\d*)\s", line)
        if match:
            zhaires_event.n = float(match.group(1))

        match = re.search(
            r"Ground\saltitude:\s*(\d\.\d*)\s", line)
        if match:
            zhaires_event.obs_level = float(match.group(1))

        match = re.search(r"Injection\saltitude:\s*(\d*\.\d*)\s", line)
        if match:
            zhaires_event.injection_height = float(match.group(1)) * 1000

    # E_emag and xmax from summery file
    sryname = os.path.join(indir, "Event%i.sry" % evtnumber)
    zhaires_event.energy, zhaires_event.E_gamma, zhaires_event.E_ep_ion, zhaires_event.E_ep_cut, zhaires_event.xmax = parse_sry_file(sryname)
    zhaires_event.E_emag = zhaires_event.E_gamma + \
        zhaires_event.E_ep_ion + zhaires_event.E_ep_cut
    """
  print zhaires_event.zenith, zhaires_event.azimuth, zhaires_event.energy
  print zhaires_event.magnetic_field_strength, zhaires_event.magnetic_field_inclination, zhaires_event.magnetic_field_declination
  print zhaires_event.n
  print zhaires_event.E_gamma, zhaires_event.E_ep_ion, zhaires_event.E_ep_cut, zhaires_event.E_emag
  print zhaires_event.xmax
  print zhaires_event.injection_height
  """

    # antenna infos ...
    lowco = 30
    hico = 80

    fresnelfile = os.path.join(indir, "timefresnel-root.dat")
    Alldata = np.loadtxt(
        fresnelfile, skiprows=19, usecols=(1, 2, 3, 4, 5, 11, 12, 13))

    """
  Alldata cols
   0            | 1| 2| 3        | 4        | 5 | 6 | 7
  antenna number, x, y, z pos [m], time [ns], Ex, Ey, Ez [V/m]
  """

    nantennas = int(Alldata[-1, 0])
    antenna_start_index = np.zeros(nantennas)
    for i in range(Alldata.shape[0] - 1):
        if Alldata[i, 0] != Alldata[i + 1, 0]:
            antenna_start_index[int(Alldata[i + 1, 0] - 1)] = int(i + 1)

    # convert to AUGER coordinates (AUGER y = x, AUGER x = - y
    temp = np.copy(Alldata)
    Alldata[:, 1], Alldata[:, 2] = -temp[:, 2], temp[:, 1]
    Alldata[:,5], Alldata[:,6], Alldata[:,7] = -temp[:,6], temp[:,5], -temp[:,7]

    magnetic_field_vector = rdhelp.SphericalToCartesian(np.pi * 0.5 + zhaires_event.magnetic_field_inclination, zhaires_event.magnetic_field_declination + np.pi * 0.5)
    ctrans = CSTransformation(zhaires_event.zenith, zhaires_event.azimuth, magnetic_field=magnetic_field_vector)

    antenna_position = np.zeros([nantennas, 3])
    power = np.zeros([nantennas, 3])
    peak_time = np.zeros([nantennas, 3])
    peak_amplitude = np.zeros([nantennas, 3])
    peak_amplitude_total = np.zeros(nantennas)
    power_showerplane = np.zeros([nantennas, 3])

    for j in np.arange(nantennas):
        start_index = int(antenna_start_index[j])
        if j != nantennas - 1:
            end_index = int(antenna_start_index[j + 1])
        else:
            end_index = Alldata.shape[0]

        x, y, z = Alldata[start_index, 1], Alldata[
            start_index, 2], Alldata[start_index, 3]
        antenna_position[j] = [x, y, -(z - zhaires_event.injection_height)]

        data = np.zeros([end_index - start_index, 4])
        data = Alldata[start_index:end_index, 4:8]
        data[:, 0] *= 1E-9  # ns to s

        dlength = data.shape[0]
        spec = np.fft.rfft(data[:, 1:4], axis=-2)

        tstep = data[1, 0] - data[0, 0]
        freqhi = 0.5 / tstep / 1e6  # MHz
        freqstep = freqhi / (dlength / 2 + 1)  # MHz

        fb = int(np.floor(lowco / freqstep))
        lb = int(np.floor(hico / freqstep) + 1)
        window = np.zeros([1, dlength / 2 + 1, 1])
        window[0, fb:lb + 1, 0] = 1
        # correct normalization of fft
        pow0 = np.abs(spec[:, 0]) * np.abs(spec[:, 0]) / dlength * 2
        pow1 = np.abs(spec[:, 1]) * np.abs(spec[:, 1]) / dlength * 2
        pow2 = np.abs(spec[:, 2]) * np.abs(spec[:, 2]) / dlength * 2
        power[j] = np.array([np.sum(pow0[fb:lb + 1]), np.sum(pow1[fb:lb + 1]), np.sum(pow2[fb:lb + 1])]) * \
            tstep * conversion_factor_integrated_signal

        filtered_spec = np.array([spec[0:dlength, 0] * window[0, 0:dlength, 0], spec[
            0:dlength, 1] * window[0, 0:dlength, 0], spec[0:dlength, 2] * window[0, 0:dlength, 0]])
        filt = np.fft.irfft(filtered_spec, axis=-1)

        # roll trace to have its maximum in the middle. Due to the bandpass
        # filter, the pulse extends to the end of the trace
        tt = data[:, 0] * 1e9
        filt = np.roll(filt, np.int(0.5 * len(tt)), axis=-1)
        hilbenv = np.abs(hilbert(filt, axis=-1))
        peak_time[j] = np.argmax(hilbenv, axis=-1)
        peak_amplitude[j] = np.max(hilbenv, axis=-1)
        peak_amplitude_total[j] = np.sum(peak_amplitude[j] ** 2) ** 0.5
        peak_time_sum = tt[np.argmax(np.sum(hilbenv ** 2, axis=0) ** 0.5)]

        # calculate energy density (signal window = 200ns, subtract influence
        # of noise)
        mask_nonoise = (tt > (peak_time_sum - 100)) & (
            tt < (peak_time_sum + 100))
        if dlength % 2 == 1:
            mask_nonoise = mask_nonoise[0:-1]
        mask_noise = ~mask_nonoise

        u_signal = np.sum(filt[..., mask_nonoise] ** 2, axis=-1) * \
            conversion_factor_integrated_signal * tstep
        u_noise = np.sum(filt[..., mask_noise] ** 2, axis=-1) * \
            conversion_factor_integrated_signal * tstep * \
            np.sum(mask_nonoise) / np.sum(mask_noise)
        power[j] = u_signal - u_noise

        # calculate power in shower plane
        trace_showerplane = ctrans.transform_to_vxB_vxvxB(filt.T).T
        u_signal = np.sum(trace_showerplane[..., mask_nonoise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep
        u_noise = np.sum(trace_showerplane[..., mask_noise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep * np.sum(mask_nonoise) / np.sum(mask_noise)
        power_showerplane[j] = u_signal - u_noise

    zhaires_event.antenna_position = antenna_position
    zhaires_event.power = power
    zhaires_event.power_showerplane = power_showerplane
    zhaires_event.energy_density = np.sum(power, axis=1)

    # calculate radiation enrergy
    cs = CSTransformation(zhaires_event.zenith, zhaires_event.azimuth, magnetic_field=magnetic_field_vector)
    station_positions_transformed = cs.transform_to_vxB_vxvxB(antenna_position)
    xx = station_positions_transformed[..., 0]
    yy = station_positions_transformed[..., 1]
    distances = (xx ** 2 + yy ** 2) ** 0.5
    tot_power = np.sum(power, axis=1)

    az = np.round(np.rad2deg(np.arctan2(yy, xx)))
    az[az < 0] += 360
    az[az >= 360] -= 360
    mask = (az == 90) | (az == 270)
    dd = distances[mask]
    yy_tot = tot_power[mask]

    data = np.column_stack((yy_tot,dd))
    datasorted = np.array(sorted(data, key=lambda row: row[1]))
    yy_tot = datasorted[:,0]
    dd = datasorted[:,1]

    from scipy import integrate
    y_int_num = integrate.cumtrapz(yy_tot * dd, dd)[-1] * 2 * np.pi
    zhaires_event.radiation_energy_1D = y_int_num

    if (np.unique(az) == np.arange(0, 360, 45.)).all():
        import scipy.interpolate as intp
        print "\tcalculating radiation energy via 2D integration"
        func = intp.Rbf(xx, yy, tot_power, smooth=0, function='quintic')
        x_range = xx.max()
        y_range = yy.max()
        opts = {'epsabs': 1., 'epsrel': 1.49e-01, 'limit': 1000}

        def bounds_y(x):
            return [-np.sqrt(np.square(x_range) - np.square(x)), np.sqrt(np.square(y_range) - np.square(x))]

        def bounds_x():
            return [-x_range, y_range]

        radiation_energy_2D = integrate.nquad(func, [bounds_y, bounds_x], opts=opts)
        zhaires_event.radiation_energy = radiation_energy_2D

    pickfile = open(pickname, 'wb')
    cPickle.dump(zhaires_event, pickfile)
    pickfile.close()

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option(
        "-d", "--directory", default="./", help="Input directory")
    parser.add_option("-o", "--output", default="./", help="Output directory")
    (options, args) = parser.parse_args()

    dir_list = os.listdir(options.directory)
    dir_list.sort()

    print "processing directory %s" % options.directory
    for d in dir_list:
        dd = os.path.join(options.directory, d)
        if os.path.isdir(dd):
            print "processing event %s" % d
            try:
                ProcessData(dd, options.output, int(d))
            except Exception:
                print "processing data failed"
