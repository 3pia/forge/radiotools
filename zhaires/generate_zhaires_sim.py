import numpy as np
import HelperFunctions as hp
import CSTransformation
import os
import stat


def write_input_file(filename, event_number, primary_particle, E,
                     zenith, azimuth, rs, seed, obs_level, 
                     F=24.2807845837, I=-0.623648716439, thin=1e-5,
                     thinwfac=0.06, n=1.00029200, const_n=False,
                     azimuths=np.deg2rad([0, 45, 90, 135, 180, 225, 270, 315])):
    with open(filename, 'w') as f:
        f.write("TaskName Event%i \n" % event_number)

        # AIRES section
        f.write("PrimaryParticle %s\n" % primary_particle)
        f.write("PrimaryEnergy %f eV\n" % E)
        f.write("PrimaryZenAngle %f deg\n" % zenith)
        f.write("PrimaryAzimAngle %f deg Magnetic\n" % (float(azimuth) - 90))
                # use magnetic azimuth to avoid confusion with geografic
                # coordinates in AIRES, convert azimuth to zhaires definition

        f.write("ForceModelName SIBYLL\n")

        f.write("TotalShowers 1\n")
        f.write("RunsPerProcess Infinite\n")
        f.write("ShowersPerRun 1\n")
        f.write("RandomSeed %f\n" % seed)

        f.write("InjectionAltitude 100 km\n")
        f.write("GroundAltitude %f cm\n" % obs_level)
        f.write("ObservingLevels 510\n")
                # obs_level used for long. development recording
        f.write("Atmosphere 1\n")
                # Linsley's standard atmosphere model, 2 for Linsleys model
                # south pole
        f.write("GeomagneticField %f uT %f rad\n" % (F, I))

        f.write("MuonCutEnergy 0.05 GeV\n")
        f.write("ElectronCutEnergy 0.00025 GeV\n")
        f.write("GammaCutEnergy 0.00025 GeV\n")
        # HadronCutEnergy: "Obsolete Hillas splitting parameter. It has no
        # equivalent in the current implementation of the algorithm"

        f.write("ThinningEnergy %e Relative\n" % thin)
        f.write("ThinningWFactor %f\n" %thinwfac)

        f.write("SaveInFile grdpcles None\n")
        f.write("SaveInFile lgtpcles None\n")

        # ZHAires section
        f.write("ZHAireS On\n")
        f.write("RefractionIndex %.10f\n" % n)
        if const_n:
            f.write("ConstRefrIndex\n")

        f.write("FresnelTime On\n")
        f.write("TimeDomainBin 0.2 ns\n")

        """
        f.write("FresnelFreq On\n")
        f.write("AddFrequency Linear %i %i %i\n" % (fmin, fmax, n_freq))
        """

        zen = np.deg2rad(zenith)
        az = np.deg2rad(azimuth)
        B = np.array([0, np.cos(I), -np.sin(I)])

        cs = CSTransformation.CSTransformation(zen, az, magnetic_field=B)
        n_antenna = len(rs)
        rs = np.append(0, rs)
        for azim in azimuths:
            for i in np.arange(1, n_antenna + 1):
                station_position = rs[i] * hp.SphericalToCartesian(
                    np.pi * 0.5, azim)
                pos = cs.transform_from_vxB_vxvxB(station_position)
                name = "pos_%i_%i" % (rs[i], np.rad2deg(azim))
                x, y, z = pos[1], -pos[0], pos[2] + obs_level
                f.write("AddAntenna {0} {1} {2}\n".format(
                    x, y, z))  # in m, aires coordinate system
        # AddAntenna Line x1 y1 z1 x2 y2 z2 n_ant <- line from point 1 to point
        # 2 with n_ant equally spaced antennas

        f.write("End\n")


def write_shell_script(filename, tmp_dir, event_number, input_file_name, output_dir):
    with open(filename, 'w') as f:
        f.write("rm -rf %s\n" % tmp_dir)
        f.write("mkdir %s\n" % tmp_dir)
        f.write("cd %s\n" % tmp_dir)
        f.write("Aires < %s\n" % input_file_name)
        f.write("mkdir %s/%06i\n" % (output_dir, event_number))
        f.write("mv Event%i.lgf %s/%06i/\n" %
                (event_number, output_dir, event_number))
        f.write("mv Event%i.sry %s/%06i/\n" %
                (event_number, output_dir, event_number))
        f.write("mv timefresnel-root.dat %s/%06i/\n" %
                (output_dir, event_number))
