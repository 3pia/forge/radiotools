from radiotools.coreas.pickle_sim_to_class import *
import matplotlib.pyplot as plt
from radiotools import HelperFunctions as hp
import pickle
from optparse import OptionParser
from radiotools.atmosphere import models as atm
from scipy import integrate as int

atmc = atm.Atmosphere(model=1)

parser = OptionParser()
parser.add_option("-f", "--filename", default="", help="input filename")
(options, args) = parser.parse_args()

fin = open(options.filename, "r")
event = pickle.load(fin)
fin.close()

if 0:  # plotting of lingitudinal profile
    fig, ax = plt.subplots(1, 1)
    X = event.long_dE[0]
    dEdXnewem = event.long_dE[2] + event.long_dE[1] + event.long_dE[3]
    ax.plot(X, dEdXnewem, "-b")
    ax.legend()
    ax.set_xlabel("X [g/cm^2]")
    ax.set_ylabel("dE/dX (EM)")
    plt.show()


for key, obs_level in event.obs_levels.items():
    observation_height = obs_level["core"][2]
    print("observation level = %g at %s" % (observation_height, obs_level['orientation']))
    zenith = event.zenith
    azimuth = event.azimuth
    antenna_position = obs_level["antenna_position"]
    magnetic_field_vector = rdhelp.SphericalToCartesian(np.pi * 0.5 + event.magnetic_field_inclination, event.magnetic_field_declination + np.pi * 0.5)  # in auger cooordinates north is + 90 deg
    core = copy.copy(event.core)

    core = obs_level["core"]
    cs = CSTransformation(zenith, azimuth, core, magnetic_field=magnetic_field_vector)
    station_positions_transformed = cs.transform_to_vxB_vxvxB(antenna_position)
    xx = station_positions_transformed[..., 0]
    yy = station_positions_transformed[..., 1]
    distances = (xx ** 2 + yy ** 2) ** 0.5
    tot_power = obs_level['energy_fluence']

    az = np.round(np.rad2deg(np.arctan2(yy, xx)))
    az[az < 0] += 360
    az[az >= 360] -= 360
    mask = (az == 90) | (az == 270)

    fig, ((ax2, ax1), (ax2l, ax1l)) = plt.subplots(2, 2)
    ddp = distances[az == 90]
    yyp = tot_power[az == 90]
    ddm = distances[az == 270]
    yym = tot_power[az == 270]
    ax1.plot(ddp, yyp, "ok")
    ax1.plot(-1.* ddm, yym, "ok")
    ax1.set_xlabel("position in vxvxB [m]")

    ax1l.plot(ddp, yyp, "ok")
    ax1l.plot(-1.* ddm, yym, "ok")
    ax1l.set_xlabel("position in vxvxB [m]")
    ax1l.semilogy(True)

    ddp = distances[az == 0]
    yyp = tot_power[az == 0]
    ddm = distances[az == 180]
    yym = tot_power[az == 180]
    ax2.plot(ddp, yyp, "ok")
    ax2.plot(-1.* ddm, yym, "ok")
    ax2.set_xlabel("position in vxB [m]")

    ax2l.plot(ddp, yyp, "ok")
    ax2l.plot(-1.* ddm, yym, "ok")
    ax2l.set_xlabel("position in vxB [m]")
    ax2l.semilogy(True)
    plt.tight_layout()

    fig.suptitle("E = %.2f EeV, theta = %.0f, phi = %.0f, h = %.0fm" % (event.energy * 1e-18, np.rad2deg(zenith),
                                                                        np.rad2deg(azimuth), observation_height))
    fig.subplots_adjust(top=0.93)

    # plot 2D LDF
    observation_height = core[2]
    Xatm = atmc.get_distance_xmax(event.zenith, 0, observation_height)
    xmax = event.hillas[2]
    dxmax = atmc.get_distance_xmax(event.zenith, xmax, observation_height)

    fig, ax = plt.subplots(1, 1)
    ax.scatter(xx, yy, c=tot_power, marker='o', cmap=plt.cm.gnuplot2_r, linewidth=2,
               s=150, label='data', edgecolors=".4")
    ax.set_xlabel("position in vxB [m]")
    ax.set_ylabel("position in vxvxB [m]")
    ax.set_aspect("equal")

    # plot 1D projection

    Xatm = atmc.get_distance_xmax(event.zenith, 0, observation_height)
    deltaxmax = atmc.get_distance_xmax(event.zenith, xmax, observation_height)

    mask = (az == 90)
    if np.sum(mask):
        dd = distances[mask]
        yy_tot = obs_level['energy_fluence'][mask]
        y_int_num = int.cumtrapz(yy_tot * dd, dd)[-1] * 2 * np.pi

        xxx = np.linspace(0, distances[az == 90].max() * 1.5)
        fig, ax3 = plt.subplots(1, 1, sharex=True, squeeze=True, figsize=(5, 4))
        ax3.errorbar(dd, yy_tot, marker="o", color="k", linestyle="None",
                     markersize=10, zorder=2, label=r"$\phi = %i$" % (90.))
        colors = ['g', 'b', 'r', 'm', 'r', 'b', 'g', 'm']
        markers = ['o', 'o', 'o', 'o', 'd', 'd', 'd', 'd']
        unit = r"energy fluence $f$ [eV/m$^2$]"
        ax3.set_xlabel("distance to shower axis [m]")
        ax3.set_ylabel(unit)
        ax3.set_ylim(0)
        dy = 0.85
        x = 0.4
        ax3.text(x, dy, r"X = %.0f g/cm$^2$" % (Xatm), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        ax3.text(x, dy, r"h = %.1f km" % (observation_height * 1e-3), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        ax3.text(x, dy, r"$\Delta$X$_\mathrm{max}$ = %.0f g/cm$^2$" % (deltaxmax), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        ax3.text(x, dy, r"$E_\mathrm{rad}$ = %.3g eV" % (y_int_num), transform=ax3.transAxes, fontsize="large")
        if 'radiation_energy' in obs_level:
            dy -= 0.12
            ax3.text(x, dy, r"$E_\mathrm{rad, 2D}$ = %.3g eV" % (obs_level['radiation_energy'][0]), transform=ax3.transAxes, fontsize="large")
        # ax3.text(0.4, dy, r"$2 \pi \int u(r) r dr$ = %.4g eV" % ((y_int_ce_gamma[0] + y_int_geo_gamma[0]) * 2 * np.pi), color="g", transform=ax3.transAxes)
        ax3.set_ylim(0, yy_tot.max() * 1.2)
        ax3.set_xlim(0, dd.max())
        ax3.set_title(r"E = %.1f EeV $\theta$ = %.0f $\phi$ = %.0f" % (event.energy * 1e-18, np.rad2deg(event.zenith), np.rad2deg(event.azimuth)))
        plt.tight_layout()

        # plot 1D projection geomangetic and charg-excess separately
        fig, (ax3, ax4) = plt.subplots(1, 2, sharex=True, squeeze=True, figsize=(10, 4))
        ax3.errorbar(dd, obs_level['power_showerplane'][az == 90][..., 0], marker="o", color="k", linestyle="None",
                     markersize=10, zorder=2, label="geomagnetic")
        ax4.errorbar(dd, obs_level['power_showerplane'][az == 90][..., 1], marker="o", color="k", linestyle="None",
                     markersize=10, zorder=2, label="charge-excess")

        ax3.set_xlabel("distance to shower axis [m]")
        ax4.set_xlabel("distance to shower axis [m]")
        ax3.set_ylabel(r"energy fluence $f_\mathrm{geo}$ [eV/m$^2$]")
        ax4.set_ylabel(r"energy fluence $f_\mathrm{ce}$ [eV/m$^2$]")
        ax3.set_ylim(0)
        ax4.set_ylim(0)
        dy = 0.85
        x = 0.4
        ax3.text(x, dy, r"X = %.0f g/cm$^2$" % (Xatm), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        ax3.text(x, dy, r"h = %.1f km" % (observation_height * 1e-3), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        ax3.text(x, dy, r"$\Delta$X$_\mathrm{max}$ = %.0f g/cm$^2$" % (deltaxmax), transform=ax3.transAxes, fontsize="large")
        dy -= 0.12
        y_int_num_geo = int.cumtrapz(obs_level['power_showerplane'][az == 90][..., 0] * dd, dd)[-1] * 2 * np.pi
        y_int_num_ce = int.cumtrapz(obs_level['power_showerplane'][az == 90][..., 1] * dd, dd)[-1] * 2 * np.pi
        ax3.text(x, dy, r"$E_\mathrm{rad}$ = %.3g eV" % (y_int_num_geo), transform=ax3.transAxes, fontsize="large")
        ax4.text(x, dy, r"$E_\mathrm{rad}$ = %.3g eV" % (y_int_num_ce), transform=ax4.transAxes, fontsize="large")
        # ax3.text(0.4, dy, r"$2 \pi \int u(r) r dr$ = %.4g eV" % ((y_int_ce_gamma[0] + y_int_geo_gamma[0]) * 2 * np.pi), color="g", transform=ax3.transAxes)
        # ax3.set_ylim(0, yy_tot.max() * 1.2)
        ax3.set_xlim(0, dd.max())
        ax3.set_title(r"E = %.1f EeV $\theta$ = %.0f $\phi$ = %.0f" % (event.energy * 1e-18, np.rad2deg(event.zenith), np.rad2deg(event.azimuth)))
        plt.tight_layout()
    plt.show()
