import numpy as np
from radiotools.atmosphere import models as atm

atmc = atm.Atmosphere(model=1)  # initialize atmosphere class with model 1 (US standard atmospher)
# all functions use "grams" and "meters", only the functions that receive and
# return "atmospheric depth" use the unit "g/cm^2"


# calculate density at Xmax
xmax = 600.  # in g/cm^2
zenith = np.deg2rad(38)
rho = atmc.get_density(zenith, xmax)
print "the density for an %.0fdeg air shower at the shower maximum of xmax = \
%.1f g/cm^2 is %.2f g/m^3" % (np.rad2deg(zenith), xmax, rho)

# This function is also implemented vectorized
xmaxs = np.array([600., 700., 800.])  # in g/cm^2
zeniths = np.ones_like(xmaxs) * np.deg2rad(38)
rhos = atmc.get_density(zeniths, xmaxs)
print rhos

# calculate the distance from the ground to the shower maximum
Dxmax = atmc.get_distance_xmax(zenith, xmax, observation_level=1564.)
print "the distance to the shower maximum is %.2f g/cm^2 for an air shower with\
 an %.0fdeg zenith angle and a shower maximum of xmax = \
%.1f g/cm^2" % (Dxmax, np.rad2deg(zenith), xmax)

# or again vectorized
Dxmaxs = atmc.get_distance_xmax(zeniths, xmaxs, observation_level=1564.)
print Dxmaxs