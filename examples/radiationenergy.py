import numpy as np
from radiotools.analyses import radiationenergy
from radiotools.atmosphere import models as atm
from radiotools import HelperFunctions as hp

atmc = atm.Atmosphere(model=1)  # initialize atmosphere class with model 1 (US standard atmospher)

# define shower parameters
xmax = 700.  # in g/cm^2
zenith = np.deg2rad(38)
azimuth = np.deg2rad(270)
sinalpha = hp.GetSineAngleToLorentzForce(zenith, azimuth)

# get fraction of radiation energy released in atmosphere
Dxmax = atmc.get_distance_xmax(zenith, xmax, observation_level=1564.)
frac = radiationenergy.get_clipping(Dxmax)
print "clipping correction for an %.0fdeg air shower with xmax = %.1fg/cm^2 is %.3f" % (np.rad2deg(zenith), xmax, frac)

# get corrected radiation energy with xmax information
erad = 10e6  # 10 MeV
density = atmc.get_density(zenith, xmax) * 1.e-3  # in kg/m^3
Srd = radiationenergy.get_S(erad, sinalpha, density)
print("corrected radiation energy: %.2f MeV" % (Srd * 1e-6))

# get corrected radiation energy without xmax information
Srd_zenith = radiationenergy.get_S_zenith(erad, sinalpha, zenith)
print("corrected radiation energy wo xmax information: %.2f MeV" % (Srd_zenith * 1e-6))
