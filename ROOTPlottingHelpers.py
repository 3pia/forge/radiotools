#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math
import os
import ROOT
from ROOT import TCanvas, TLine
from ROOT import TGraph, TGraphErrors
from ROOT import TText, TLatex 
from ROOT import TH2D, TH1D
from CSTransformation import *


def show_plot():
    try:  # so windows don't close
        from time import sleep
        while len(ROOT.gROOT.GetListOfCanvases()) > 0:
            sleep(0.5)
    except KeyboardInterrupt:
        raise SystemExit()
    except:
        print "Hilfe!"


def get_color(i):
    colors = [1, 2, 4, 6, 3, 7, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]
    if(i > len(colors) - 1 or i < 0):
        return 1
    else:
        return colors[i]


def GetTH1D(name, title, nBins, bin1, bin2, xAxisTitle, yAxisTitle="Entries"):
    h1 = TH1D(name, title, nBins, bin1, bin2)
    h1.GetXaxis().SetTitle(xAxisTitle)
    h1.GetYaxis().SetTitle(yAxisTitle)
    return h1


def GetTH2D(name, title, nBins, bin1, bin2, nBinsY, binY1, binY2,
            xAxisTitle="", yAxisTitle="", zAxisTitle="Entries"):
    h1 = TH2D(name, title, nBins, bin1, bin2, nBinsY, binY1, binY2)
    h1.GetXaxis().SetTitle(xAxisTitle)
    h1.GetYaxis().SetTitle(yAxisTitle)
    h1.GetZaxis().SetTitle(zAxisTitle)
    h1.SetContour(10000)
    return h1


def GetTGraphErrors(title="", xAxisTitle="", yAxisTitle=""):
    g1 = TGraphErrors()
    g1.GetXaxis().SetTitle(xAxisTitle)
    g1.GetYaxis().SetTitle(yAxisTitle)
    g1.SetTitle(title)
    return g1


def draw_two_histograms(h_all, h=None, scale=True):
        if(h == None):
            if(type(h_all) == ROOT.TGraph or type(h_all) == ROOT.TGraphErrors):
                h_all.Draw("AP")
            if(type(h_all) == ROOT.TH2D):
                # h_all.SetStats(0)
                h_all.Draw("COLZ")
            else:
                h_all.GetYaxis().SetRangeUser(0., 1.2 * h_all.GetMaximum())
                h_all.Draw()
        else:
            if(scale and (h.GetEntries() != 0 and h_all.GetEntries() != 0)):
                h_all.Scale(1. * h.GetEntries() / h_all.GetEntries())
            h_all.GetYaxis().SetRangeUser(0., 1.35 * max(h_all.GetMaximum(), h.GetMaximum()))
            h_all.SetMarkerColor(2)
            h_all.SetStats(0)
            h_all.Draw("P")
            h.Draw("sames")


def plot_histograms(hs_all, hs=None, canvas_name='c1', plot_folder='', show=False, scale=True):
    import uuid
    N = len(hs_all)
    if(hs):
        for h in hs:  # increase number of pads by one in case of 2D histograms
            if(type(h) == ROOT.TH2D):
                N += 1
    if(N <= 3):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * N, 500)
        c.Divide(N, 1)
    elif(N == 4):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 2, 1000)
        c.Divide(2, 2)
    elif(N <= 6):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 3, 1000)
        c.Divide(3, 2)
    elif(N <= 9):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 3, 1500)
        c.Divide(3, 3)
    elif(N <= 12):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 4, 1500)
        c.Divide(4, 3)
    elif(N <= 16):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 4, 500 * 4)
        c.Divide(4, 4)
    elif(N <= 20):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 5, 500 * 4)
        c.Divide(5, 4)
    elif(N <= 25):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 5, 500 * 5)
        c.Divide(5, 5)
    elif(N <= 30):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 6, 500 * 5)
        c.Divide(6, 5)
    elif(N <= 35):
        c = TCanvas(str(uuid.uuid4()), canvas_name, 500 * 7, 500 * 5)
        c.Divide(7, 5)
    else:
        print "WARNING: more than 35 pads are not implemented"
        raise "WARNING: more than 35 pads are not implemented"
    i_canvas = 0
    for i, h in enumerate(hs_all):
        i_canvas += 1 
        c.cd(i_canvas)
        if(hs == None):
            if(type(h) == ROOT.TH2D):
                c.GetPad(i_canvas).SetRightMargin(0.16)
            draw_two_histograms(h, None, scale=scale)
        else:
            if(type(hs[i]) == ROOT.TH2D):
                c.GetPad(i_canvas).SetRightMargin(0.16)
                draw_two_histograms(h, None, scale=scale)
                i_canvas += 1
                c.cd(i_canvas)
                c.GetPad(i_canvas).SetRightMargin(0.16)
                draw_two_histograms(hs[i], None, scale=scale)
            else:
                draw_two_histograms(h, hs[i], scale=scale)
    print("saving histogram to %s" % os.path.join(plot_folder, canvas_name + ".png"))
    c.SaveAs(os.path.join(plot_folder, canvas_name + ".png"))
    if(show):
        show_plot()


def plot_map(core, azimuth, positions, signals, positions_all=None,
                     localCS=True, show=False, filename=None, text=''):
    core = np.array(core)
    if(localCS):
        positions -= core
        if(positions_all != None):
            positions_all -= core
        core = np.array([0, 0, 0])
    signal_size = 25.
    minx = min(positions[..., 0].min(), core[0])
    miny = min(positions[..., 1].min(), core[1])
    maxx = max(positions[..., 0].max(), core[0])
    maxy = max(positions[..., 1].max(), core[1])
    nx = np.int(np.round((maxx - minx + 200) / signal_size))
    ny = np.int(np.round(((maxy - miny + 200) / signal_size)))
    hPos1 = GetTH2D("hPos1", "",
                nx + 1, minx - 100 - 0.5 * signal_size,
                maxx + 100 + 0.5 * signal_size,
                ny + 1, miny - 100 - 0.5 * signal_size,
                maxy + 100 + 0.5 * signal_size,
                "x [m]", "y [m]", "sigmal [#muV/m]")
    np.vectorize(lambda x, y, w: hPos1.Fill(x, y, w))(positions[..., 0], positions[..., 1], signals * 1e6)
    c = TCanvas("c")
    c.cd()
    c.GetPad(0).SetRightMargin(0.16)
    hPos1.SetMarkerStyle(20)
    hPos1.SetMarkerSize(5)
    hPos1.SetStats(0)
    hPos1.Draw("COLZ")
    if(positions_all != None):
        gPos = TGraph()
        for i, (x, y, z) in enumerate(positions_all):
            gPos.SetPoint(i, x, y)
        gPos.Draw("P")
    gCore = TGraph()
    gCore.SetPoint(0, core[0], core[1])
    gCore.SetMarkerStyle(29)
    gCore.Draw("Psame")
    # plot incoming direction
    line = TLine()
    line.SetLineWidth(2)
    length = 200
    line.DrawLine(core[0], core[1], core[0] + math.cos(azimuth) * length,
                core[1] + math.sin(azimuth) * length)
    latex = TLatex()
    latex.SetNDC(True)
    # latex.SetTextSize(0.022)
    latex.DrawLatex(0.16, 0.93, text)
    if(filename):
        c.SaveAs(filename)
    if(show):
        show_plot()



def plot_map2(core, azimuth, positions, signals, positions_all=None,
                     localCS=True, show=False, filename=None, text=''):
    import uuid
    core = np.array(core)
    if(localCS):
        positions -= core
        if(positions_all != None):
            positions_all -= core
        core = np.array([0, 0, 0])
    signal_size = 25.
    minx = min(positions[..., 0].min(), core[0])
    miny = min(positions[..., 1].min(), core[1])
    maxx = max(positions[..., 0].max(), core[0])
    maxy = max(positions[..., 1].max(), core[1])
    nx = np.int(np.round((maxx - minx + 200) / signal_size))
    ny = np.int(np.round(((maxy - miny + 200) / signal_size)))
    hPos = GetTH2D(str(uuid.uuid4()), "",
                nx + 1, minx - 100 - 0.5 * signal_size,
                maxx + 100 + 0.5 * signal_size,
                ny + 1, miny - 100 - 0.5 * signal_size,
                maxy + 100 + 0.5 * signal_size,
                "x [m]", "y [m]", "sigmal [#muV/m]")
    np.vectorize(lambda x, y, w: hPos.Fill(x, y, w))(positions[..., 0], positions[..., 1], signals * 1e6)
    hPos.SetMarkerStyle(20)
    hPos.SetMarkerSize(5)
    hPos.SetStats(0)
    hPos.Draw("COLZ")
    if(positions_all != None):
        gPos = TGraph()
        for i, (x, y, z) in enumerate(positions_all):
            gPos.SetPoint(i, x, y)
        gPos.Draw("P")
    gCore = TGraph()
    gCore.SetPoint(0, core[0], core[1])
    gCore.SetMarkerStyle(29)
    gCore.Draw("Psame")
    # plot incoming direction
    line = TLine()
    line.SetLineWidth(2)
    length = 200
    line.DrawLine(core[0], core[1], core[0] + math.cos(azimuth) * length,
                core[1] + math.sin(azimuth) * length)
    latex = TLatex()
    latex.SetNDC(True)
    # latex.SetTextSize(0.022)
    latex.DrawLatex(0.16, 0.93, text)
    return hPos


def plot_vxB_LDF(zenith, azimuth, positions, signals, core=None, filename=None, show=False):
    import uuid
    # test class
    print "plotvxb"
    cs = CSTransformation(zenith, azimuth, core=core)
    positions_transformed = cs.transform_to_vxB_vxvxB(positions)
    gvxB = TGraph()
    gvxvxB = TGraph()
    for i, pos in enumerate(positions_transformed):
        gvxB.SetPoint(i, pos[0], signals[i])
        gvxvxB.SetPoint(i, pos[1], signals[i])
    c = TCanvas("c", "", 1200, 1000)
    c.Divide(2, 2)
    c.cd(1)
    c.GetPad(1).SetRightMargin(0.16)
    core = np.array([0, 0, 0])
    signal_size = 25.
    minx = min(positions[..., 0].min(), core[0])
    miny = min(positions[..., 1].min(), core[1])
    maxx = max(positions[..., 0].max(), core[0])
    maxy = max(positions[..., 1].max(), core[1])
    nx = np.int(np.round((maxx - minx + 200) / signal_size))
    ny = np.int(np.round(((maxy - miny + 200) / signal_size)))
    hPos1 = GetTH2D(str(uuid.uuid4()), "",
                nx + 1, minx - 100 - 0.5 * signal_size,
                maxx + 100 + 0.5 * signal_size,
                ny + 1, miny - 100 - 0.5 * signal_size,
                maxy + 100 + 0.5 * signal_size,
                "x [m]", "y [m]", "sigmal [#muV/m]")
    np.vectorize(lambda x, y, w: hPos1.Fill(x, y, w))(positions[..., 0], positions[..., 1], signals * 1e6)
    hPos1.SetMarkerStyle(20)
    hPos1.SetMarkerSize(5)
    hPos1.SetStats(0)
    hPos1.Draw("COLZ")
    gCore1 = TGraph()
    gCore1.SetPoint(0, core[0], core[1])
    gCore1.SetMarkerStyle(29)
    gCore1.Draw("Psame")
    # plot incoming direction
    line = TLine()
    line.SetLineWidth(2)
    length = 200
    line.DrawLine(core[0], core[1], core[0] + math.cos(azimuth) * length,
                core[1] + math.sin(azimuth) * length)
    c.cd(2)
    c.GetPad(2).SetRightMargin(0.16)
    core = np.array([0, 0, 0])
    signal_size = 25.
    minx = min(positions_transformed[..., 0].min(), core[0])
    miny = min(positions_transformed[..., 1].min(), core[1])
    maxx = max(positions_transformed[..., 0].max(), core[0])
    maxy = max(positions_transformed[..., 1].max(), core[1])
    nx = np.int(np.round((maxx - minx + 200) / signal_size))
    ny = np.int(np.round(((maxy - miny + 200) / signal_size)))
    hPos = GetTH2D(str(uuid.uuid4()), "",
                nx + 1, minx - 100 - 0.5 * signal_size,
                maxx + 100 + 0.5 * signal_size,
                ny + 1, miny - 100 - 0.5 * signal_size,
                maxy + 100 + 0.5 * signal_size,
                "x [m]", "y [m]", "sigmal [#muV/m]")
    np.vectorize(lambda x, y, w: hPos.Fill(x, y, w))(positions_transformed[..., 0], positions_transformed[..., 1], signals * 1e6)
    hPos.SetMarkerStyle(20)
    hPos.SetMarkerSize(5)
    hPos.SetStats(0)
    hPos.Draw("COLZ")
    gCore = TGraph()
    gCore.SetPoint(0, core[0], core[1])
    gCore.SetMarkerStyle(29)
    gCore.Draw("Psame")
    # plot incoming direction
    line.SetLineWidth(2)
    length = 200
    # transform shower axis
    axis = hp.SphericalToCartesian(zenith, azimuth)
    axis_transformed = cs.transform_to_vxB_vxvxB(axis)
    zenith_t, azimuth_t = hp.CartesianToSpherical(axis_transformed[0], axis_transformed[1], axis_transformed[2])
    print zenith_t

    c.cd(3)
    gvxB.GetXaxis().SetTitle("vxB [m]")
    gvxB.GetYaxis().SetTitle("signal [#muV/m]")
    gvxB.Draw("AP")
    c.cd(4)
    gvxvxB.GetXaxis().SetTitle("vxvxB [m]")
    gvxvxB.GetYaxis().SetTitle("signal [#muV/m]")
    gvxvxB.Draw("AP")
    print("drawn")
    if(filename):
        c.SaveAs(filename)
    if(show):
        print "show"
        show_plot()












