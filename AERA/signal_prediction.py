import numpy as np
from radiotools.atmosphere import models as atm
from radiotools.analyses import radiationenergy
from radiotools import HelperFunctions as hp

conversion_factor_integrated_signal = 2.65441729e-3 * 6.24150934e18  # to convert V**2/m**2 * s -> J/m**2 -> eV/m**22
atmc = atm.Atmosphere(model=1)


def get_Srd_JCAP(energy):
    # see Glaser et al., JCAP 09(2016)024
    return 1.683e7 * (energy * 1e-18) ** 2.006


def get_radiation_energy(energy, zenith, azimuth, xmax):
    sinalpha = hp.GetSineAngleToLorentzForce(zenith, azimuth)
    density = atmc.get_density(zenith, xmax) * 1e-3  # in kg/m^3
    Srd = get_Srd_JCAP(energy)
    Erad = radiationenergy.get_radiation_energy(Srd, sinalpha, density)
    return Erad


def get_amplitude(Erad, zenith, sigma_plus, sigma_minus):
    c0, c1, c2 = get_constants(zenith)
    A = Erad / (np.pi * (sigma_plus ** 2 - c0 * sigma_minus ** 2))
    return A


def get_sigma_plus(dxmax):
    return 37.7 - 0.006 * dxmax + 5.5e-4 * dxmax ** 2 - 3.7e-7 * dxmax ** 3


def get_sigma_minus(dxmax):
    return 26.9 - 0.041 * dxmax + 2e-4 * dxmax ** 2


def get_constants(zenith):
    C_0 = 0.44
    if zenith < np.deg2rad(10):
        C1theta = 8.
        C2theta = -21.2
    elif zenith < np.deg2rad(20):
        C1theta = 10.
        C2theta = -23.1
    elif zenith < np.deg2rad(30):
        C1theta = 12.
        C2theta = -25.5
    elif zenith < np.deg2rad(40):
        C1theta = 20.
        C2theta = -32.
    elif zenith < np.deg2rad(50):
        C1theta = 25.1
        C2theta = -34.5
    elif zenith < np.deg2rad(55):
        C1theta = 27.3
        C2theta = -9.8
        C_0 = 0.46
    elif zenith >= np.deg2rad(55):
        C1theta = 27.3
        C2theta = -9.8
        C_0 = 0.71
    return C_0, C1theta, C2theta


def get_energy_fluence(x, y, energy, zenith, azimuth, xmax, core=None):
    X = 0
    Y = 0
    if core is not None:
        X = core[0]
        Y = core[1]

    Erad = get_radiation_energy(energy, zenith, azimuth, xmax)
    dxmax = atmc.get_distance_xmax(zenith, xmax, observation_level=1564.)
    sigma_plus = get_sigma_plus(dxmax)
    sigma_minus = get_sigma_minus(dxmax)
    A = get_amplitude(Erad, zenith, sigma_plus, sigma_minus)
    c0, c1, c2 = get_constants(zenith)
    t1 = np.exp(-((x - (X + c1)) ** 2 + (y - Y) ** 2) / sigma_plus ** 2)
    t2 = np.exp(-((x - (X + c2)) ** 2 + (y - Y) ** 2) / sigma_minus ** 2)
    return A * t1 - c0 * A * t2


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    E = 0.5e18
    zenith = np.deg2rad(50)
    azimuth = np.deg2rad(270)
    import astrotools
    xmax = 0.5 * (astrotools.auger.gumbelParameters(18, 1, 'QGSJetII-04')[0] + astrotools.auger.gumbelParameters(18, 56, 'QGSJetII-04')[0])
    xmax = 700
    d = 100
    print "E = %.2g, zenith %.0f deg, d = %.0f m: f = %.3g eV/m^2" % (E, np.rad2deg(zenith), d, get_energy_fluence(d, 0, E, zenith, azimuth, xmax))

    xx = np.linspace(0, 500, 500)
    fig, ax = plt.subplots(1, 1)
    ax.plot(xx, get_energy_fluence(0, xx, E, zenith, azimuth, xmax))
    ax.plot(xx, get_energy_fluence(0, xx, E, zenith, 0, xmax))
#     ax.plot(xx, get_energy_fluence(xx, 0, E, np.deg2rad(30), azimuth, xmax))
#     ax.plot(xx, get_energy_fluence(xx, 0, E, np.deg2rad(0), azimuth, xmax))
    plt.show()









