import numpy as np
import os

folder = os.path.dirname(os.path.abspath(__file__))
RDS_id, lat, lon, y, x, h = np.genfromtxt(os.path.join(folder, "AERA_coordinates_DB_3_all_info_correct.txt"),
                                                    skip_footer=10,
                                                    usecols=(0, 2, 3, 4, 5, 6), unpack=True)
stations_UTM = {}
for i, key in enumerate(RDS_id):
    stations_UTM[key] = np.array([x[i], y[i], h[i]])


stations_latlon = {}
for i, key in enumerate(RDS_id):
    stations_latlon[key] = np.array([lat[i], lon[i]])


lRDS_id, lx, ly, lz = np.genfromtxt(os.path.join(folder, "AERA_localCRS_coordinates.txt"),
                                    usecols=(0, 2, 3, 4), unpack=True, delimiter=",")

stations_CRS = {}
for i, key in enumerate(lRDS_id):
    stations_CRS[key] = np.array([lx[i], ly[i], lz[i]])


def get_stations_UTM():
    return stations_UTM


def get_stations_latlon(station_id):
    return stations_latlon[station_id]


def get_stations_CRS():
    return stations_CRS


def get_positions_UTM():
    return x, y, h


def get_lat_lon():
    return lat, lon


def get_station_ids():
    return RDS_id


def get_NL_stations():
    return np.array([2, 5, 9, 4, 8, 12, 34, 35, 36, 43, 44, 49, 58, 59, 60, 69,
                     70, 83, 84, 85, 88, 92, 93, 94, 100, 101, 102, 103, 110, 111,
                     112, 113, 122, 123, 124, 125, 126, 135, 136, 137, 144, 145,
                     146, 151, 152, 157], dtype=np.int)


def get_positions_local_CRS():
    return lx, ly, lz


def get_station_ids_local_CRS():
    return lRDS_id


def get_PampaAmarilla_in_UTM():
    return np.array([477256.659999998, 6099203.68000086, 1400.0001581125])


def get_CRS_in_PampaAmarilla():
    return np.array([-25832.4509, 15674.37199, 87.1192])


def get_CRS_in_UTM():
    return np.array([6114803.350, 451401.053, 1558.682])


def get_CRS_in_latlon():
    return np.array([-35.108420311, -69.533283297])




if __name__ == "__main__":
    sids = np.arange(1, 122, dtype=np.int)
    x, y, z = get_positions_local_CRS()

    sid = 62
    stations = [50, 51, 61, 62, 71, 72]
    core = get_stations_CRS()[sid]
    print "core is at station %i in pampa amarilla\n%s" % (sid, str(core + get_CRS_in_PampaAmarilla()))
    print "core is at station %i in CRS\n%s" % (sid, str(core))
    for i in stations:
        tpos = get_stations_CRS()[i]
        print "%i, %.2f, %.2f, %.2f" % (i, tpos[0] - core[0], tpos[1] - core[1], tpos[2] + get_CRS_in_UTM()[2])
    print
    print

    sid = 13
    stations = range(1, 20)
    core = get_stations_CRS()[sid]
    print "core is at station %i at %s" % (sid, str(core + get_CRS_in_PampaAmarilla()))
    for i in stations:
        tpos = get_stations_CRS()[i]
        print "%i, %.2f, %.2f, %.2f" % (i, tpos[0] - core[0], tpos[1] - core[1], tpos[2] + get_CRS_in_UTM()[2])
    print
    print
