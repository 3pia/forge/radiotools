import numpy as np
import os


def parse_long_file(filename):
    """
    pases a long CORSIKA output file
    returns:
        n_data: DEPTH, GAMMAS, POSITRONS, ELECTRONS, MU+, MU-, HADRONS, CHARGED, NUCLEI, CHERENKOV
        dE data: DEPTH, GAMMA, EM IONIZ, EM CUT, MU IONIZ, MU CUT, HADR IONIZ, HADR CUT, NEUTRINO, SUM
        parameters of Gaisser-Hillar fit N(T) = P1*((T-P2)/(P3-P2))**((P3-P2)/(P4+P5*T+P6*T**2)) * EXP((P3-T)/(P4+P5*T+P6*T**2))
        energy in electromagnetic cascade
        invisible energy (muons and neutrinos)
        energy deposit of hadrons
    """
    file = open(filename, "r")
    lines = file.readlines()
    n_steps = int(lines[0].rstrip().split()[3])
    # data = np.genfromtxt(filename, lines[2:(n_steps + 2)], skip_header=2, skip_footer=(len(lines) - n_steps + 2))
    n_data = np.genfromtxt(lines[2:(n_steps + 2)])

    import re

    def my_replace(m):
        return str(m.group(0)[0]) + " -"

    for i, line in enumerate(lines):
        lines[i] = re.sub("[0-9]-", my_replace, line)
    dE_data = np.genfromtxt(lines[(n_steps + 4):(2 * n_steps + 4)])


    dE_data = dE_data.T
    for i in xrange(1, 10):  # convert units to eV
        dE_data[i] = dE_data[i] * 1e9

    Egamma = np.sum(dE_data[1])
    Eion = np.sum(dE_data[2])
    Ecut = np.sum(dE_data[3])
    Emuion = np.sum(dE_data[4])
    Emucut = np.sum(dE_data[5])
    Ehadion = np.sum(dE_data[6])
    Ehadcut = np.sum(dE_data[7])
    Eneutrino = np.sum(dE_data[8])
    Esum = np.sum(dE_data[9])
    Eemag = Egamma + Eion + Ecut
    Eemag2 = np.sum(dE_data[1][:-2]) + np.sum(dE_data[2][:-2]) + np.sum(dE_data[3][:-2])
    Einv = Emucut + Emuion + Eneutrino
    Ehad = Ehadcut + Ehadion

    if(len(lines) > (2 * n_steps + 4 + 2)):
        p = np.genfromtxt(lines[(2 * n_steps + 4 + 2)].split()[2:])
    else:
        print("no Gaisser-Hillas fit available in CORSIKA output, probably because parallel version was used")
        print("performing Gaisser-Hillas fit...")
        from scipy import optimize
        def gaisser_hillas(X, N, X0, Xmax, p0, p1=0, p2=0):
            l = p0 + p1 * X + p2 * X ** 2
            power = (Xmax - X0) / l
            if(type(X) == float):
                if(power < 0):
                    return 0
                if(X < X0):
                    return 0
                return N * ((X - X0) / (Xmax - X0)) ** (power) * np.exp((Xmax - X) / l)
            else:
                if(np.sum(l < 0)):
                    print "some l < 0, returning infinity", X[l < 0]
                    return np.inf
                if(np.sum(power < 0)):
                    print "some  power < 0, returning infinity", X[power < 0]
                    return np.inf
                result = np.zeros_like(X)
                mask = (X - X0) >= 0
                mask = mask & (power > 0)
                result[mask] = N * ((X[mask] - X0) / (Xmax - X0)) ** (power[mask]) * np.exp((Xmax - X[mask]) / l[mask])
                result[power > 100] = 0
                if np.sum(np.isnan(result)):
                    print N, X0, Xmax, p0, p1, p2
                    print l
                    print power
                    print result
                result = np.nan_to_num(result)
                return result
        xx = dE_data[0][:-2]
        yy = dE_data[9][:-2] * 1e-9
        print "xmax = ", xx[yy.argmax()]
        popt, pcov = optimize.curve_fit(gaisser_hillas, xx, yy, p0=[yy.max(), 0, xx[yy.argmax()], 20])
        popt, pcov = optimize.curve_fit(gaisser_hillas, xx, yy, p0=[popt[0], popt[1], popt[2], popt[3], 0, 0])
        p = popt
        print p
        print("... done")
    return n_data.T, dE_data, p, Eemag, Einv, Ehad, Esum, Eemag2

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    simdir = "/net/aera/cglaser/coreas/different_atmospheres/conex/iron"
    filename = os.path.join(simdir, "180002", "DAT180002.long")

    filename = "DAT010001.long"
    n_data, dE_data, p, Eemag, Einv, Ehad, Esum, Eemag2 = parse_long_file(filename)

    P = [0]
    P.extend(p)

    Egamma = np.sum(dE_data[1])
    Eion = np.sum(dE_data[2])
    Ecut = np.sum(dE_data[3])
    Emuion = np.sum(dE_data[4])
    Emucut = np.sum(dE_data[5])
    Ehadion = np.sum(dE_data[6])
    Ehadcut = np.sum(dE_data[7])
    Eneutrino = np.sum(dE_data[8])
    Esum = np.sum(dE_data[9])
    print("Eion = %.2g eV,  Ecut = %.2g eV, Eneutrino = %.2g Emu = %.2g  Emucut = %.2g Esum = %.2g" % (Eion, Ecut, Eneutrino, Emuion, Emucut, Esum))
    print("Egamma = %.2g %.2f%%" % (Egamma, (Egamma) / Esum * 100))
    print("Eelectromagnetic = %.2g %.2f%%" % (Eion + Ecut + Egamma, (Eion + Ecut + Egamma) / Esum * 100))
    print("muon = %.2g %.2f%%" % (Emuion + Emucut, (Emuion + Emucut) / Esum * 100))
    print("hadron = %.2g %.2f%%" % (Ehadion + Ehadcut, (Ehadion + Ehadcut) / Esum * 100))
    print("neutrino = %.2g %.2f%%" % (Eneutrino, (Eneutrino) / Esum * 100))
    print("invisible = %.2g %.2f%%" % (Emuion + Emucut + Eneutrino, (Emuion + Emucut + Eneutrino) / Esum * 100))
    print("sum = %.2f" % ((Egamma + Eion + Ecut + Emuion + Emucut + Ehadion + Ehadcut + Eneutrino) / Esum * 100))

    N = lambda x: P[1] * ((x - P[2]) / (P[3] - P[2])) ** ((P[3] - P[2]) / (P[4] + P[5] * x + P[6] * x ** 2)) * np.exp((P[3] - x) / (P[4] + P[5] * x + P[6] * x ** 2))

    from StringIO import StringIO
    with open(os.path.join(".", "long.long2")) as fh:
        io = StringIO(fh.read().replace('+-', ' '))
    data = np.genfromtxt(io, unpack=True)
    X = data[0]
    gammas = data[1]
    positrons = data[3]
    electrons = data[5]
    muplus = data[7]
    muminus = data[9]

    dE_data_sum = np.empty_like(dE_data)
    for i in xrange(10):
        for j in xrange(len(dE_data[0])):
            dE_data_sum[i][j] = np.sum(dE_data[i][0:j])

    import matplotlib.cm as cm
    colors = cm.brg(np.linspace(0, 1, 9))
    alpha = 1
    markersize = 5

    if 1:
        import Pylab_Helpers as phelp
        fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        ax1.plot(dE_data[0], dE_data[1], phelp.get_marker(0), label="gamma", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[2], phelp.get_marker(1), label="$e^+/e^-$ ionisation", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[3], phelp.get_marker(2), label="$e^+/e^-$ cut", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[4], phelp.get_marker(3), label="muon ionisation", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[5], phelp.get_marker(4), label="muon cut", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[6], phelp.get_marker(5), label="hadron ionisation", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[7], phelp.get_marker(6), label="hadron cut", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[8], phelp.get_marker(7), label="neutrino", alpha=alpha, markersize=markersize)
        ax1.plot(dE_data[0], dE_data[9], phelp.get_marker(8), label="sum", alpha=alpha, markersize=markersize)
        # ax1.plot(dE_data[0], N(dE_data[0]), "k-")
        # ax1.set_xlabel("X [$g/cm^2$]")
        ax1.set_ylabel("energy deposit [eV]")
        import matplotlib.ticker as mtick
        ax1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))
        ax1.semilogy(True)
        ax1.set_ylim(1e8, 0.5e17)
        ax1.set_xlim(0, 2500)
        ax1.legend(fontsize=12)

        ax2.plot(dE_data[0], dE_data_sum[1], phelp.get_marker(0), label="gamma", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[2], phelp.get_marker(1), label="$e^+/e^-$ ionisation", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[3], phelp.get_marker(2), label="$e^+/e^-$ cut", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[4], phelp.get_marker(3), label="muon ionisation", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[5], phelp.get_marker(4), label="muon cut", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[6], phelp.get_marker(5), label="hadron ionisation", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[7], phelp.get_marker(6), label="hadron cut", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[8], phelp.get_marker(7), label="neutrino", alpha=alpha, markersize=markersize)
        ax2.plot(dE_data[0], dE_data_sum[9], phelp.get_marker(8), label="sum", alpha=alpha, markersize=markersize)
        # ax1.plot(dE_data[0], N(dE_data[0]), "k-")
        ax2.set_xlabel("X [$g/cm^2$]")
        ax2.set_ylabel("commulative energy deposit [eV]")
        import matplotlib.ticker as mtick
        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))
        ax2.set_ylim(1e8, 0.6e18)
        ax2.set_xlim(0, 2500)
        # ax2.semilogy(True)
        ax2.legend(fontsize=12)
        text = r"$E_{e^+/e^-} = %.2f\%%$ \newline" % (Eemag / Esum * 100.)
        text += r"$E_\mathrm{inv} = %.2f\%%$ \newline" % (Einv / Esum * 100.)
        text += r"$E_\mathrm{had} = %.2f\%%$" % (Ehad / Esum * 100.)
        ax2.text(0.2, 0.8, text, va="bottom")

        plt.tight_layout()


        lw = 6
        fig, ax3 = plt.subplots(1, 1, sharex=True)
        ax3.plot(dE_data[0], dE_data_sum[9], "-", color="0.4", label="sum", alpha=alpha, markersize=markersize, lw=lw)
        ax3.plot(dE_data[0], dE_data_sum[6] + dE_data_sum[7], "-c", label="hadrons", alpha=alpha, markersize=markersize, lw=lw)
        ax3.plot(dE_data[0], dE_data_sum[1] + dE_data_sum[2] + dE_data_sum[3], "--b", label="$e^+/e^-$", alpha=alpha, markersize=markersize, lw=lw)
        ax3.plot(dE_data[0], dE_data_sum[8], "--g", label="neutrino", alpha=alpha, markersize=markersize, lw=lw)
        ax3.plot(dE_data[0], dE_data_sum[4] + dE_data_sum[5], "-r", label="muons", alpha=alpha, markersize=markersize, lw=lw)
        ax3.axvline(dE_data[0][-1] + 12, ls=":", color="0.1")
        ax3.text(dE_data[0][-1] + 17, dE_data_sum[9][-1] * 0.5, "ground", rotation='vertical')
        ax3.semilogy(True)
        ax3.legend()
        ax3.set_xlabel("atmospheric depth [$g/cm^2$]")
        ax3.set_ylabel("commulative energy deposit [eV]")
        ax3.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))
        plt.tight_layout()

    if 0:
        fig, ax1 = plt.subplots(1, 1)
        ax1.plot(n_data[0], n_data[7], "ob")
        ax1.plot(dE_data[0], N(dE_data[0]), "k-")
        ax1.set_xlabel("X [$g/cm^2$]")
        ax1.set_ylabel("number of charged particles")
        import matplotlib.ticker as mtick
        ax1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))
        ax1.text(100, 3e8, "X0 = %.0f\nXmax = %.0f" % (p[1], p[2]))
    if 0:
        fig, ax1 = plt.subplots(1, 1)
        ax1.plot(X, gammas, "ob", label="gamma")
        ax1.plot(X, electrons, "og", label="e-")
        ax1.plot(X, positrons, "or", label="e+")
        ax1.plot(X, muplus, "ok", label="mu+")
        ax1.plot(X, muminus, "oy", label="mu-")
        ax1.legend()


    plt.show()
