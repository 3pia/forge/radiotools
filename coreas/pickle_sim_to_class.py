import numpy as np
import sys
from optparse import OptionParser
import cPickle
import copy
import re
from scipy.signal import hilbert
import os
from radiotools import HelperFunctions as rdhelp
import A02ParseLong as parse_long
from radiotools.CSTransformation import CSTransformation

"""
This tool parses the CORSIKA/CoREAS text output and fills it into python objects.
Each event will be one stored in one pickle file containing an coreas_event object.
This object contains the following members:
coreas_event.zenith
coreas_event.azimuth
coreas_event.energy
coreas_event.hillas
coreas_event.longprofile
coreas_event.Xground
coreas_event.antenna_position
coreas_event.traces
coreas_event.traces_short
coreas_event.times
coreas_event.times_short
coreas_event.power
coreas_event.zenith
coreas_event.magnetic_field_inclination
coreas_event.magnetic_field_declination
coreas_event.magnetic_field_strength

The coordinates are transformed to auger coordinates (x=east (phi=0deg), y=north (phi=90deg).
The time trace is bandwidth limited to 30 to 80 MHz.
The pulse maximum is rotated such that it is in the middle of the trace.
"traces_short" contains the time trace downsampled to 5ns
"""

conversion_factor_integrated_signal = 2.65441729e-3 * 6.24150934e18  # to convert V**2/m**2 * s -> J/m**2 -> eV/m**22
conversion_fieldstrength_cgs_to_SI = 2.99792458e4


class coreas_sim():
    """ Class that stores all coreas event information """
    def __init__(self, eventid):
        self.id = eventid


def ProcessData(datadir, outputfilename, event, compute_radiation_energy=True,
                lowco=30, hico=80):
    """
    lowco and hico are the lower and higher frequency cuts in MHz
    """

    coreas_event = coreas_sim(event)

    print "Processing event %s in %s" % (event, datadir)

    # check if steering folder exists
    steeringpath = os.path.join(datadir, "steering")
    steering_folder = ""
    if(os.path.exists(steeringpath) and (len(os.listdir(steeringpath)) > 0)):
        steering_folder = "steering"

    # get core position from reas file
    freas = open(os.path.join(datadir, steering_folder, 'SIM{0}.reas'.format(str(event).zfill(6))), "r")
    lreas = freas.readlines()
    freas.close()
    coreas_event.core = np.array([l.split("=")[1].split()[0] for i, l in enumerate(lreas) if l.startswith("CoreCoordinate")], dtype=np.float) * 1e-2  # convert to m
    coreas_event.n = np.array([l.split("=")[1].split()[0] for l in lreas if l.startswith("GroundLevelRefractiveIndex")], dtype=float).flatten()

    import glob
    longfile = glob.glob('{0}/DAT*.long'.format(datadir, str(event).zfill(6)))[0]
    listfile = open(os.path.join(datadir, steering_folder, 'SIM{0}.list'.format(str(event).zfill(6))))
    steerfile = os.path.join(datadir, steering_folder, 'RUN{0}.inp'.format(str(event).zfill(6)))
    longdata = np.genfromtxt(longfile, skip_header=2, skip_footer=5, usecols=(0, 2, 3))

    n_data, dE_data, hillas, Eemag, Einv, Ehad, Esum, Eemag2 = parse_long.parse_long_file(longfile)
    coreas_event.long_N = n_data
    coreas_event.long_dE = dE_data
    coreas_event.energy_emag = Eemag
    coreas_event.energy_inv = Einv
    coreas_event.energy_had = Ehad
    coreas_event.energy_sum = Esum
    coreas_event.energy_emag_atmosphere = Eemag2

    xlength = np.argmax(np.isnan(longdata[:, 0]))
    Xground = xlength * 10.0
    profile = longdata[0:xlength, :]
    longprofile = np.zeros([xlength, 3])  # The profiles have different lengths, unlikely to exceed 400... (maybe inclined events??)
    longprofile[0:xlength, :] = np.array([profile[:, 0], profile[:, 1] + profile[:, 2], profile[:, 2] - profile[:, 1]]).T

    zenith = (np.genfromtxt(re.findall("THETAP.*", open(steerfile, 'r').read()))[1]) * np.pi / 180.  # rad; CORSIKA coordinates
    azimuth = (np.genfromtxt(re.findall("PHIP.*", open(steerfile, 'r').read()))[1]) * np.pi / 180.  # rad; CORSIKA coordinates
    azimuth = 3 * np.pi / 2. + azimuth  # convert to auger convention +x = east (phi=0), +y = north (phi=90)
    energy = np.genfromtxt(re.findall("ERANGE.*", open(steerfile, 'r').read()))[1]  # GeV
    Bx, Bz = np.genfromtxt(re.findall("MAGNET.*", open(steerfile, 'r').read()))[1:3]
    B_inclination = np.arctan2(Bz, Bx)
    B_declination = 0
    B_strength = (Bx ** 2 + Bz ** 2) ** 0.5
    magnetic_field_vector = rdhelp.SphericalToCartesian(np.pi * 0.5 + B_inclination, B_declination + np.pi * 0.5)  # in auger cooordinates north is + 90 deg
    ctrans = CSTransformation(zenith, azimuth, magnetic_field=magnetic_field_vector)

    lines = listfile.readlines()
    lines = [item for item in lines if item != "\n"]  # skip empty lines
    lines = [item for item in lines if not item.startswith("#")]  # skip comments
    lines = np.array(lines)

    names = [x.split(" ")[5] for x in lines]
    if len(lines[0].split("_")) < 4:
        planes = np.array(["na_na"] * len(names))
    elif len(lines[0].split("_")) == 4:
        planes = np.array([x.rstrip().split("_")[3] + "_na" for x in names])
    elif len(lines[0].split("_")) >= 4:
        planes = np.array([x.rstrip().split("_")[3] + "_" + x.rstrip().split("_")[4] for x in names])
    planes_unique = np.unique(planes)

    print "\tobservation planes = ", planes_unique

    observation_levels = {}
    observation_levels_small = {}
    for plane in planes_unique:
        observation_height = copy.copy(coreas_event.core[2])
        try:
            observation_height = float(plane.split("_")[0])
        except:
            pass
        observation_level = {}
        observation_level_small = {}

        mask_plane = (planes == plane)
        nantennas = np.sum(mask_plane)
        print "\t%i antennas in observation plane %s" % (nantennas, plane)

        antenna_position = np.zeros([nantennas, 3])
        antenna_heights = np.zeros(nantennas)
        power = np.zeros([nantennas, 3])
        power_showerplane = np.zeros([nantennas, 3])
        peak_time = np.zeros([nantennas, 3])
        peak_amplitude = np.zeros([nantennas, 3])
        peak_amplitude_total = np.zeros(nantennas)
        polarization_vector = np.zeros([nantennas, 3])
        traces = []
        times = []
        times_short = []
        traces_short = []
        gamma_cuts = []
        for j in np.arange(nantennas):

            antenna_position[j] = (lines[mask_plane][j].split(" ")[2:5])
            antenna_file = lines[mask_plane][j].split(" ")[5]
            try:
                antenna_heights[j] = float(antenna_file.split("_")[4])  # hight might not be part of the antenna name, only relevant for simulations with observers at multiple heights
            except:
                pass
            coreasfile = '{0}/SIM{1}_coreas/raw_{2}.dat'.format(datadir, str(event).zfill(6), antenna_file.rstrip())  # drop the \n from the string!
            try:
                data = np.genfromtxt(coreasfile)
            except:
                print "No coreas file", coreasfile
                continue

            # read gamma cuts
            line_split = lines[mask_plane][j].split(" ")
            gamma_pos = [item for item in range(len(line_split)) if (line_split[item] == 'gamma' or line_split[item] == 'GAMMA')]
            gamma_cuts_temp = []
            for iG in gamma_pos:
                gamma_cuts_temp.append([float(line_split[iG + 1]), float(line_split[iG + 2])])
            gamma_cuts.append(gamma_cuts_temp)

            # convert CORSIKA to AUGER coordinates (AUGER y = CORSIKA x, AUGER x = - CORSIKA y
            temp = np.copy(data)
            data[:, 1], data[:, 2] = -temp[:, 2], temp[:, 1]

            # convert to SI units
            data[:, 1] *= conversion_fieldstrength_cgs_to_SI
            data[:, 2] *= conversion_fieldstrength_cgs_to_SI
            data[:, 3] *= conversion_fieldstrength_cgs_to_SI

            temp = np.copy(antenna_position[j])
            antenna_position[j, 0], antenna_position[j, 1], antenna_position[j, 2] = -temp[1] / 100., temp[0] / 100., temp[2] / 100.

            dlength = data.shape[0]
            if np.sum(np.isnan(data[:, 1:4])):
                print "ERROR in antenna %i, time trace contains NaN" % j
                import sys
                sys.exit(-1)
            spec = np.fft.rfft(data[:, 1:4], axis=-2)

            tstep = data[1, 0] - data[0, 0]
            freqhi = 0.5 / tstep / 1e6  # MHz
            freqstep = freqhi / (dlength / 2 + 1)  # MHz

            fb = int(np.floor(lowco / freqstep))
            lb = int(np.floor(hico / freqstep) + 1)
            window = np.zeros([1, dlength / 2 + 1, 1])
            window[0, fb:lb + 1, 0] = 1
            pow0 = np.abs(spec[:, 0]) * np.abs(spec[:, 0]) / dlength * 2  # correct normalization of fft
            pow1 = np.abs(spec[:, 1]) * np.abs(spec[:, 1]) / dlength * 2
            pow2 = np.abs(spec[:, 2]) * np.abs(spec[:, 2]) / dlength * 2

            power[j] = np.array([np.sum(pow0[fb:lb + 1]), np.sum(pow1[fb:lb + 1]), np.sum(pow2[fb:lb + 1])]) * tstep * conversion_factor_integrated_signal
            # assume that simulated time resolution is higher than LOFAR time resolution (t_step=5 ns)
            filtered_spec = np.array([spec[0:dlength, 0] * window[0, 0:dlength, 0], spec[0:dlength, 1] * window[0, 0:dlength, 0], spec[0:dlength, 2] * window[0, 0:dlength, 0]])
            filt = np.fft.irfft(filtered_spec, axis=-1)

            t_step = 5e-9
            maxfreqbin = int(np.floor(tstep / t_step * dlength / 2.) + 1)
            shortspec = np.array([spec[0:maxfreqbin, 0] * window[0, 0:maxfreqbin, 0], spec[0:maxfreqbin, 1] * window[0, 0:maxfreqbin, 0], spec[0:maxfreqbin, 2] * window[0, 0:maxfreqbin, 0]])
            filt_short = np.fft.irfft(shortspec, axis=-1)

            polarization_vector[j] = rdhelp.get_polarization_vector_FWHM(filt)

            # roll trace to have its maximum in the middle. Due to the bandpass filter, the pulse extends to the end of the trace
            tt = data[:, 0] * 1e9
            filt = np.roll(filt, np.int(0.5 * len(tt)), axis=-1)
            hilbenv = np.abs(hilbert(filt, axis=-1))
            peak_time[j] = np.argmax(hilbenv, axis=-1)
            peak_amplitude[j] = np.max(hilbenv, axis=-1)
            peak_amplitude_total[j] = np.sum(peak_amplitude[j] ** 2) ** 0.5
            peak_time_sum = tt[np.argmax(np.sum(hilbenv ** 2, axis=0) ** 0.5)]

            # calculate energy density (signal window = 200ns, subtract influence of noise)
            mask_nonoise = (tt > (peak_time_sum - 100)) & (tt < (peak_time_sum + 100))
            mask_noise = ~mask_nonoise
            u_signal = np.sum(filt[..., mask_nonoise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep
            u_noise = np.sum(filt[..., mask_noise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep * np.sum(mask_nonoise) / np.sum(mask_noise)
            power[j] = u_signal - u_noise

            # calculate power in shower plane
            trace_showerplane = ctrans.transform_to_vxB_vxvxB(filt.T).T
            u_signal = np.sum(trace_showerplane[..., mask_nonoise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep
            u_noise = np.sum(trace_showerplane[..., mask_noise] ** 2, axis=-1) * conversion_factor_integrated_signal * tstep * np.sum(mask_nonoise) / np.sum(mask_noise)
            power_showerplane[j] = u_signal - u_noise

            times.append(data[:, 0])
            traces.append(filt)
            times_short.append(t_step * np.arange(len(filt_short[0])))
            traces_short.append(filt_short)

        if(len(traces) == 0):  # skip empty events
            return 0

        # shift antenna positions to core position of observation level
        # compute translation in x and y
        r = np.tan(zenith) * (observation_height - coreas_event.core[2])
        deltax = np.cos(azimuth) * r
        deltay = np.sin(azimuth) * r
        antenna_position[..., 0] -= deltax
        antenna_position[..., 1] -= deltay
        if r > 1:
            print "\tshifting antenna positions by x = %.1f, y =  %.1f" % (deltax, deltay)

        # calculate radiation enrergy
        core = copy.copy(coreas_event.core)
        core[2] = observation_height
        cs = CSTransformation(zenith, azimuth, core, magnetic_field=magnetic_field_vector)
        station_positions_transformed = cs.transform_to_vxB_vxvxB(antenna_position)
        xx = station_positions_transformed[..., 0]
        yy = station_positions_transformed[..., 1]
        distances = (xx ** 2 + yy ** 2) ** 0.5
        tot_power = np.sum(power, axis=1)

        az = np.round(np.rad2deg(np.arctan2(yy, xx)))
        az[az < 0] += 360
        az[az >= 360] -= 360
        mask = (az == 90) | (az == 270)

        if np.sum(mask) > 5:
            dd = distances[mask]
            yy_tot = tot_power[mask]
            from scipy import integrate
            print dd
            y_int_num = integrate.cumtrapz(yy_tot * dd, dd)[-1] * 2 * np.pi
            observation_level_small['radiation_energy_1D'] = y_int_num

        if np.array_equal(np.unique(az), np.arange(0, 360, 45.)):
            import scipy.interpolate as intp
            func = intp.Rbf(xx, yy, tot_power, smooth=0, function='quintic')
            x_range = xx.max()
            y_range = yy.max()
            opts = {'epsabs': 1., 'epsrel': 0.5e-3, 'limit': 1000}

            def bounds_y(x):
                return [-np.sqrt(np.abs(np.square(x_range) - np.square(x))), np.sqrt(np.abs(np.square(y_range) - np.square(x)))]

            def bounds_x():
                return [-x_range, y_range]

            radiation_energy_2D = integrate.nquad(func, [bounds_y, bounds_x], opts=opts)
            observation_level_small['radiation_energy'] = radiation_energy_2D
            print "\tcalculating radiation energy via 2D integration: %.4e eV" % (radiation_energy_2D[0]) 

        times = np.array(times)
        times_short = np.array(times_short)
        traces = np.array(traces)
        traces_short = np.array(traces_short)

        observation_level['times'] = times
        observation_level['times_short'] = times_short
        observation_level['traces'] = traces
        observation_level['traces_short'] = traces_short

        observation_level_small['antenna_position'] = antenna_position
        observation_level_small['core'] = core
        observation_level_small['orientation'] = plane.split("_")[1]
        observation_level_small['polarization_vector'] = polarization_vector
        observation_level_small['power'] = power
        observation_level_small['power_showerplane'] = power_showerplane
        observation_level_small['energy_fluence'] = np.sum(power, axis=1)
        observation_level_small['amplitude'] = peak_amplitude
        observation_level_small['amplitude_total'] = peak_amplitude_total
        observation_level_small['gamma_cuts'] = np.array(gamma_cuts)

        observation_levels[plane] = observation_level
        observation_levels_small[plane] = observation_level_small

    coreas_event.zenith = zenith
    coreas_event.azimuth = azimuth
    coreas_event.energy = energy * 1e9  # in eV
    coreas_event.hillas = hillas
    coreas_event.longprofile = longprofile
    coreas_event.Xground = Xground
    coreas_event.magnetic_field_inclination = B_inclination
    coreas_event.magnetic_field_declination = B_declination
    coreas_event.magnetic_field_strength = B_strength

    coreas_event.obs_levels = observation_levels_small.copy()

    pickfile = open(outputfilename[:-4] + "_small.dat", 'w')
    cPickle.dump(coreas_event, pickfile)

    coreas_event.obs_levels.update(observation_levels)

    pickfile = open(outputfilename, 'w')
    cPickle.dump(coreas_event, pickfile)
    # cPickle.dump((zenith, azimuth, energy, hillas, longprofile, Xground, firstInt, antenna_position, power, power11, power21, power41, peak_time, peak_amplitude, particle_radius, energy_deposit), pickfile)

if __name__ == "__main__":
    max_sub_simulations = 20
    # Parse commandline options
    parser = OptionParser()
    parser.add_option("-d", "--directory", default="/vol/astro3/auger/aera_sim/from_aachen_upwards/", help="Input directory")
    parser.add_option("-o", "--output", default="./", help="Output directory")
    parser.add_option("-c", "--submit_vispa_condor", action="store_true", dest="submit_vispa_condor", help="Should conversion of each file be submitted as condor job?")
    parser.add_option("-s", "--single", action="store_true", dest="single", help="Convert single file only")
    parser.add_option("-j", "--johannes", action="store_true", dest="johannes", help="")
    parser.add_option("--flow", type="float", default=30., help="lower frequency cut in MHz")
    parser.add_option("--fup", type="float", default=80., help="upper frequency cut in MHz")
    parser.add_option("--norad", action="store_true", dest="compute_radiation_energy", help="Do not compute radiation energy?")
    parser.add_option("--particle-type", default="xx", help="primary particle identifier (e.g. \"p\" or \"Fe\")")

    (options, args) = parser.parse_args()

    if not os.path.exists(options.output):
        os.mkdir(options.output)

    calculate_erad = not (options.compute_radiation_energy == True)

    if(options.single is True):  # convert only single simulation
        directory, event = os.path.split(os.path.abspath(options.directory))
        if(os.path.split(directory)[1] == 'proton'):
            filename = os.path.join(options.output, 'auger_{1}_{2}_p_{0}.dat'.format(event, int(options.flow), int(options.fup)))
        elif(os.path.split(directory)[1] == 'iron'):
            filename = os.path.join(options.output, 'auger_{1}_{2}_Fe_{0}.dat'.format(event, int(options.flow), int(options.fup)))
        else:
            filename = os.path.join(options.output, 'auger_{1}_{2}_{3}_{0}.dat'.format(event, int(options.flow), int(options.fup), options.particle_type))
        directory = os.path.join(directory, event)
        print "converting event %s in directory %s to %s" % (event, directory, filename)
        ProcessData(directory, filename, event, calculate_erad,
                    lowco=options.flow, hico=options.fup)
    else:  # convert all simulations in proton and iron directory
        if(os.path.isdir(os.path.join(options.directory, 'proton'))):
            print "Processing proton simulations in ", options.directory
            dir_list = os.listdir(os.path.join(options.directory, 'proton'))
            dir_list.sort()
            for event in dir_list:
                proton_dir = os.path.join(options.directory, 'proton', str(event))
                if(options.johannes):
                    for i in xrange(1, max_sub_simulations):
                        proton_file = os.path.join(options.output, 'auger_{2}_{3}_p_{0}_{1}.dat'.format(event, i, int(options.flow), int(options.fup)))
                        if os.path.exists(proton_file):
                            print("pickle file %s already exists" % proton_file)
                            continue
                        if(options.submit_vispa_condor):
                            import subprocess
                            scriptpath = os.path.abspath(__file__)
                            print "submit_python", scriptpath, "-d", proton_dir, "-o", options.output, "-s"
                            subprocess.call(["/home/public/software/scripts/submit_python.py", scriptpath, "-d", proton_dir, "-o", options.output, "-s"])
                        else:
                            # ProcessData(proton_dir, proton_file, i)
                            try:
                                ProcessData(proton_dir, proton_file, i, calculate_erad,
                                            lowco=options.flow, hico=options.fup)
                            except:
                                print "Failed at event", event, i
                                print sys.exc_info()[0]
                                continue
                else:
                    proton_file = os.path.join(options.output, 'auger_{1}_{2}_p_{0}.dat'.format(event, int(options.flow), int(options.fup)))
                    if os.path.exists(proton_file):
                        print("pickle file %s already exists" % proton_file)
                        continue
                    if(options.submit_vispa_condor):
                        import subprocess
                        scriptpath = os.path.abspath(__file__)
                        print "submit_python", scriptpath, "-d", proton_dir, "-o", options.output, "-s"
                        subprocess.call(["/home/public/software/scripts/submit_python.py", scriptpath, "-d", proton_dir, "-o", options.output, "-s"])
                    else:
                        try:
                            ProcessData(proton_dir, proton_file, event, calculate_erad,
                                        lowco=options.flow, hico=options.fup)
                        except:
                            print "Failed at event", event
                            print sys.exc_info()[0]
                            continue

        if(os.path.isdir(os.path.join(options.directory, 'iron'))):
            print "Processing iron simulations in ", options.directory
            dir_list = os.listdir(os.path.join(options.directory, 'iron'))
            dir_list.sort()
            for event in dir_list:
                iron_dir = os.path.join(options.directory, 'iron', str(event))
                if(options.johannes):
                    for i in xrange(1, max_sub_simulations):
                        iron_file = os.path.join(options.output, 'auger_{2}_{3}_Fe_{0}_{1}.dat'.format(event, i, int(options.flow), int(options.fup)))
                        if os.path.exists(iron_file):
                            print("pickle file %s already exists" % iron_file)
                            continue
                        if(options.submit_vispa_condor):
                            scriptpath = os.path.abspath(__file__)
                            subprocess.call(["/home/public/software/scripts/submit_python.py", scriptpath, "-d", iron_dir, "-o", options.output, "-s"])
                        else:
                            try:
                                ProcessData(iron_dir, iron_file, i, calculate_erad,
                                            lowco=options.flow, hico=options.fup)
                            except:
                                print "Failed at event", event
                                print sys.exc_info()[0]
                                continue
                else:
                    iron_file = os.path.join(options.output, 'auger_{1}_{2}_Fe_{0}.dat'.format(event, int(options.flow), int(options.fup)))
                    if os.path.exists(iron_file):
                        print("pickle file %s already exists" % iron_file)
                        continue
                    if(options.submit_vispa_condor):
                        scriptpath = os.path.abspath(__file__)
                        subprocess.call(["/home/public/software/scripts/submit_python.py", scriptpath, "-d", iron_dir, "-o", options.output, "-s"])
                    else:
                        try:
                            ProcessData(iron_dir, iron_file, event, calculate_erad,
                                        lowco=options.flow, hico=options.fup)
                        except:
                            print "Failed at event", event
                            print sys.exc_info()[0]
                            continue
