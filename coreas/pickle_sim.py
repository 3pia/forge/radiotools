import numpy as np
from optparse import OptionParser
import matplotlib.pyplot as plt
import cPickle
import re
from ROOT import TFile, TTree
from scipy.signal import hilbert
import os
import glob
import sys

# Parse commandline options
parser = OptionParser()
parser.add_option("-d", "--directory", default="/vol/astro3/auger/aera_sim/from_aachen_upwards/", help="Input directory")
parser.add_option("-o", "--output", default="./", help="Output directory")

(options, args) = parser.parse_args()


def ProcessData(datadir, outputfilename, event):

    nevents = 1
    i = 0
    lowco = 30
    hico = 80
    nantennas = 160
    zenith = np.zeros([nevents])
    azimuth = np.zeros([nevents])
    energy = np.zeros([nevents])
    hillas = np.zeros([6, nevents])
    longprofile = np.zeros([400, 3, nevents])  # The profiles have different lengths, unlikely to exceed 200... (maybe inclined events??)
    Xground = np.zeros([nevents], dtype=int)  # The X value that corresponds to ground level
    antenna_position = np.zeros([nevents, nantennas, 3])
    power = np.zeros([nevents, nantennas, 3])
    power11 = np.zeros([nevents, nantennas, 3])
    power21 = np.zeros([nevents, nantennas, 3])
    power41 = np.zeros([nevents, nantennas, 3])
    peak_time = np.zeros([nevents, nantennas, 3])
    peak_amplitude = np.zeros([nevents, nantennas, 3])
    particle_radius = np.zeros([nevents, 200])
    energy_deposit = np.zeros([nevents, 200])
    firstInt = np.zeros([nevents])

    print "Processing event #", event

    longfile = '{0}/DAT{1}.long'.format(datadir, str(event).zfill(6))
    listfile = open('{0}/steering/SIM{1}.list'.format(datadir, str(event).zfill(6)))
    steerfile = '{0}/steering/RUN{1}.inp'.format(datadir, str(event).zfill(6))
    try:
        print longfile
        longdata = np.genfromtxt(longfile, skip_header=2, skip_footer=5, usecols=(0, 2, 3))
    except:
        print "No long data"
        return 1
    xlength = np.argmax(np.isnan(longdata[:, 0]))
    Xground[i] = xlength * 10.0
    profile = longdata[0:xlength, :]
    longprofile = np.zeros([400, 3])  # The profiles have different lengths, unlikely to exceed 400... (maybe inclined events??)
    longprofile[0:xlength, :] = np.array([profile[:, 0], profile[:, 1] + profile[:, 2], profile[:, 2] - profile[:, 1]]).T
    hillas[:, i] = np.genfromtxt(re.findall("PARAMETERS.*", open(longfile, 'r').read()))[2:]

    zenith = (np.genfromtxt(re.findall("THETAP.*", open(steerfile, 'r').read()))[1]) * np.pi / 180.  # rad; CORSIKA coordinates
    azimuth = (np.genfromtxt(re.findall("PHIP.*", open(steerfile, 'r').read()))[1]) * np.pi / 180.  # rad; CORSIKA coordinates
    energy = np.genfromtxt(re.findall("ERANGE.*", open(steerfile, 'r').read()))[1]  # GeV

    lines = listfile.readlines()

    for j in np.arange(nantennas):
        antenna_position[i, j] = (lines[j].split(" ")[2:5])
        antenna_file = lines[j].split(" ")[5]
        coreasfile = '{0}/SIM{1}_coreas/raw_{2}.dat'.format(datadir, str(event).zfill(6), antenna_file[:-1])  # drop the \n from the string!
        try:
            data = np.genfromtxt(coreasfile)
        except:
            print "No coreas file", coreasfile
            continue

        dlength = data.shape[0]
        poldata = np.ndarray([dlength, 2])
        az_rot = 3 * np.pi / 2 + azimuth
        zen_rot = zenith
        poldata[:, 0] = -1.0 / np.sin(zen_rot) * data[:, 3]  # -1/sin(theta) *z
        poldata[:, 1] = np.sin(az_rot) * data[:, 2] + np.cos(az_rot) * data[:, 1]  # -sin(phi) *x + cos(phi)*y in coREAS 0=positive y, 1=negative x

#         d = data[:,1:4]
        spec = np.fft.rfft(data[:, 1:4], axis=-2)

        tstep = data[1, 0] - data[0, 0]
        freqhi = 0.5 / tstep / 1e6  # MHz
        freqstep = freqhi / (dlength / 2 + 1)  # MHz

        fb = np.floor(lowco / freqstep)
        lb = np.floor(hico / freqstep) + 1
        window = np.zeros([1, dlength / 2 + 1, 1])
        window[0, fb:lb + 1, 0] = 1
        pow0 = np.abs(spec[:, 0]) * np.abs(spec[:, 0])
        pow1 = np.abs(spec[:, 1]) * np.abs(spec[:, 1])
        pow2 = np.abs(spec[:, 2]) * np.abs(spec[:, 2])

        power[i, j] = np.array([np.sum(pow0[fb:lb + 1]), np.sum(pow1[fb:lb + 1]), np.sum(pow2[fb:lb + 1])])
        # assume that simulated time resolution is higher than LOFAR time resolution (t_step=5 ns)
        maxfreqbin = np.floor(tstep / 5e-9 * dlength / 2.) + 1
        shortspec = np.array([spec[0:maxfreqbin, 0] * window[0, 0:maxfreqbin, 0], spec[0:maxfreqbin, 1] * window[0, 0:maxfreqbin, 0], spec[0:maxfreqbin, 2] * window[0, 0:maxfreqbin, 0]])
        filt = np.fft.irfft(shortspec, axis=-1)
        hilbenv = np.abs(hilbert(filt, axis=-1))
        peak_time[i, j] = np.argmax(hilbenv, axis=-1)
        peak_amplitude[i, j] = np.max(hilbenv, axis=-1)
        if (peak_amplitude[i, j, 0] > peak_amplitude[i, j, 1]):
            pt = peak_time[i, j, 0]
        else:
            pt = peak_time[i, j, 1]
        d = filt.shape[1]
        rng = 5
        a = np.max([0, pt - rng])
        b = pt + rng + 1
        c = np.min([d, pt + d - rng])
        power11[i, j] = np.sum(np.square(filt[:, a:b]), axis=-1) + np.sum(np.square(filt[:, c:d]), axis=-1)
        rng = 10
        a = np.max([0, pt - rng])
        b = pt + rng + 1
        c = np.min([d, pt + d - rng])
        power21[i, j] = np.sum(np.square(filt[:, a:b]), axis=-1) + np.sum(np.square(filt[:, c:d]), axis=-1)
        rng = 20
        a = np.max([0, pt - rng])
        b = pt + rng + 1
        c = np.min([d, pt + d - rng])
        power41[i, j] = np.sum(np.square(filt[:, a:b]), axis=-1) + np.sum(np.square(filt[:, c:d]), axis=-1)
#      try:
#         pdata = np.genfromtxt(lorafile)
#         particle_radius[i,:]=5*pdata[:,0]+2.5
#         energy_deposit[i,:]=pdata[:,1]
#      except:
# 	 particle_radius[i,:] = 0.
#         energy_deposit[i,:] = 0.

# convert CORSIKA to AUGER coordinates (AUGER y = CORSIKA x, AUGER x = - CORSIKA y


    temp = np.copy(antenna_position)
    antenna_position[:, :, 0], antenna_position[:, :, 1], antenna_position[:, :, 2] = -temp[:, :, 1] / 100., temp[:, :, 0] / 100., temp[:, :, 2] / 100.
    temp = np.copy(peak_amplitude)
    peak_amplitude[:, :, 0], peak_amplitude[:, :, 1] = -temp[:, :, 1], temp[:, :, 0]
    temp = np.copy(peak_time)
    peak_time[:, :, 0], peak_time[:, :, 1] = temp[:, :, 1], temp[:, :, 0]
    azimuth = 3 * np.pi / 2 + azimuth  #  +x = east (phi=0), +y = north (phi=90)
    pickfile = open(outputfilename, 'w')
    cPickle.dump((zenith, azimuth, energy, hillas, longprofile, Xground, firstInt, antenna_position, power, power11, power21, power41, peak_time, peak_amplitude, particle_radius, energy_deposit), pickfile)


# Aachen upwards events
dir_list = os.listdir(options.directory + '/proton')

for event in dir_list:
    print event
    proton_dir = options.directory + '/proton/' + str(event)

    proton_file = 'auger_30_80_p_{0}.dat'.format(event)
    print "Processing proton simulations"
    try:
        ProcessData(proton_dir, proton_file, event)
    except:
#
        print "Failed at event", event
        continue

dir_list = os.listdir(options.directory + '/iron/')
for event in dir_list:
    print event
    iron_dir = options.directory + '/iron/' + str(event)

    iron_file = 'auger_30_80_Fe_{0}.dat'.format(event)
    print "Processing iron simulations"
    try:
        ProcessData(iron_dir, iron_file, event)
    except:
#
        print "Failed at event", event
        continue
