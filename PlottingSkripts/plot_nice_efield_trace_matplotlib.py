#!/usr/bin/env python
# -*- coding: utf-8 -*-

from convertEnums import *
import os
import sys
from scipy import signal
import HelperFunctions as hp
import ROOT
import matplotlib.pyplot as plt
import numpy as np


def GetRMS(trace):
    rms = 0
    N = 0
    for val in trace:
        rms += val ** 2
        N += 1
    return np.sqrt(rms / N)


filename = sys.argv[1]
# iteration = sys.argv[2]

plothilbert = 1
plot = 1
upsampling = 10.

ROOT.gSystem.Load("$AUGEROFFLINEROOT/lib/libRecEventKG.so")

file = ROOT.RecEventFile(str(filename))
event = ROOT.RecEvent()
file.SetBuffers(event)

while file.ReadNextEvent() == ROOT.RecEventFile.eSuccess:

    rEvent = event.GetRdEvent()
    eventId = rEvent.GetRdEventId()
    rShower = rEvent.GetRdRecShower()
    stations = rEvent.GetRdStationVector()
    event_id = rEvent.GetRdEventId()
    print "Event: ", event_id
    if(len(stations) <= 2):
        continue
#     if (event_id != 492420): # plot for energy paper
#         continue

#     if(event_id != 41998):
#         continue
    for station in stations:
        station_id = station.GetId()
#         if(station_id != 112):
#             continue
        if(not station.HasPulse()):
            continue
        if(station.GetParameter(rdstQ.eSignalToNoise) < 300):
            print "skipping SNR"
            continue
        print "Event: ", rEvent.GetRdEventId(), "station ", station_id
        sampling_rate = station.GetRdTrace(0).GetSamplingRate()
        #min_bin = station.GetParameter(rdstQ.eSignalSearchWindowStart) * sampling_rate
        max_bin = np.int((station.GetParameter(rdstQ.eSignalTime) - station.GetParameter(rdstQ.eTraceStartTime)) / sampling_rate)
        n_samples = 1000
        print max_bin
        vEX = np.array(station.GetRdTimeTrace(0))[max_bin - n_samples / 2:max_bin + n_samples / 2]
        vEY = np.array(station.GetRdTimeTrace(1))[max_bin - n_samples / 2:max_bin + n_samples / 2]
        vEZ = np.array(station.GetRdTimeTrace(2))[max_bin - n_samples / 2:max_bin + n_samples / 2]
        vEMag = np.sqrt(vEX ** 2 + vEY ** 2 + vEZ ** 2)

        vEXHilbert = abs(signal.hilbert(vEX))
        vEYHilbert = abs(signal.hilbert(vEY))
        vEZHilbert = abs(signal.hilbert(vEZ))
        vEHilbertMag = abs(np.array(np.sqrt(vEXHilbert ** 2 + vEYHilbert ** 2 + vEZHilbert ** 2)))

        n_samples = vEX.size
        times = np.linspace(0, n_samples / sampling_rate, n_samples)
        # times =
        fig, ax = plt.subplots(1, 1, figsize=(10, 5))
        markersize = 6
        ax.plot(times, vEX, "-k", label="east-west", markersize=markersize)
        ax.plot(times, vEY, "-r", label="north-south", markersize=markersize)
        ax.plot(times, vEZ, "-b", label="vertical", markersize=markersize)
        ax.plot(times, vEHilbertMag, "--k", label="Hilbert envelope")
        hilbert_max = vEHilbertMag.max()
        hilbert_max_pos = np.squeeze(vEHilbertMag.argmax())
        ylim = (-hilbert_max, hilbert_max * 1.3)
        # ax.plot([times[hilbert_max_pos], times[hilbert_max_pos]], [ylim[0], ylim[1]], "--r")
        FWHM1 = 0
        FWHM2 = 999999999
        tmp = 0
        for i, val in enumerate(vEHilbertMag[:hilbert_max_pos][::-1]):
            if val < hilbert_max / 2:
                FWHM1 = hilbert_max_pos - i
                break
        for i, val in enumerate(vEHilbertMag[hilbert_max_pos:]):
            if val < hilbert_max / 2:
                FWHM2 = hilbert_max_pos + i
                break
        print FWHM1, FWHM2
        xmin = max(0, FWHM1)
        xmax = min(len(times) - 1, FWHM2)
        xlim = (times[xmin] - 20, times[xmax] + 50)
        ax.annotate('pulse maximum', xy=(times[hilbert_max_pos], hilbert_max), xycoords='data', fontsize='large',
                xytext=(5 + times[hilbert_max_pos], ylim[1] * 0.95), textcoords='data', color="red",
                arrowprops=dict(color='red', shrink=0.05, frac=0.4, width=3),
                horizontalalignment='left', verticalalignment='top',)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        # ax.axvspan(times[hilbert_max_pos] - 100, times[hilbert_max_pos] + 100, facecolor='b', alpha=0.2, label="")
        ax.set_xlabel("time [ns]", fontsize=20)
        ax.set_ylabel("electric field [$\mu V/m$]", fontsize=20)
        import matplotlib.ticker as ticker
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        minorLocator = MultipleLocator(5)
        # ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.1i'))
        ticks = np.round(np.arange(xlim[0], xlim[1], 20), -1)
        if(ticks[0] % 20):
            ticks += 10
        print ticks
        ax.set_xticks(ticks)
        ax.xaxis.set_minor_locator(minorLocator)
        ax.yaxis.get_major_formatter().set_powerlimits((0, 1000))
        ax.legend(loc='upper right', numpoints=1)
        plt.tight_layout()
        plt.show()


