#!/usr/bin/env python
# -*- coding: utf-8 -*-


import ROOT
from ROOT import TCanvas
from ROOT import TIter
from ROOT import TGraph
from ROOT import TVector3
from ROOT import TRandom3
from numpy import fft
import copy
import numpy as np
import math
from scipy import constants
from scipy import signal
import os
import sys
import ast
import HelperFunctions as hp

from pxl import core, modules


class Example(modules.PythonModule):

    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        # self._exampleVariable = startValue

    def initialize(self, module):
        self.__module = module
        self._logger = core.Logger(self.__module.getName())
        ''' Initialize module sinks, sources and options '''

#        self.__module.addSink("in", "Input port")

#        self.__module.addSource("signal", "Signal port 1")
#        self.__module.addSource("signal2", "Signal port 2")

#        self.__module.addSource("out", "Output port")
        #===============================================================================
        # module options
        #===============================================================================
        self.__module.addOption("ADST", "path to ADST file to get detector geometry", "ADST.root", 1)
        self.__module.addOption("wavefront", "which wavefront model should be used to calculate station timing (choose between plane, spherical, cone)", "plane")
        self.__module.addOption("output folder", "folder where the simulated efield traces are saved", ".", 1)
        self.__module.addOption("run number", "run number from which simulations are counted", "1")
        self.__module.addOption("skip existing files", "skips all existing files", True)

        #----------------------------------------------------------- shower core
        self.__module.addOption("N core positions", "number of varied core positions for each run", "1")
        self.__module.addOption("core x error", "uncertainty of shower core", "0")
        self.__module.addOption("core y error", "uncertainty of shower core", "0")
        self.__module.addOption("core z error", "uncertainty of shower core", "0")

        #----------------------------------------------------------------- noise
        self.__module.addOption("noise", "add noise to signal trace?", False)
        self.__module.addOption("SNR", "list of signal to noise ratios ", "[10,20]")
        self.__module.addOption("noise library", "path to noise library", "./noise", 1)

        #-------------------------------------------------------------- gemoetry
        self.__module.addOption("antennas", "list of antenna ids that should be simulated ", "[101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121]")

        self.__module.addOption("B-field zenith", "zenith angle of magnetic field vector in degree", "54.4")
        self.__module.addOption("B-field azimuth", "azimuth angle of magnetic field vector in degree", "87.3")

        #------------------------------------------------- air shower properties
        self.__module.addOption("zenith", "list of zenith angles of the shower axis in degree", "[10, 30, 50]")
        self.__module.addOption("azimuth", "list of azimuth angles of the shower axis in degree", "[270, 300, 315, 330, 360]")
        self.__module.addOption("radius", "list of distances from core position to source point (in case of spherical wavefront model)", "[10000]")
        self.__module.addOption("rho", "list of cone opening angles in degree (in case of cone wavefront model)", "[0]")

        #------------------------------------------------- time uncertainty
        self.__module.addOption("time uncertainty", "time uncertainty in nanoseconds", "0")

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        self._logger.log(core.LOG_LEVEL_INFO, "Begin job")
        print "beginJob"

        #=======================================================================
        # get module options
        #=======================================================================
        self.__ADST_path = self.__module.getOption("ADST")
        self.__wavefrontmodel = self.__module.getOption("wavefront")
        self.__output_folder = self.__module.getOption("output folder")
        self.__run_number = int(self.__module.getOption("run number"))
        self.__skip_existing_files = self.__module.getOption("skip existing files")

        #----------------------------------------------------------- shower core
        self.__N_core_positions = int(self.__module.getOption("N core positions"))
        self.__core_x_error = float(self.__module.getOption("core x error"))
        self.__core_y_error = float(self.__module.getOption("core y error"))
        self.__core_z_error = float(self.__module.getOption("core z error"))

        #----------------------------------------------------------------- noise
        self.__noise = self.__module.getOption("noise")
        self.__SNR_inits = np.array(ast.literal_eval(self.__module.getOption("SNR")))
        self.__noise_library_path = self.__module.getOption("noise library")

        self.__magnetic_field_zenith = np.deg2rad(float(self.__module.getOption("B-field zenith")))
        self.__magnetic_field_azimuth = np.deg2rad(float(self.__module.getOption("B-field azimuth")))

        self.__antennas = np.array(ast.literal_eval(self.__module.getOption("antennas")), dtype=np.int)
        self.__N_antennas = self.__antennas.size

        #------------------------------------------------- air shower properties
        self.__zenith_inits = np.deg2rad(np.array(ast.literal_eval(self.__module.getOption("zenith"))))
        self.__azimuth_inits = np.deg2rad(np.array(ast.literal_eval(self.__module.getOption("azimuth"))))
        self.__radius_inits = np.array(ast.literal_eval(self.__module.getOption("radius")))
        self.__rho_inits = np.deg2rad(np.array(ast.literal_eval(self.__module.getOption("rho"))))

        #------------------------------------------------- time uncertainty
        self.__time_uncertainty = float(self.__module.getOption("time uncertainty"))

        self.__N_events = len(self.__zenith_inits) * len(self.__azimuth_inits) * len(self.__radius_inits) * self.__N_core_positions * len(self.__SNR_inits) * len(self.__rho_inits)

        print "Parameters for e-field trace simulation"
        print "\tzenith angles: ", str(self.__zenith_inits)
        print "\tazimuth angles: ", str(self.__azimuth_inits)
        print "\tradius: ", str(self.__radius_inits)
        print "\tSNR: ", str(self.__SNR_inits)
        print "\tadd noise: ", str(self.__noise)
        print "total number of events: " + str(self.__N_events)

        #=======================================================================
        # definition of some constants in augerUnits
        #=======================================================================
        self.__c = constants.c * 1e-9
        self.__tracelength = 3900 / 2
        self.__samplingrate = 1.  # in ns
        self.__upsampling = int(round(5. / self.__samplingrate))

        #-------------------------------------------- set magnetic field vector
        self.__magnetic_field_vector = TVector3(1)
        self.__magnetic_field_vector.SetTheta(self.__magnetic_field_zenith)
        self.__magnetic_field_vector.SetPhi(self.__magnetic_field_azimuth)

        #--------------------------------------------------- signal noise window
        self.__signal_search_window_start = 900
        self.__signal_search_window_stop = 1100
        self.__noise_window_start = 0
        self.__noise_window_stop = 900

        # offset = (490-self.__samplingrate*self.__tracelength)/2
        self.__offset = 0  # what is this parameter for?

        #=======================================================================
        # define geometry
        #=======================================================================
        ROOT.gSystem.Load("$AUGEROFFLINEROOT/lib/libRecEventKG.so")

        self.__ADST_file = ROOT.RecEventFile(self.__ADST_path)
        geo = ROOT.DetectorGeometry()
        self.__ADST_file.ReadDetectorGeometry(geo)

        gPos = TGraph()
        self.__antenna_positions = np.zeros((self.__N_antennas, 3))
        for i, statId in enumerate(self.__antennas):
            self.__antenna_positions[i] = np.array(geo.GetRdStationPosition(np.int(statId)))
            gPos.SetPoint(gPos.GetN(), self.__antenna_positions[i][0], self.__antenna_positions[i][1])

        #------------------------------------------------------- save debug plot
        gPos.GetXaxis().SetTitle("X [m]")
        gPos.GetYaxis().SetTitle("Y [m]")
        gPos.SetTitle("AERA Antenna Positions")
        c1 = TCanvas("c1", "Antenna Positions", 800, 800)
        c1.cd()
        gPos.Draw("AP")
        c1.SaveAs(os.path.join(self.__output_folder, "antenna_positions.png"))
        c1.Close()
        del c1
        del gPos

        #--------------------------------------- calculate bary center of radio stations
        mPos = np.mat(self.__antenna_positions)
        self.__bary_center = np.zeros(3)
        self.__bary_center[0] = mPos[:, 0].mean()
        self.__bary_center[1] = mPos[:, 1].mean()
        self.__bary_center[2] = mPos[:, 2].mean()
        self.__bary_center_vector = TVector3(self.__bary_center[0], self.__bary_center[1], self.__bary_center[2])

        self.__random = TRandom3(0)
        self.__core_point = TVector3()

        #------------------------------------------------------- get radio pulse
        self.__radio_pulse = self.__generateRadioPulse('dirac')

        print "end beginJob"

    def beginRun(self):
        '''Executed before each run'''
        print "beginRun"
        pass

    def generate(self):
        '''Executed on every object'''
        # event = core.toEvent(object)
        print "process"

        # send signal
#        self.__module.getSource("signal").setTargets(object)
#        self.__module.getSource("signal").progressTargets()
#
#        self.__module.getSource("signal2").setTargets(object)
#        self.__module.getSource("signal2").progressTargets()

        parameters = []
        n = 0

        progress = 0

        foutlog = open(os.path.join(self.__output_folder, "log_simulation.txt"), 'a')

        for SNR in self.__SNR_inits:
            for thetaInit in self.__zenith_inits:
                for phiInit in self.__azimuth_inits:
                    for rInit in self.__radius_inits:
                        for rhoInit in self.__rho_inits:
                            self.__run_number += 1
                            iEvent = 0

                            #-------------------------------------- create output folder
                            if(not os.path.isdir(os.path.join(self.__output_folder, 'Run' + str(self.__run_number)))):
                                os.mkdir(os.path.join(self.__output_folder, 'Run' + str(self.__run_number)))

                            #--------------------------------------------- open log file
                            foutlog.write(str(self.__run_number) + "\t" + str(SNR) + "\t" + str(thetaInit) + "\t" + str(phiInit) + "\t" + str(rInit) + "\t" + str(rhoInit) + "\t" + str(self.__time_uncertainty) + "\t" + str(self.__core_x_error) + "\t" + str(self.__core_y_error) + "\t" + str(self.__core_z_error) + "\n")

                            for iCorePosition in range(self.__N_core_positions):
                                n += 1
                                iEvent += 1
                                #--------------------------------------- output progress
                                output_accuracy = 10
                                if(round(1000. * n * output_accuracy / self.__N_events) > output_accuracy * progress):
                                    progress = int(round(1000. * n / self.__N_events))
                                    sys.stdout.write("\r[" + progress / 10 * "=" + ">" + (1000 - progress) / 10 * " " + "] " + str(progress / 10.) + "%")
                                    sys.stdout.flush()

                                #-------------------- vary core positions within uncertainty
                                self.__core_point.SetX(self.__random.Gaus(self.__bary_center_vector.X(), self.__core_x_error))
                                self.__core_point.SetY(self.__random.Gaus(self.__bary_center_vector.Y(), self.__core_y_error))
                                self.__core_point.SetZ(self.__random.Gaus(self.__bary_center_vector.Z(), self.__core_z_error))

                                #--------------------------------------- open and write output file
                                if(os.path.isfile(os.path.join(self.__output_folder, 'Run' + str(self.__run_number), 'Event' + str(iEvent) + '.dat'))):
                                    print "simulation ", iEvent, " for theta = ", thetaInit, " phi = ", phiInit, " SNR = ", SNR, " radius = ", rInit, " rho = ", rhoInit, " already exists"
                                    continue
                                fout = open(os.path.join(self.__output_folder, 'Run' + str(self.__run_number), 'Event' + str(iEvent) + '.dat'), 'w')

                                fout.write("##Event_Begin\n")
                                fout.write("runNumber=" + str(self.__run_number) + "\n")
                                fout.write("eventNumber=" + str(iEvent) + "\n")
                                fout.write("tracelength=" + str(self.__tracelength) + "\n")
                                fout.write("samplingrate=" + str(self.__samplingrate) + "\n")
                                fout.write("noise=" + str(self.__noise) + "\n")
                                fout.write("SNR=" + str(SNR) + "\n")
                                fout.write("ShowerAxisR=" + str(rInit) + "\n")
                                fout.write("ShowerAxisZenith=" + str(thetaInit) + "\n")
                                fout.write("ShowerAxisAzimuth=" + str(phiInit) + "\n")
                                fout.write("ShowerAxisRoh=" + str(rhoInit) + "\n")
                                fout.write("CorePositionX=" + str(self.__core_point.X()) + "\n")
                                fout.write("CorePositionY=" + str(self.__core_point.Y()) + "\n")
                                fout.write("CorePositionZ=" + str(self.__core_point.Z()) + "\n")

                                fout.write("NumberOfStations=" + str(len(self.__antennas)) + "\n")

                                ShowerAxis = TVector3(rInit)
                                ShowerAxis.SetTheta(thetaInit)
                                ShowerAxis.SetPhi(phiInit)

#                                sourcePoint = np.zeros(3)
#                                sourcePoint[0] = rInit * math.sin(thetaInit) * math.cos(phiInit)
#                                sourcePoint[1] = rInit * math.sin(thetaInit) * math.sin(phiInit)
#                                sourcePoint[2] = rInit * math.cos(thetaInit)
#
#                                sourcePoint = sourcePoint + self.__bary_center
#

                                # calculate source point and mean signal time for a spherical wavefront model
                                timeMean = 0.
                                if(self.__wavefrontmodel == "spherical"):
                                    sourcePoint = self.__core_point + ShowerAxis
                                    timeMean = ShowerAxis.Mag() / self.__c

                                #===============================================
                                # loop through all antenna stations
                                #===============================================
                                for i, pole in enumerate(self.__antenna_positions):
                                    AntennaPosition = TVector3(pole[0], pole[1], pole[2]) - self.__bary_center_vector
                                    npElectricField = hp.GetExpectedEFieldVector(np.array(self.__core_point), thetaInit, phiInit, pole)[0]

                                    # calculate signal times for the different wavefront models
                                    if(self.__wavefrontmodel == "spherical"):
                                        distanceAntennaToSourcePoint = (AntennaPosition - sourcePoint).Mag()
                                        t = distanceAntennaToSourcePoint / self.__c - timeMean
                                    elif(self.__wavefrontmodel == "cone"):
                                        ShowerAxis.SetMag(math.fabs(ShowerAxis.Unit() * AntennaPosition))
                                        lateraldistance = (ShowerAxis - AntennaPosition).Mag()
                                        t = (-ShowerAxis.Unit() * AntennaPosition + math.tan(rhoInit) * lateraldistance) / self.__c
                                    elif(self.__wavefrontmodel == "plane"):
                                        t = (-ShowerAxis.Unit() * AntennaPosition) / self.__c

                                    tRand = self.__random.Gaus(t, self.__time_uncertainty)

                                    fout.write("#Station_Begin" + "\n")
                                    fout.write("stationNo=" + str(self.__antennas[i]) + "\n")
                                    fout.write("traceStartTime=" + str(tRand) + "\n")
                                    fout.write("TimeUncertainty=" + str(self.__time_uncertainty) + "\n")

                                    #===========================================
                                    # create signal trace
                                    #===========================================
                                    polarisations = ['EW', 'NS', 'V']
                                    signalTrace = [[], [], []]
                                    for j, polarisation in enumerate(polarisations):
                                        for k in range(self.__tracelength):
                                            signalTrace[j].append(np.roll(self.__radio_pulse[int(
                                                 self.__offset * self.__timeToBin + 
                                                 self.__timeToBin * k * self.__samplingrate)]
                                                                           * npElectricField[j], 0))

                                    #===========================================
                                    # add noise to signal trace
                                    #===========================================
                                    if(self.__noise):
                                        noiseTrace = [[], [], []]
                                        randomNoiseNumber = int(self.__random.Uniform() * 151 + 1)
                                        noiseOffset = int(self.__random.Uniform() * 500)

                                        #--- read noise trace from noise library
                                        for j, polarisation in enumerate(polarisations):
                                            noiseIn = np.loadtxt(os.path.join(self.__noise_library_path, 'Upsampling' + str(self.__upsampling), polarisation + '_' + str(randomNoiseNumber) + '.txt'))
                                            for k in range(self.__tracelength):
                                                noiseTrace[j].append(noiseIn[k + noiseOffset])

                                        signalTrace = np.array(signalTrace)
                                        noiseTrace = np.array(noiseTrace)

                                        # normalize noise trace to a maximum amplitude of the Hilbert envelope of 1
                                        noiseHilbert = [[], [], []]
                                        noiseHilbert[0] = abs(signal.hilbert(noiseTrace[0]))
                                        noiseHilbert[1] = abs(signal.hilbert(noiseTrace[1]))
                                        noiseHilbert[2] = abs(signal.hilbert(noiseTrace[2]))
                                        noiseHilbertMag = abs(np.array(np.sqrt(noiseHilbert[0] ** 2 + noiseHilbert[1] ** 2 + noiseHilbert[2] ** 2)))

                                        noiseTrace *= 1 / self.__getRMS(noiseHilbertMag)

                                        #---- recalculate Hilbert envelope, why?
#                                        noiseHilbert = [[], [], []]
#                                        noiseHilbert[0] = abs(signal.hilbert(noiseTrace[0]))
#                                        noiseHilbert[1] = abs(signal.hilbert(noiseTrace[1]))
#                                        noiseHilbert[2] = abs(signal.hilbert(noiseTrace[2]))
#                                        noiseHilbertMag = abs(np.array(np.sqrt(noiseHilbert[0] ** 2 + noiseHilbert[1] ** 2 + noiseHilbert[2] ** 2)))

                                        # print "RMS noiseHilbertMag", GetRMS(noiseHilbertMag)
                                        iWhile = 0
                                        trace = signalTrace + noiseTrace / math.sqrt(SNR)
                                        true_SNR = self.__getSNR(trace)
                                        deviation = (SNR - true_SNR) / SNR
                                        accuracy = 500
                                        tmp_SNR = np.linspace(SNR - 0.8 * SNR, SNR + 0.9 * SNR, accuracy)
                                        accuracy = float(accuracy)
                                        lower_bound = 0
                                        upper_bound = 1
#                                        print "SNR = ", SNR, "\ttrue SNR = ",true_SNR,"\tdeviation = ", deviation
                                        while(math.fabs(deviation) > 0.01):
                                            i = int(self.__random.Uniform(lower_bound, upper_bound) * accuracy)
                                            if(iWhile >= 100):
#                                                print "WARNING: didn't find the correct SNR ratio after 100 iterations (SNR = ", SNR, "), the deviation is still ", deviation, "lower_bound = ", lower_bound, " upper_bound = ", upper_bound, "i = ", i, "-> SNR[i] = ", tmp_SNR[i]
                                                break
                                            trace = signalTrace + noiseTrace / math.sqrt(tmp_SNR[i])
                                            true_SNR = self.__getSNR(trace)
                                            deviation = (true_SNR - SNR) / SNR
#                                            print iWhile,"\ti = ", i, "\tSNR = ", tmp_SNR[iWhile], "\ttrue SNR = ",true_SNR,"\tdeviation = ", deviation
                                            if(deviation > 0.):
                                                upper_bound = i / accuracy
                                            else:
                                                lower_bound = i / accuracy
                                            iWhile += 1

                                    else:
                                        trace = np.array(signalTrace)

                                    #===========================================
                                    # write trace to output file
                                    #===========================================
                                    for j, polarisation in enumerate(polarisations):
                                        fout.write("#trace" + polarisation + "\n")

                                        for k in range(self.__tracelength):
                                            val = trace[j][k]
                                            fout.write(str(val) + " ")
                                        fout.write("\n")

                                    fout.write("#Station_End" + "\n")

                                fout.write("##Event_End")
                                fout.close()

        print
        print "...done. ", self.__N_events, " events have been simulated."

        # return the name of the source
        return "out"

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        self._logger.log(core.LOG_LEVEL_INFO, "End job")
        self.__ADST_file.Close()
        del self.__ADST_file

    def __getSNR(self, trace):
        traceHilbert = [[], [], []]
        traceHilbert[0] = abs(signal.hilbert(trace[0]))
        traceHilbert[1] = abs(signal.hilbert(trace[1]))
        traceHilbert[2] = abs(signal.hilbert(trace[2]))
        traceHilbertMag = abs(np.array(np.sqrt(traceHilbert[0] ** 2 + traceHilbert[1] ** 2 + traceHilbert[2] ** 2)))

        RMS = self.__getRMS(traceHilbertMag[self.__noise_window_start / self.__samplingrate:self.__noise_window_stop / self.__samplingrate])
        maximum = traceHilbertMag[self.__signal_search_window_start / self.__samplingrate:self.__signal_search_window_stop / self.__samplingrate].max()
        return (maximum / RMS) ** 2

    def __getRMS(self, trace):
        rms = 0
        N = 0
        for val in trace:
            rms += val ** 2
            N += 1
        return np.sqrt(rms / N)

    #===============================================================================
    # generate dirac pulse
    #===============================================================================
    def __generateRadioPulse(self, pulse_form, low_freq=30, up_freq=80):
        if(pulse_form == 'dirac'):
            nSamples = 2 ** 16
            sampling_dirac = 0.03 * 1.e-9
            dirac = np.zeros(nSamples)
            epsilon = 0.005
            time = np.linspace(0, sampling_dirac * nSamples, nSamples) * 1.e9
            freqs = np.linspace(0, 1. / (2 * sampling_dirac), nSamples / 2 + 1) / (1.e6)
            freqToBin = nSamples * sampling_dirac
            dirac[nSamples / 2] = 1
            diracfft = fft.rfft(dirac)
            diracfft_limited = copy.deepcopy(diracfft)
            diracfft_limited[0:(low_freq * 1.e6 * freqToBin)] = 0
            diracfft_limited[(up_freq * 1.e6 * freqToBin):] = 0
            dirac = fft.irfft(diracfft_limited)
            dirac = dirac / abs(dirac).max()
            self.__timeToBin = 1 / (sampling_dirac * 1.e9)
            return dirac
