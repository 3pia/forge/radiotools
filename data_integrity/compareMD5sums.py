#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import sys

compare_full_path = False

# read in md5sums
files_nopartner = []
files_nomd5match = []
counter_success = 0

if(len(sys.argv) != 3):
    print("ERROR: incorrect input parameters\nusage: first parameter must be the input file. second parameter the file to compare")
md5sums_filename = sys.argv[1]
md5sums_filename2 = sys.argv[2]

print("comparing file1 %s with file2 %s (if second file contains more entries than the first file, this is not taken into account)" % (md5sums_filename, md5sums_filename2))
# fill second file into map
data2 = np.genfromtxt(md5sums_filename, dtype=str)
data2_dict = dict()
for md5sum, filename in data2:
    if compare_full_path:
        data2_dict[filename] = md5sum
    else:
        data2_dict[os.path.basename(filename)] = md5sum

data = np.genfromtxt(md5sums_filename, dtype=str)
n = data.shape[0]
for md5sum, filename in data:
    filebasename = os.path.basename(filename)
    if compare_full_path:
        filebasename = filename

    if(filebasename in data2_dict):
        if(md5sum != data2_dict[filebasename]):
            print("ERROR: md5sums of file %s do not match (%s vs %s)" % (os.path.basename(filename), md5sum, data2_dict[filebasename]))
            files_nomd5match.append(filename)
        else:
            counter_success += 1
    else:
        files_nopartner.append(filename)
        print("WARNING: no md5sum for file %s could be found" % os.path.basename(filename))
print("SUMMARY:")
print("\t%i md5sums of the first file compared with the checksums from the second file" % n)
print("\t%i (%.1f%%) files have matching md5sum" % (counter_success, 100. * counter_success / n))
print("\t%i (%.1f%%) do not have a partner in second file" % (len(files_nopartner), 100. * len(files_nopartner) / n))
print("\t%i (%.1f%%) do not have a matching md5sum" % (len(files_nomd5match), 100. * len(files_nomd5match) / n))
