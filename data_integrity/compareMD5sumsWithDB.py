#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import MySQLdb
import sys

mysql_opts = {
    'host': "paomondb.physik.uni-wuppertal.de",
    'user': "RioGrande",
    'pass': "Yannick300",
    'db': "AERA"
    }

mysql = MySQLdb.connect(mysql_opts['host'], mysql_opts['user'], mysql_opts['pass'], mysql_opts['db'])
mysql.apilevel = "2.0"
mysql.threadsafety = 2
mysql.paramstyle = "format"
cursor = mysql.cursor()

# read in md5sums
files_nodbentry = []
files_nomd5match = []
counter_success = 0

if(len(sys.argv) != 2):
    print("ERROR: incorrect input parameters\nusage: first parameter must be the input file.")
md5sums_filename = sys.argv[1]

data = np.genfromtxt(md5sums_filename, dtype=str)
n = data.shape[0]
for md5sum, filename in data:
    cursor.execute("select MD5 from DAQFiles where FileName='%s'" % (os.path.basename(filename)))
    md5sum_db = np.array(cursor.fetchall())
    #print md5sum_db
    if(len(md5sum_db) == 0):
        print("WARNING: no md5sum for file %s could be found" % os.path.basename(filename))
        files_nodbentry.append(filename)
    elif(len(md5sum_db) > 1):
        print("WARNING: more than one md5sum for file %s could be found" % os.path.basename(filename))
    else:
        md5sum_db = md5sum_db[0][0]
        if(md5sum != md5sum_db):
            print("ERROR: md5sums of file %s do not match (%s vs %s)" % (os.path.basename(filename), md5sum, md5sum_db))
#             print md5sum
#             print md5sum_db
            files_nomd5match.append(filename)
        else:
            counter_success += 1
print("SUMMARY:")
print("\t%i files compared with db" % n)
print("\t%i (%.1f%%) files have matching md5sum" % (counter_success, 100. * counter_success / n))
print("\t%i (%.1f%%) do not have an entry in db" % (len(files_nodbentry), 100. * len(files_nodbentry) / n))
print("\t%i (%.1f%%) do not have a matching md5sum" % (len(files_nomd5match), 100. * len(files_nomd5match) / n))
