#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import numpy as np
import os
import MySQLdb
import sys
import glob
import hashlib


def md5(fname):
    hash = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash.update(chunk)
    return hash.hexdigest()


def saveresult():
    np.savetxt("files_not_found.txt", files_not_found, fmt="%s")
    np.savetxt("files_no_md5match.txt", files_nomd5match, fmt="%s")
    np.savetxt("files_correct_md5match.txt", files_correctmd5sum, fmt="%s")
    np.savetxt("files_no_md5sumindb.txt", files_nomd5sumindb, fmt="%s")
    print()
    print("SUMMARY")
    print("%i files not found" % n_notfound)
    print("%i files have no md5 sum in db" % n_md5sum0)
    print("%i files have correct md5sum" % n_correctmd5sum)
    print("%i files have incorrect md5sum" % n_incorrectmd5sum)

import atexit
atexit.register(saveresult)


mysql_opts = {
    'host': "paomondb.physik.uni-wuppertal.de",
    'user': "RioGrande",
    'pass': "Yannick300",
    'db': "AERA"
    }

mysql = MySQLdb.connect(mysql_opts['host'], mysql_opts['user'], mysql_opts['pass'], mysql_opts['db'])
mysql.apilevel = "2.0"
mysql.threadsafety = 2
mysql.paramstyle = "format"
cursor = mysql.cursor()

# read in md5sums
files_nodbentry = []
files_nomd5match = []
counter_success = 0

# if(len(sys.argv) != 2):
#     print("ERROR: incorrect input parameters\nusage: first parameter must be the input file.")
# md5sums_filename = sys.argv[1]
#
# data = np.genfromtxt(md5sums_filename, dtype=str)
# n = data.shape[0]

cursor.execute("select FileName, MD5 from DAQFiles ORDER BY FileName asc;")
md5sums = np.array(cursor.fetchall())
path = "/net/aerapublic/binary_data"
n_notfound = 0
n_md5sum0 = 0
n_correctmd5sum = 0
n_incorrectmd5sum = 0
files_not_found = []
files_nomd5match = []
files_nomd5sumindb = []
files_correctmd5sum = []


if os.path.exists("files_not_found.txt"):
    files_not_found = list(np.loadtxt("files_not_found.txt", dtype=np.str))

if os.path.exists("files_no_md5match.txt"):
    files_nomd5match = list(np.loadtxt("files_no_md5match.txt", dtype=np.str))

if os.path.exists("files_correct_md5match.txt"):
    files_correctmd5sum = list(np.loadtxt("files_correct_md5match.txt", dtype=np.str))

if os.path.exists("files_no_md5sumindb.txt"):
    files_nomd5sumindb = list(np.loadtxt("files_no_md5sumindb.txt", dtype=np.str))

already_considered_files = files_not_found + files_nomd5match + files_correctmd5sum + files_nomd5sumindb

for filename, md5sum in md5sums:
    if filename in already_considered_files:
        print("file %s already considerd" % filename)
        continue
    f2 = glob.glob(os.path.join(path, "*/*/%s" % filename))
    print("searching for %s with md5sum %s" % (filename, md5sum))
    if len(f2) == 0:
        n_notfound += 1
        files_not_found.append(filename)
        print("\tfile %s not found" % filename)
    else:
        if(md5sum == "0"):
            n_md5sum0 += 1
            files_nomd5sumindb.append(filename)
        else:
            md5sum_server = md5(f2[0])
            if(md5sum != md5sum_server):
                print("\tfile %s has a different md5sum on the server and in the DB" % f2[0])
                print(md5sum)
                print(md5sum_server)
                n_incorrectmd5sum += 1
                files_nomd5match.append(filename)
            else:
                print("\tfile %s has correct md5sum" % filename)
                n_correctmd5sum += 1
                files_correctmd5sum.append(filename)










