import numpy as np


def parse_gdas(filename):
    import csv
    datafile = open(filename, "r")
    datareader = csv.reader(datafile)
    data = []
    data2 = []
    data_tmp = []
    for row in datareader:
        tmp = [float(elem) for elem in row[0].split()]
        if len(tmp) == 34:
            data.append(tmp)
            if len(data) > 0:
                data2.append(data_tmp)
            data_tmp = []
        else:
            data_tmp.append(tmp)
    data = np.array(data)
    data2 = np.array(data2)
    return data, data2


def get_temp_in_celsius(T):
    return T - 273.15


def get_g0(latitude=np.deg2rad(-35)):
    """ returns the gravitational acceleration at sea level (in m/s^2)
    the latitude of -35deg is the position of the GDAS data point used at the Pierre Auger Observatory
    from Abreu et al., Astropart. Phys. 35 (2012) 591-607 """
    return 9.806160 * (1 - 0.0026373 * np.cos(2 * latitude) + 0.0000059 * np.cos(2 * latitude) ** 2)


def get_altitude_from_geopotential_height(z, latitude=np.deg2rad(-35)):
    """ returns the height (above sea level in meters) from the geopotential height
    the latitude of -35deg is the position of the GDAS data point used at the Pierre Auger Observatory"""
    A = 0.0053024
    B = 0.0000058
    C = 3.086e-6
    g0 = get_g0(latitude)
    t1 = g0 * (2 + A - B - A * np.cos(2 * latitude) + 2 * B * np.cos(4 * latitude))
    t2 = g0 * (-8 * C * z + g0 * (2 + A - B - A * np.cos(2 * latitude) + B * np.cos(4 * latitude)) ** 2)
    return (t1 - t2 ** 0.5) / (2 * C)


def get_refractivity(P, T, Pv=0):
    """
    P: Pressure sufrace
    T: Temperature
    Pv: partial pressure of water vapor
    """
    return 77.6 / T * (P + 4810 * Pv / T) * 1e-6


def get_partial_pressure_water_vapor(Rh, T):
    """ returns the partial pressure of water vapor in hPa
    input is the humidity percentage and the temperature in Kelvin """
    return Rh * get_pressure_saturated_vapor(T)


def get_pressure_saturated_vapor(T):
    """ returns the pressure of the saturated vapor in hPa,
    input is the temperature in Kelvin """
    T = get_temp_in_celsius(T)
    return 6.1121 * np.exp((18.678 - T / 234.5) * (T / (257.14 + T)))


if __name__ == "__main__":
    data, data2 = parse_gdas("/net/aera/cglaser/gdas/GDASmalargue/gdas1.apr12.w1_malargue")
    for i, row in enumerate(data):
        P = row[1 + 4]
        T = row[19 + 4]
        Rh = row[8 + 4] * 0.01
        N = get_refractivity(P, T, get_partial_pressure_water_vapor(Rh, T))
        print
        print "%.1f %.1f %.0f%% %.3e" % (P, T, Rh * 100, get_refractivity(P, T))
        print "%.1f %.1f %.0f%% %.3e" % (P, T, Rh * 100, N)
        if 0:
            for t in data2[i]:
                T = t[2 + 4]
                Rh = t[6 + 4] * 0.01
                print P, T, Rh * 100, get_refractivity(P, T, get_partial_pressure_water_vapor(Rh, T))

