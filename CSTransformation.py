#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math
from numpy.linalg import linalg
import copy

import HelperFunctions as hp


class CSTransformation():

    """ class to performe coordinate transformations """

    def __init__(self, zenith, azimuth, core=None, magnetic_field=None):
        self.__core = core
        showeraxis = -1 * hp.SphericalToCartesian(zenith, azimuth)  # -1 is because shower is propagating towards us
        if(magnetic_field is None):
            # print "using default magnetic field (", hp.GetMagneticFieldVector(), ")"
            magnetic_field = hp.GetMagneticFieldVector()
        magnetic_field /= linalg.norm(magnetic_field)
        vxB = np.cross(showeraxis, magnetic_field)
        e1 = vxB
        e2 = np.cross(showeraxis, vxB)
        e3 = np.cross(e1, e2)
        e1 /= linalg.norm(e1)
        e2 /= linalg.norm(e2)
        e3 /= linalg.norm(e3)
        self.__transformation_matrix = np.matrix([e1, e2, e3])
#         print "__init__"
#         print showeraxis
#         print e1
#         print e2
#         print e3
#         print self.__transformation_matrix
        self.__inverse_transformation_matrix = np.linalg.inv(self.__transformation_matrix)
#         print "init:"
#         print "matrix: "
#         print self.__transformation_matrix
#         print "inv matrix: "
#         print self.__inverse_transformation_matrix
#         print "trans matrix: "
#         print self.__transformation_matrix.T
#         print "comparison ", np.testing.assert_allclose(self.__inverse_transformation_matrix, self.__transformation_matrix.T, rtol=1e-10)

    def transform_to_vxB_vxvxB(self, station_position):
        """ transform a single station position or a list of multiple
        station positions into vxB, vxvxB shower plane """
        if(self.__core is not None):
            station_position = np.array(copy.deepcopy(station_position))
        if(len(station_position.shape) == 1):
            if(self.__core is not None):
                station_position -= self.__core
            return np.squeeze(np.asarray(np.dot(self.__transformation_matrix, station_position)))
        else:
            result = []
            for pos in station_position:
                if(self.__core is not None):
                    pos -= self.__core
                result.append(np.squeeze(np.asarray(np.dot(self.__transformation_matrix, pos))))
            return np.array(result)

    def transform_from_vxB_vxvxB(self, station_position):
        """ transform a single station position or a list of multiple
        station positions back to x,y,z CS """
        if(self.__core is not None):
            station_position = copy.deepcopy(station_position)
        if(len(station_position.shape) == 1):
            temp = np.squeeze(np.asarray(np.dot(self.__inverse_transformation_matrix, station_position)))
            if(self.__core is not None):
                return temp + self.__core
            return temp
        else:
            result = []
            for pos in station_position:
                temp = np.squeeze(np.asarray(np.dot(self.__inverse_transformation_matrix, pos)))
                if(self.__core is not None):
                    result.append(temp + self.__core)
                else:
                    result.append(temp)
            return np.array(result)

    def transform_from_vxB_vxvxB_2D(self, station_position):
        """ transform a single station position or a list of multiple
        station positions back to x,y,z CS """
        if(self.__core is not None):
            station_position = copy.deepcopy(station_position)
        if(len(station_position.shape) == 1):
            position = np.array([station_position[0], station_position[1],
                                 self.get_height_in_showerplane(station_position[0], station_position[1])])
            result = np.squeeze(np.asarray(np.dot(self.__inverse_transformation_matrix, position)))
            if(self.__core is not None):
                result += self.__core
            return result
        else:
            result = []
            for pos in station_position:
                position = np.array([pos[0], pos[1], self.get_height_in_showerplane(pos[0], pos[1])])
                pos_transformed = np.squeeze(np.asarray(np.dot(self.__inverse_transformation_matrix, position)))
                if(self.__core is not None):
                    pos_transformed += self.__core
                result.append(pos_transformed)
            return np.array(result)

    def get_height_in_showerplane(self, x, y):
        return -1. * (self.__transformation_matrix[0, 2] * x + self.__transformation_matrix[1, 2] * y) / self.__transformation_matrix[2, 2]

    def get_euler_angles(self):
        R = self.__transformation_matrix
        if(abs(R[2, 0]) != 1):
            theta_1 = -math.asin(R[2, 0])
            theta_2 = math.pi - theta_1
            psi_1 = math.atan2(R[2, 1] / math.cos(theta_1), R[2, 2] / math.cos(theta_1))
            psi_2 = math.atan2(R[2, 1] / math.cos(theta_2), R[2, 2] / math.cos(theta_2))
            phi_1 = math.atan2(R[1, 0] / math.cos(theta_1), R[0, 0] / math.cos(theta_1))
            phi_2 = math.atan2(R[1, 0] / math.cos(theta_2), R[0, 0] / math.cos(theta_2))
        else:
            phi_1 = 0.
            if(R[2, 0] == -1):
                theta_1 = math.pi * 0.5
                psi_1 = phi_1 + math.atan2(R[0, 1], R[0, 2])
            else:
                theta_1 = -1. * math.pi * 0.5
                psi_1 = -phi_1 + math.atan2(-R[0, 1], -R[0, 2])
        return psi_1, theta_1, phi_1


if __name__ == "__main__":
    zenith = np.deg2rad(21.2149)
    azimuth = np.deg2rad(-143.746 + 360.)
    cs = CSTransformation(zenith, azimuth, magnetic_field=np.array([0.0317967,  0.807811, 0.588583]))
    print [-29.2463, 36.3054, 2.73222], cs.transform_to_vxB_vxvxB(np.array([-29.2463, 36.3054, 2.73222]))
    print [-288.319, -100.42, 5.16209], cs.transform_to_vxB_vxvxB(np.array([-288.319, -100.42, 5.16209]))
     

    showeraxis = hp.SphericalToCartesian(zenith, azimuth)
    print("showeraxis = %s" % showeraxis)
    magnetic_field = -1. * hp.GetMagneticFieldVector()
    # magnetic_field[0] = 0
#     magnetic_field = np.array([0, -1., 0])
    print("magnetic_field = %s" % magnetic_field)
    vxB = np.cross(showeraxis, magnetic_field)
    print("vxB = %s" % vxB)

#     e1 = showeraxis
#     e2 = np.cross(showeraxis, np.cross(showeraxis, np.cross(showeraxis, magnetic_field)))
#     e3 = np.cross(e1, e2)
#     e2 /= linalg.norm(e2)
#     e3 /= linalg.norm(e3)
    e1 = vxB
    e2 = np.cross(showeraxis, vxB)
    e3 = np.cross(e1, e2)
    e1 /= linalg.norm(e1)
    e2 /= linalg.norm(e2)
    e3 /= linalg.norm(e3)
    print e1, e2, e3

    print linalg.norm(e1), linalg.norm(e2), linalg.norm(e3)

    # check if vectors are perpendicular
    print("dot e1*e1 = %f" % np.dot(e1, e2))
    print("dot e1*e3 = %f" % np.dot(e1, e3))
    print("dot e2*e3 = %f" % np.dot(e2, e3))

    print("showeraxis = %s" % showeraxis)

    transformation_matrix = np.matrix([e1, e2, e3])
    # transformation_matrix = linalg.inv(transformation_matrix)

    cs = CSTransformation(zenith, azimuth)

#     showeraxis_ = cs.transform_to_vxB_vxvxB(showeraxis)
#     print("transformation matrix = \n%s" % transformation_matrix)
#     print("transformed showeraxis = %s" % showeraxis_)
#
#     x1 = np.array([100., 0., 0.])
#     x1_ = cs.transform_to_vxB_vxvxB(x1)
#     print("x1  = %s" % x1)
#     print("x1_ = %s" % x1_)
#     print
#     x1 = np.array([0., 100., 0.])
#     x1_ = cs.transform_to_vxB_vxvxB(x1)
#     print("x1  = %s" % x1)
#     print("x1_ = %s" % x1_)
#     print
#     x1 = np.array([-100., 0., 0.])
#     x1_ = cs.transform_to_vxB_vxvxB(x1)
#     print("x1  = %s" % x1)
#     print("x1_ = %s" % x1_)
#     print
#     x1 = np.array([100., 100., 0.])
#     x1_ = cs.transform_to_vxB_vxvxB(x1)
#     print("x1  = %s" % x1)
#     print("x1_ = %s" % x1_)
#     print
#
#     print("distance = %.2f" % hp.GetDistanceToShowerAxis(np.array([0, 0, 0]), zenith, azimuth, x1))
#     zenith_, azimuth_ = hp.CartesianToSpherical(showeraxis_[0], showeraxis_[1], showeraxis_[2])
#     print("distance_ = %.2f" % hp.GetDistanceToShowerAxis(np.array([0, 0, 0]), zenith_, azimuth, x1_))
#
#     # test class
#
#     x_1 = cs.transform_to_vxB_vxvxB(x1)
#     print x_1, type(x_1)
#     print cs.transform_to_vxB_vxvxB(np.array([x1, x1, x1, x1]))
#
#     print np.rad2deg(cs.get_euler_angles())
#
#     m = TMatrix(3,3, np.array(transformation_matrix))
#     y = TVector3(x1[0], x1[1],x1[2])
#     print m*y
