import os


class Quantities:

    def __init__(self):
        self.elements = {}

    def __repr__(self):
        ret = ''
        for k, v in self.__dict__.iteritems():
            if k == 'elements':
                continue
            ret += str(k)
            ret += ' = '
            ret += str(v)
            ret += '\n'
        return ret


rdstQ = Quantities()
rdshQ = Quantities()
rdchQ = Quantities()

pathPrefix = ""
if "AUGEROFFLINEROOT" in os.environ:
    pathPrefix = os.path.join(os.environ["AUGEROFFLINEROOT"], "include/adst/")

for ifile in (os.path.join(pathPrefix, 'StationRRecDataQuantities.h'),
              os.path.join(pathPrefix, 'ShowerRRecDataQuantities.h'),
              os.path.join(pathPrefix, 'ChannelRRecDataQuantities.h')):
    if not os.path.exists(ifile):
        raise IOError("File does not exist!\n" + ifile)
    if ifile.find('Station') > -1:
        elem = rdstQ
    elif ifile.find('Shower') > -1:
        elem = rdshQ
    elif ifile.find('Channel') > -1:
        elem = rdchQ
    else:
        raise IOError('failed to load,' + ifile)
    for iline in open(ifile):
        iline = iline.strip()
        if iline.find('=') > -1 and iline[0] == 'e':
            iline = iline.split(',')[0]
            iline = iline.split('=')
            idx = iline[0]
            for i in iline[1].split(' '):
                if i != '':
                    val = i
                    break
            elem.__dict__[idx.strip()] = int(val.strip())
            elem.elements[idx.strip()] = int(val.strip())
