from math import log10


def getSignificantExponent(value):
    """
    Returns the Significant exponent, it is
    """
    if(value == 0):
        return 0
    l = log10(abs(value))
    if l > 0:
        significantFigures = 0
    elif abs(l) - int(abs(l)) == 0:
        significantFigures = int(abs(l))
    else:
        significantFigures = int(abs(l)) + 1
    return significantFigures


def reduceToSignificantFigure(value, error):
    """
    Round value and error to the significant figures
    """
    significantFigures = getSignificantExponent(error)
    return round(value, significantFigures), round(error,
            significantFigures)


def formatError(value, error):
    """
    return two formatted latex strings of form value, error with
    correct significant figures
    """
    value, error = reduceToSignificantFigure(value, error)
    significantFigures = getSignificantExponent(error)
    if((abs(value) > 1e4) or (abs(value) < 1e-2)):
        stringTemplate = '%%.%ig' % (significantFigures)
    else:
        stringTemplate = '%%.%if' % (significantFigures)
    result = stringTemplate % (value), stringTemplate % (error)
    return result

if __name__ == "__main__":
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .1))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .01))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .006))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .0099))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .00123))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .001999))
    print "%f +/- %f" % (reduceToSignificantFigure(.123456, .00100001))
    print
    print "%s +/- %s" % (formatError(.123456, .1))
    print "%s +/- %s" % (formatError(.123456, .01))
    print "%s +/- %s" % (formatError(.123456, .006))
    print "%s +/- %s" % (formatError(.123456, .0099))
    print "%s +/- %s" % (formatError(.123456, .00123))
    print "%s +/- %s" % (formatError(.123456, .001999))
    print "%s +/- %s" % (formatError(.123456, .00100001))
    print
    print "%s +/- %s" % (formatError(1004.123456, 12.00100001))

