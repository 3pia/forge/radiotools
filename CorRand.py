#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ROOT import TF2, TRandom3, TH2D
import math
import numpy as np


def Get2DRandomNumber(x, xError, y, yError, rho):
    mu = np.zeros(2)
    sig = np.zeros(2)
    mu[0] = x
    mu[1] = y
    sig[0] = xError
    sig[1] = yError

    rnd = TRandom3(0)
    RT = np.zeros((2, 2))
    RT[0][0] = xError
    RT[1][0] = 0.0
    RT[0][1] = rho * yError
    RT[1][1] = yError * math.sqrt(1 - rho * rho)

    # gausFkt = TF2("2Dim Gauss", "exp( -0.5 / (1.-[4]**2) * ( ((x-[0])/[1])**2 + ((y-[2])/[3])**2 - 2.*[4]*((x-[0])/[1])*((y-[2])/[3])) ) / ( 2.*TMath::Pi()*[1]*[3]*sqrt(1.-[4]**2) )",                    mu[0]-10*sig[0], mu[0]+10*sig[0], mu[1]-10*sig[1], mu[1]+10*sig[1])
    # gausFkt.SetParameters(mu[0],sig[0],mu[1],sig[1],rho)

    normRnd = np.zeros(2)
    corRnd = np.zeros(2)

    normRnd[0] = rnd.Gaus(0.0, 1.0)
    normRnd[1] = rnd.Gaus(0.0, 1.0)
    corRnd[0] = mu[0] + RT[0][0] * normRnd[0]  # RT[1][0]=0!
    corRnd[1] = mu[1] + RT[0][1] * normRnd[0] + RT[1][1] * normRnd[1]

    return corRnd[0], corRnd[1]

# Test Code:
if __name__ == "__main__":

    h = TH2D("h", "", 50, -5, 5, 50, -5, 5)
    for i in range(10000):
        x, y = Get2DRandomNumber(2, 1, -1, 2, 0.6)
        h.Fill(x, y)
    h.Draw("COLZ")

    # h2 = TH2D("h2","",50,-5,5,50,-5,5)
    # for x in range(50):
        # for y in range(50):
        # h2.SetBinContent(x,y,gausFkt.Eval(1.*x/25-5,1.*y/25-5))
    # h2.Draw("COLZ")
