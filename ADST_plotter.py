#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from ROOT import *
import ROOT
from ROOT import TH1D, TGraph, TLegend, TText, TLatex, TLine
from ROOT import TCanvas
import sys
import math
import numpy as np
import os
from pyik import adst
import argparse
import HelperFunctions as hp
import ROOTPlottingHelpers as rhp
from convertEnums import *

ROOT.gROOT.SetBatch(1)


def GetRMS(trace):
    rms = 0
    N = 0
    for val in trace:
        rms += val ** 2
        N += 1
    return np.sqrt(rms / N)


def read_adst_to_dict(filename):
        print "reading in ", filename
        file = ROOT.RecEventFile(str(filename))
        event = ROOT.RecEvent()
        file.SetBuffers(event)
        data = dict()
        while file.ReadNextEvent() == ROOT.RecEventFile.eSuccess:
                event_dict = dict()

                rEvent = event.GetRdEvent()
                eventId = rEvent.GetRdEventId()
                runId = rEvent.GetRdRunNumber()
                key = str(runId) + "." + str(eventId)

                rShower = rEvent.GetRdRecShower()
                stations = rEvent.GetRdStationVector()

                zenith = rShower.GetZenith()
                azimuth = rShower.GetAzimuth()

                if(zenith > max_zenith):
                    continue
                event_dict['zenith'] = zenith
                event_dict['azimuth'] = azimuth

                station_data = dict()

                for station in stations:
                        station_dict = dict()
                        key_station = 'station_' + str(station.GetId())

                        noiseStartTime = station.GetParameter(
                            rdstQ.eNoiseWindowStart) - station.GetParameter(rdstQ.eTraceStartTime)
                        noiseStopTime = station.GetParameter(
                            rdstQ.eNoiseWindowStop) - station.GetParameter(rdstQ.eTraceStartTime)

                        signal = station.GetParameter(rdstQ.eSignal)
                        noise = station.GetParameter(rdstQ.eNoise)

                        station_dict['signal'] = signal
                        station_dict['noise'] = noise

                        station_data[key_station] = station_dict
                event_dict['stations'] = station_data
                data[key] = event_dict
        return data

station_plots = True

if __name__ == "__main__":
    #=========================================================================
    # parse command line arguments
    #=========================================================================
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dataFiles', dest='data_files', metavar='ADST1.root',
                        type=str, nargs='+',
                        help='list of input ADST files')
    parser.add_argument('--energyCut', dest='energy_cut',
                        metavar='energy_cut', type=str, nargs='?',
                        default='0', help='minumum energy in eV')
    parser.add_argument('--outputFolder', dest='output_folder',
                        metavar='analysis1', type=str, nargs='?',
                        default='', help='output folder')
    parser.add_argument('--prefix', dest='prefix', metavar='cut1_',
                        type=str, nargs='?', default='', help='plot prefix')
    parser.add_argument('--eventlist', dest='eventlist_filename',
                        metavar='eventlist.txt',
                        type=str, nargs='?', default='',
                        help='list of events that should be highlighted')

    args = vars(parser.parse_args())
    data_files = args['data_files']
    output_folder = args['output_folder']
    plot_prefix = args['prefix']
    energy_cut = np.float(args['energy_cut'])
    eventlist_filename = args['eventlist_filename']
    extra_events = True
    if(eventlist_filename == ''):
        extra_events = False
    else:
        print "event list will read from %s" % eventlist_filename
        print "it will be assumed that the first two collums os the files contain the run numbers and event ids"
        event_list = np.loadtxt(eventlist_filename, dtype=np.int, usecols=(0, 1))
        event_list = ["%06i.%07i" % (x[0], x[1]) for x in event_list]
        print event_list

    plot_folder = os.path.join("plots", output_folder)
    if(not os.path.exists(plot_folder)):
        os.mkdir(plot_folder)
    if(station_plots):
        if(not os.path.exists(os.path.join(plot_folder, "station_plots"))):
            os.mkdir(os.path.join(plot_folder, "station_plots"))
    print "output will be saved to %s" % (plot_folder)

#     ROOT.gStyle.SetPadTopMargin(0.12)
    # ROOT.gStyle.SetPadBottomMargin(0.12)
    # ROOT.gStyle.SetPadLeftMargin(0.12)
    # ROOT.gStyle.SetPadRightMargin(0.04)
#     ROOT.gStyle.SetStatX(0.96)
#     ROOT.gStyle.SetStatY(0.88)
#     ROOT.gStyle.SetStatW(0.25)
#     ROOT.gStyle.SetStatH(0.2)
    # ROOT.gStyle.SetOptTitle(0)
    # ROOT.gStyle.SetOptStat(1)
    # ROOT.gStyle.SetTitleOffset(0.6,"Y")
#     ROOT.gStyle.SetHistFillColor(4)
    # ROOT.gStyle.SetHistLineColor(602)
#     ROOT.gStyle.SetHistFillStyle(3004)
#     ROOT.gStyle.SetMarkerSize(0.95)
#     ROOT.gStyle.SetLineWidth(3)
    # ROOT.gStyle.SetStatX(0.15)
    # ROOT.gStyle.SetStatX(0.9)
    # ROOT.gStyle.SetOptFit(0)
    # ROOT.gStyle.SetOptDate(1)
    # ROOT.gROOT.SetBatch()
    #=========================================================================
    # define histograms
    #=========================================================================
    hZenith_all = rhp.GetTH1D("hZenith_all", "", 18, 0, 90, "zenith [deg]")
    hAzimuth_all = rhp.GetTH1D("hAzimuth_all", "", 18, 0, 360, "azimuth [deg]")
    hSinAlpha_all = rhp.GetTH1D("hSinAlpha_all", "", 20, 0, 1, "sin(#alpha)")
    # event time histogram will be filled aferwards as range is unknown

    # SD related histograms
    hEnergy_all = rhp.GetTH1D(
        "hEnergy_all", "", 20, 16, 19, "log_{10}(energy [EeV])")
    hAngularDeviation_all = rhp.GetTH1D("hAngularDeviation_all", "",
                                       36, 0, 90, "#angle(SD,Rd) [deg]")
    hBetaSD_all = rhp.GetTH1D("hBetaSD_all", "", 30, 0, 90, "#beta_{SD}")
    hNumberOfStations_Energy_all = rhp.GetTH2D("hNumberOfStations_Energy_all", "",
                                               24, 0.5, 24.5, 20, 16, 19,
                                               "number of stations",
                                               "log_{10}(energy [EeV])")
    hNumberOfStations_Zenith_all = rhp.GetTH2D("hNumberOfStations_Zenith_all", "",
                                               24, 0.5, 24.5, 18, 0, 90,
                                               "number of stations",
                                               "zenith [deg]")

    if(extra_events):
        hEnergy = hEnergy_all.Clone("hEnergy_all")
        hZenith = hZenith_all.Clone("hZenith")
        hAzimuth = hAzimuth_all.Clone("hAzimuth")
        # SD related quantities
        hSinAlpha = hSinAlpha_all.Clone("hSinAlpha")
        hAngularDeviation = hAngularDeviation_all.Clone("hAngularDeviation")
        hBetaSD = hBetaSD_all.Clone("hBetaSD")
        hNumberOfStations_Energy = rhp.GetTH2D("hNumberOfStations_Energy", "",
                                               24, 0.5, 24.5, 20, 16, 19,
                                               "number of stations",
                                               "log_{10}(energy [EeV])")
        hNumberOfStations_Zenith = rhp.GetTH2D("hNumberOfStations_Zenith", "",
                                               24, 0.5, 24.5, 18, 0, 90,
                                               "number of stations",
                                               "zenith [deg]")

    has_SD = False
    gps_seconds = []
    gps_seconds_all = []

    ROOT.gSystem.Load("$AUGEROFFLINEROOT/lib/libRecEventKG.so")
    events = adst.RecEventProvider(data_files, "MICRO")
    geo = adst.GetDetectorGeometry(data_files[0])
    for event in events:
        rEvent = event.GetRdEvent()
        radioId = rEvent.GetRdEventId()
        runId = rEvent.GetRdRunNumber()
        radio_identifier = "%06i.%07i" % (runId, radioId)

        rShower = rEvent.GetRdRecShower()
        stations = np.array(rEvent.GetRdStationVector())
        signalstation_mask = np.array([station.HasPulse() for station in stations], dtype=np.bool)
        signal_stations = stations[signalstation_mask]
        rd_zenith = rShower.GetZenith()
        rd_azimuth = rShower.GetAzimuth()
        rd_core_fit = rShower.GetCoreSiteCS()

        if(not extra_events or radio_identifier not in event_list):
            gps_seconds_all.append(rEvent.GetGPSSecond())
            # fill histograms
            hZenith_all.Fill(np.rad2deg(rd_zenith))
            hAzimuth_all.Fill(np.rad2deg(rd_azimuth))

        if(extra_events and radio_identifier in event_list):
            gps_seconds.append(rEvent.GetGPSSecond())
            hZenith.Fill(np.rad2deg(rd_zenith))
            hAzimuth.Fill(np.rad2deg(rd_azimuth))


        # try getting SD quantities
        sdShower = event.GetSDEvent().GetSdRecShower()
        if(event.GetSDEvent().GetRecLevel() > 1.):
            has_SD = True
            ref_core = sdShower.GetCoreSiteCS()
            ref_zenith = sdShower.GetZenith()
            ref_azimuth = sdShower.GetAzimuth()
            ref_energy = sdShower.GetEnergy()
            if(ref_energy < energy_cut):
                print("energy smaller than %.g. Skipping event" % 1e17)
                continue
            angular_deviation = np.rad2deg(hp.GetAngle(
                          hp.SphericalToCartesian(ref_zenith, ref_azimuth),
                          hp.SphericalToCartesian(rd_zenith, rd_azimuth)))
            if(angular_deviation > 20):
                print "angular difference = %.1f: %s" % (angular_deviation, radio_identifier)
            betaSDmean = np.array([x.GetParameter(rdstQ.eAngleToExpectedEFieldVector) for x in signal_stations]).mean()
            if(not extra_events or radio_identifier not in event_list):
                hEnergy_all.Fill(np.log10(ref_energy))
                hAngularDeviation_all.Fill(angular_deviation)
                #print("beta = %.f" % np.rad2deg(betaSDmean))
                hBetaSD_all.Fill(np.rad2deg(betaSDmean))
            if(extra_events and radio_identifier in event_list):
                hEnergy.Fill(np.log10(ref_energy))
                hAngularDeviation.Fill(angular_deviation)
                hBetaSD.Fill(np.rad2deg(betaSDmean))

        signals = np.array([station.GetParameter(rdstQ.eSignal) for station in signal_stations])
        station_positions = np.array([geo.GetRdStationPosition(station.GetId()) for station in signal_stations])
        distances = np.array([hp.GetDistanceToShowerAxis(np.array(ref_core), ref_zenith, ref_azimuth, x) for x in station_positions])
        a = np.array
        if(not extra_events or radio_identifier not in event_list):
            hNumberOfStations_Energy_all.Fill(len(signal_stations), np.log10(ref_energy))
            hNumberOfStations_Zenith_all.Fill(len(signal_stations), np.rad2deg(ref_zenith))
        if(extra_events and radio_identifier in event_list):
            hNumberOfStations_Energy.Fill(len(signal_stations), np.log10(ref_energy))
            hNumberOfStations_Zenith.Fill(len(signal_stations), np.rad2deg(ref_zenith))
#             rhp.plot_map(ref_core, ref_azimuth, station_positions, signals,
#                             positions_all=zip(a(geo.GetRdStationXPositions()),
#                                               a(geo.GetRdStationYPositions()),
#                                               a(geo.GetRdStationZPositions())),
#                             filename=os.path.join(plot_folder, "station_plots", "%s_map.png" % radio_identifier),
#                             text='%s: E=%.1fEeV #theta=%.0f#circ' % (radio_identifier, ref_energy * 1.e-18, np.rad2deg(ref_zenith)))

    # make event time histogram
    gps_seconds_all = np.array(gps_seconds_all)
    t_min = gps_seconds_all.min()
    t_max = gps_seconds_all.max()
    hEventTime_all = rhp.GetTH1D("hEventTime_all", "", 30, t_min, t_max, "event time")
    np.vectorize(lambda x: hEventTime_all.Fill(x))(gps_seconds_all)
    if(extra_events):
        gps_seconds = np.array(gps_seconds)
        hEventTime = rhp.GetTH1D("hEventTime", "", 30, t_min, t_max, "event time")
        np.vectorize(lambda x: hEventTime.Fill(x))(gps_seconds)
        hEventTime.SetStats(0)  # stats for event time histogram do not make sense
    hEventTime_all.GetXaxis().SetTimeDisplay(1)
    hEventTime_all.GetXaxis().SetTimeFormat("%b%y%F1980-01-06 00:00:01")
    hEventTime_all.GetXaxis().SetNdivisions(5, 5, 1, 0)
    hEventTime_all.GetXaxis().SetLabelOffset(0.02)
#     hEventTime_all.GetXaxis().SetTitleOffset(1.5)

    h_all = [hZenith_all, hAzimuth_all, hEventTime_all]
    if(extra_events):
        h = [hZenith, hAzimuth, hEventTime]
    if(has_SD):
        h_all.append(hEnergy_all)
        h_all.append(hAngularDeviation_all)
        h_all.append(hBetaSD_all)
        if(extra_events):
            h.append(hEnergy)
            h.append(hAngularDeviation)
            h.append(hBetaSD)
    if(extra_events):
        rhp.plot_histograms(h_all, h, "Stats", plot_folder=plot_folder)
    else:
        rhp.plot_histograms(h_all, None, "Stats", plot_folder=plot_folder)

    if(has_SD):
        h_all = [hNumberOfStations_Energy_all, hNumberOfStations_Zenith_all]
        if(extra_events):
            h = [hNumberOfStations_Energy, hNumberOfStations_Zenith]
        else:
            h = None
        rhp.plot_histograms(h_all, h, "NumberOfStations", plot_folder=plot_folder)




