#!/usr/bin/env python
# -*- coding: utf-8 -*-


from ROOT import TH1D, TGraph, TLegend, TVector3, TF1
from ROOT import TCanvas, TRandom3

import sys
import numpy as np
import os
import glob
import MySQLdb
import pickle
from datetime import datetime, date, time, timedelta


def GetWeatherDataFromDB(datetime1, datetime2, interval=3600., weatherstation='Coihueco'):
    """ The function returns a list of average
            - temperature
            - hydrometry
            - pressure
            - rain intensities
        weather station between datetime1 and datetime2
        with 'interval' the interval in seconds over which should be averaged
        can be given. The default is 1 hour."""
    mysql_opts = {
        'host': "paomondb.physik.uni-wuppertal.de",
        'user': "RioGrande",
        'pass': "Yannick300"
    }
    # mysql_opts = {
        # 'host': "moni.auger.org.ar",
        # 'user': "Mocca",
        # 'pass': "Sibyll",
        # 'db':   "AERA"
        # }

    DB = "AugerMonitor"
    if (weatherstation == "CRS") or (weatherstation == "AERA"):
        DB = "AERA"

    mysql = MySQLdb.connect(mysql_opts['host'], mysql_opts['user'], mysql_opts['pass'], DB)

    mysql.apilevel = "2.0"
    mysql.threadsafety = 2
    mysql.paramstyle = "format"
    cursor = mysql.cursor()
    query = "select FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(`Time`))), AVG(`Temperature`), AVG(`Humidity`), AVG(`Pressure`) from\
             `Weather` where `WeatherStation` = '" + weatherstation + "' and `Time` between %s AND %s group by FLOOR(UNIX_TIMESTAMP(`Time`)/" + str(interval) + ") order by `Time` asc"
    if (weatherstation == "CRS"):
        query = "select FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(`TimeStamp`))), AVG(`Temperature`), AVG(`Hydrometry`), AVG(`Pressure`), AVG(`Rintensity`) from\
                 `Weather` where `TimeStamp` between %s AND %s group by FLOOR(UNIX_TIMESTAMP(`TimeStamp`)/" + str(interval) + ") order by `TimeStamp` asc"
    if (weatherstation == "AERA"):
        query = "select FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(`TimeStamp`))), AVG(`Temperature`), AVG(`Hydrometry`), AVG(`Pressure`), AVG(`Rintensity`) from\
                 `WeatherAERAWS` where `TimeStamp` between %s AND %s group by FLOOR(UNIX_TIMESTAMP(`TimeStamp`)/" + str(interval) + ") order by `TimeStamp` asc"

    cursor.execute(query, (datetime1, datetime2))
    data = np.array(cursor.fetchall())
    nX, nY = data.shape
    if nY == 4:
        data = np.append(data.T, [np.zeros(len(data.T[0]))], axis=0).T
    return data


def GetEFieldDataFromDB(datetime1, datetime2):
    mysql_opts = {
        'host': "paomondb.physik.uni-wuppertal.de",
        'user': "RioGrande",
        'pass': "Yannick300",
        'db': "AERA"
    }
    # mysql_opts = {
        # 'host': "moni.auger.org.ar",
        # 'user': "Mocca",
        # 'pass': "Sibyll",
        # 'db':   "AERA"
        # }
    # print "connecting to db...."
    mysql = MySQLdb.connect(mysql_opts['host'], mysql_opts['user'], mysql_opts['pass'], mysql_opts['db'])
    # print "... done"
    mysql.apilevel = "2.0"
    mysql.threadsafety = 2
    mysql.paramstyle = "format"
    cursor = mysql.cursor()
    cursor.execute("SELECT `TIMESTAMP`, `Field` FROM `Field` WHERE `TIMESTAMP` BETWEEN  %s AND %s", (datetime1, datetime2))
    eFieldData = cursor.fetchall()
    # out1 = open("1.pickle","w")
    # pickle.dump(eFieldData,out1)
    # out1.close()
    return np.array(eFieldData)
    # return np.array(eFieldData,dtype=float)


def GPStoUTC(gps):
    return gps + 315964800


def UTCtoGPS(utc):
    return utc - 315964800


def IsThunderstormEvent(GPSSec, minutesEField=15, EThres=500, EJump=300, B=2, alpha=1, S=15):
    """ reasons (return values) have the following meaning:
    0: No thunderstorm event
    1: Thunderstormevent according to criterium defined in PhD. Thesis of A. Nehls p.35ff
    4: No entry in DB for this time period
    """
    utcTime2 = datetime.utcfromtimestamp(GPStoUTC(GPSSec))
    utcTime1 = utcTime2 - timedelta(minutes=(minutesEField + 1))

    print "searching for thunderstorm event in the time interval", utcTime1, " and ", utcTime2

    eFieldData = GetEFieldDataFromDB(utcTime1, utcTime2)
    # for line in eFieldData:
      # print line

    if(eFieldData.size < 2):
        print "No EField Data in this Time Period"
        return 4
    eFieldData = np.array(eFieldData[:, 1], dtype=float)
    nY = eFieldData.size

    Vmean = np.zeros(minutesEField)
    Vstd = np.zeros(minutesEField)

    for i in range(minutesEField):
        if(len(eFieldData[i * 60:(i + 1) * 60]) < 10):
            continue

        Vmean[i] = eFieldData[i * 60:(i + 1) * 60].mean()
        Vstd[i] = eFieldData[i * 60:(i + 1) * 60].std()

    condition1 = 0

    for i, efield in enumerate(eFieldData):
        if(abs(efield) > EThres):
            condition1 = 1
    if(not condition1):
        print "Condition 1 (no fair weather conditions) not fulfilled -> no thunderstorm event"
        return 0
    else:
        print "Condition 1 (no fair weather conditions) fulfilled -> potential thunderstorm event"

    condition2 = 0
    condition3 = 0
    for i in range(1, nY):
        if(abs(eFieldData[i] - eFieldData[i - 1]) > EJump):
            condition2 = 1
            if((abs(eFieldData[i] - eFieldData[i - 1]) > B * Vstd[int(i / 60)]) and (abs(eFieldData[i] - eFieldData[i - 2]) > B * Vstd[int(i / 60)])):
                condition3 = 1
    if(not condition2):
        print "Condition 2 (significant jump) not fulfilled -> no thunderstorm event"
        return 0
    else:
        if(not condition3):
            print "Condition 2 (significant jump) fulfilled, but condition3 (jump greater than RMS) not fulfilled -> no thunderstorm event"
            return 0
        else:
            print "Condition 2 (significant jump) and condition3 (jump greater than RMS) fulfilled -> potential thunderstorm event"

    condition4 = 0
    condition5 = 0
    w = 3
    for i in range(1 + w, nY - w):
        theta1 = np.arctan(1. * (eFieldData[i - 1] - eFieldData[i - 1 - w]) / w)
        theta2 = np.arctan(1. * (eFieldData[i + w] - eFieldData[i]) / w)
        DS = theta1 - theta2
        if(abs(DS) > np.deg2rad(alpha)):
            condition4 = 1

        if(abs(eFieldData[i] - eFieldData[i - 1]) > S * Vstd[int(i / 60)]):
            condition5 = 1

    if(not (condition4 or condition5)):
        print "condition4 (slope change) and condition5 (sudden lightning discharge) not fullfilled -> no thunderstorm event"
        return 0
    else:
        print "condition4 (slope change) or condition5 (sudden lightning discharge) fullfilled -> thunderstorm event"

    return 1


def IsThunderstormEventBLS(eFieldData, minutesEField=15, EThres=500, EJump=300, B=2, alpha=1, S=15):
    # minutesEField = int(len(eFieldData)/60)
    # utcTime2 = datetime.utcfromtimestamp(GPStoUTC(GPSSec))
    # utcTime1 = utcTime2 - timedelta(minutes=(minutesEField+1))

    # print "searching for thunderstorm event in the time interval", utcTime1, " and ", utcTime2

    nY = len(eFieldData)

    Vmean = np.zeros(minutesEField)
    Vstd = np.zeros(minutesEField)
    for i in range(minutesEField):

        Vmean[i] = eFieldData[i * 60:(i + 1) * 60].mean()
        Vstd[i] = eFieldData[i * 60:(i + 1) * 60].std()

    condition1 = 0

    for i, efield in enumerate(eFieldData):
        if(abs(efield) > EThres):
            condition1 = 1
    if(not condition1):
        print "Condition 1 (no fair weather conditions) not fulfilled -> no thunderstorm event"
        return 0
    else:
        print "Condition 1 (no fair weather conditions) fulfilled -> potential thunderstorm event"

    condition2 = 0
    condition3 = 0
    for i in range(1, nY):
        if(abs(eFieldData[i] - eFieldData[i - 1]) > EJump):
            condition2 = 1
            if((abs(eFieldData[i] - eFieldData[i - 1]) > B * Vstd[int(i / 60)]) and (abs(eFieldData[i] - eFieldData[i - 2]) > B * Vstd[int(i / 60)])):
                condition3 = 1
    if(not condition2):
        print "Condition 2 (significant jump) not fulfilled -> no thunderstorm event"
        return 0
    else:
        if(not condition3):
            print "Condition 2 (significant jump) fulfilled, but condition3 (jump greater than RMS) not fulfilled -> no thunderstorm event"
            return 0
        else:
            print "Condition 2 (significant jump) and condition3 (jump greater than RMS) fulfilled -> potential thunderstorm event"

    condition4 = 0
    condition5 = 0
    w = 3
    for i in range(1 + w, nY - w):
        theta1 = np.arctan(1. * (eFieldData[i - 1] - eFieldData[i - 1 - w]) / w)
        theta2 = np.arctan(1. * (eFieldData[i + w] - eFieldData[i]) / w)
        DS = theta1 - theta2
        if(abs(DS) > np.deg2rad(alpha)):
            condition4 = 1

        if(abs(eFieldData[i] - eFieldData[i - 1]) > S * Vstd[int(i / 60)]):
            condition5 = 1

    if(not (condition4 or condition5)):
        print "condition4 (slope change) and condition5 (sudden lightning discharge) not fullfilled -> no thunderstorm event"
        return 0
    else:
        print "condition4 (slope change) or condition5 (sudden lightning discharge) fullfilled -> thunderstorm event"

    return 1


def IsThunderstormEvent2(GPSSec, minutesEField=15, EMin=-150, EMax=50, RMS=30):
    """ reasons (return values) have the following meaning:
    0: No thunderstorm event
    2: Thunderstormevent according to the criteria that mean and rms of efield strength are above some threshold value
    4: No entry in DB for this time period
    """
    utcTime2 = datetime.utcfromtimestamp(GPStoUTC(GPSSec))
    utcTime1 = utcTime2 - timedelta(minutes=(minutesEField + 1))

    print "searching for thunderstorm event in the time interval", utcTime1, " and ", utcTime2

    eFieldData = GetEFieldDataFromDB(utcTime1, utcTime2)

    if(eFieldData.size < 2):
        print "No EField Data in this Time Period"
        return 4
    eFieldData = np.array(eFieldData[:, 1], dtype=float)
    nY = eFieldData.size

    Vmean = np.zeros(minutesEField)
    Vstd = np.zeros(minutesEField)
    for i in range(minutesEField):

        Vmean[i] = eFieldData[i * 60:(i + 1) * 60].mean()
        Vstd[i] = eFieldData[i * 60:(i + 1) * 60].std()

    condition1 = 0

    for i, efield in enumerate(eFieldData[:]):
        if((efield > EMax) or (efield < EMin)):
            condition1 = 1

    condition2 = 0
    for i, rms in enumerate(Vstd):
        if(rms > RMS):
            condition2 = 1

    if(not (condition1 or condition2)):
        print "Condition 1 (E < EMin or E > EMax) and Condition 2 (RMS<E> > RMSMax) not fulfilled -> no thunderstorm event"
        return 0
    else:
        if(condition1):
            print "Condition 1 (E < EMin or E > EMax) fulfilled -> thunderstorm event"
        if(condition2):
            print "Condition 2 (RMS<E> > RMSMax) -> thunderstorm event"
        return 2


def PlotEFieldData(GPSSec, runNumber, rdeventId, title="", outputFolder="output", minutesEField=15):
    utcTime2 = datetime.utcfromtimestamp(GPStoUTC(GPSSec))
    utcTime1 = utcTime2 - timedelta(minutes=(minutesEField + 1))

    print "plotting efield data in the time interval", utcTime1, " and ", utcTime2

    eFieldData = GetEFieldDataFromDB(utcTime1, utcTime2)
    if(eFieldData.size < 2):
        print "No EField Data in this Time Period"
        return 4
    eField = np.array(eFieldData[:, 1], dtype=float)
    nY, nX = eFieldData.shape

    Vmean = np.zeros(minutesEField)
    Vstd = np.zeros(minutesEField)
    gEField = TGraph()
    gMeanEField = TGraph()
    gRMSEField = TGraph()
    for i in range(minutesEField):
        Vmean[i] = eField[i * 60:(i + 1) * 60].mean()
        Vstd[i] = eField[i * 60:(i + 1) * 60].std()
        time = eFieldData[i * 60, 0]
        UTCStart = datetime.fromtimestamp(0)
        utctimestamp = (time - UTCStart).total_seconds()
        gMeanEField.SetPoint(gMeanEField.GetN(), utctimestamp, Vmean[i])
        gRMSEField.SetPoint(gRMSEField.GetN(), utctimestamp, Vstd[i])

    for i in range(nY):
        time = eFieldData[i, 0]
        UTCStart = datetime.fromtimestamp(0)
        utctimestamp = (time - UTCStart).total_seconds()
        gEField.SetPoint(gEField.GetN(), utctimestamp, eFieldData[i, 1])
    c1 = TCanvas("c1", "", 1400, 800)
    c1.Divide(2, 1)
    c1.cd(1)
    gEField.GetXaxis().SetTimeDisplay(1)
    gEField.GetXaxis().SetTimeFormat("#splitline{%d.%m.%y}{%H:%M}%F1970-01-01 00:00:01")
    gEField.GetXaxis().SetTitle("time (15min before event)")
    gEField.GetXaxis().SetNdivisions(5, 5, 1, 0)
    gEField.GetXaxis().SetLabelOffset(0.02)
    gEField.GetXaxis().SetTitleOffset(1.5)
    gEField.GetYaxis().SetTitle("electric field [V/m]")
    gEField.SetTitle("Rd Event " + str(runNumber) + "." + str(rdeventId) + str(title))
    gEField.Draw("AL")
    c1.cd(2)
    gRMSEField.GetXaxis().SetTimeDisplay(1)
    gRMSEField.GetXaxis().SetTimeFormat("#splitline{%d.%m.%y}{%H:%M}%F1970-01-01 00:00:01")
    gRMSEField.GetXaxis().SetTitle("time (15min before event)")
    gRMSEField.GetXaxis().SetNdivisions(5, 5, 1, 0)
    gRMSEField.GetXaxis().SetLabelOffset(0.02)
    gRMSEField.GetXaxis().SetTitleOffset(1.5)
    gRMSEField.GetYaxis().SetTitle("RMS of electric field [V/m]")
    gRMSEField.SetTitle("Rd Event " + str(runNumber) + "." + str(rdeventId) + str(title))
    gRMSEField.Draw("APL")
    filename = os.path.join(outputFolder, str(runNumber) + "." + str(rdeventId) + ".png")
    c1.SaveAs(filename)
    # try: # so windows don't close
        # from time import sleep
        # while len(ROOT.gROOT.GetListOfCanvases())>0:
            # sleep(0.5)
    # except KeyboardInterrupt:
        # raise SystemExit()
    # except :
        # print "Hilfe!"

    return 0


def datetimeToUTC(dt):
    UTCStart = datetime(1970, 1, 1, 00, 00, 00)
    return (dt - UTCStart).total_seconds()


def datetimeToGPS(dt):
    GPSStart = datetime(1980, 1, 6, 00, 00, 00)
    return (dt - GPSStart).total_seconds()


# Test Code:
# query database for two events that has been identified as thunderstorm events (as stated in the wiki)
if __name__ == "__main__":
    import locale
    import matplotlib.pyplot as plt
    from matplotlib import dates
    import pytz
    start = datetime(2014, 3, 1, 00, 00, 00)
    stop = datetime(2014, 3, 2, 00, 00, 00)
    data = GetWeatherDataFromDBAERA(start, stop)
    fig, ax = plt.subplots(1, 1)
    locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
    tt = [a.hour + a.minute / 60. + a.second / 3600. for a in data[..., 0]]
    ax.plot(tt, data[..., 1], "-", label=start.strftime("%x"))

    start = datetime(2014, 6, 1, 00, 00, 00)
    stop = datetime(2014, 6, 2, 00, 00, 00)
    data = GetWeatherDataFromDBAERA(start, stop)
    tt = [a.hour + a.minute / 60. + a.second / 3600. for a in data[..., 0]]
    ax.plot(tt, data[..., 1], "-", label=start.strftime("%x"))

    start = datetime(2014, 9, 1, 00, 00, 00)
    stop = datetime(2014, 9, 2, 00, 00, 00)
    data = GetWeatherDataFromDBAERA(start, stop)
    tt = [a.hour + a.minute / 60. + a.second / 3600. for a in data[..., 0]]
    ax.plot(tt, data[..., 1], "-", label=start.strftime("%x"))

    start = datetime(2014, 12, 10, 00, 00, 00)
    stop = datetime(2014, 12, 11, 00, 00, 00)
    data = GetWeatherDataFromDBAERA(start, stop)
    tt = [a.hour + a.minute / 60. + a.second / 3600. for a in data[..., 0]]
    ax.plot(tt, data[..., 1], "-", label=start.strftime("%x"))
    ax.legend()
    ax.set_ylabel(r"temperature [$^\circ$C]")
    ax.set_xlabel("hours of day [h]")
    ax.set_xlim(0, 24)
    plt.tight_layout()
    fig.savefig("temperature_daily_2014.pdf")

    start = datetime(2014, 1, 1, 00, 00, 00)
    stop = datetime(2015, 1, 1, 00, 00, 00)
    data = GetWeatherDataFromDBAERA(start, stop, 60 * 60 * 24 * 7)
    fig, ax = plt.subplots(1, 1)
    locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
    ax.plot_date(data[..., 0], data[..., 1], "-o")
    fig.autofmt_xdate()
    ax.set_ylabel(r"temperature [$^\circ$C]")
    plt.tight_layout()
    fig.savefig("temperature_2014.pdf")
    plt.show()

    a = 1 / 0

    GPSStart = datetime(1980, 1, 6, 00, 00, 00)

    # Wed_Jan_18_22:14:13_2012
    dt_thunder1 = datetime(2012, 1, 18, 22, 14, 13)
    GPSSeconds = (dt_thunder1 - GPSStart).total_seconds()  # convert datetime to GPS Seconds
    IsThunderstormEvent(GPSSeconds)
    IsThunderstormEvent2(GPSSeconds)
    PlotEFieldData(GPSSeconds, 0o001, 12345)

    # Mon_Jan_23_21:37:35_2012
    # dt_thunder2 = datetime(2012,1,23,21,37,35)
    # GPSSeconds = (dt_thunder2-GPSStart).total_seconds()   # convert datetime to GPS Seconds
    # IsThunderstormEvent(GPSSeconds)
