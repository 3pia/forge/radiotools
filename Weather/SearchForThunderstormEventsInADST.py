#!/usr/bin/env python
# -*- coding: utf-8 -*-


# from ROOT import *
import ROOT
from ROOT import TH1D, TGraph, TLegend, TVector3, TF1
from ROOT import TCanvas, TRandom3
import math
import sys
from datetime import datetime, date, time, timedelta
from EFieldDataFromDB import *

ROOT.gROOT.SetBatch(1)

# Test Code:
if __name__ == "__main__":
    print "parameters: file1 [file2 file3 ...]"

    documentation = """ reasons have the following meaning:
    0: No thunderstorm event
    1: Thunderstormevent according to criterium defined in PhD. Thesis of A. Nehls p.35ff
    2: Thunderstormevent according to the criteria that mean and rms of efield strength are above some threshold value
    4: No entry in DB for this time period
    """
    print "documentation:"
    print documentation

    print "reading in Radio data files..."
    DataFiles = []
    for i in range(1, len(sys.argv)):
        print "appending ", sys.argv[i]
        DataFiles.append(sys.argv[i])
    files = ROOT.std.vector('string')()
    for val in DataFiles:
        files.push_back(val)
    print "... done"

    ROOT.gSystem.Load("$AUGEROFFLINEROOT/lib/libRecEventKG.so")
    ROOT.gStyle.SetLineWidth(2)

    file = ROOT.RecEventFile(files)
    event = ROOT.RecEvent()
    file.SetBuffers(event)
    # geo = ROOT.DetectorGeometry()
    # file.ReadDetectorGeometry(geo)

    fout = open("thunderstormevents.txt", "w")
    fout.write("#runNumber\tradioId\tSDId\tGPSSec\treason\n")

    print "looping through radio data"
    while file.ReadNextEvent() == ROOT.RecEventFile.eSuccess:
        rEvent = event.GetRdEvent()
        rShower = rEvent.GetRdRecShower()
        sEvent = event.GetSDEvent()
        sShower = sEvent.GetSdRecShower()

        runNumber = rEvent.GetRdRunNumber()
        radioId = rEvent.GetRdEventId()
        SDId = sEvent.GetEventId()

        GPSSec = rEvent.GetGPSSecond()
        dt = datetime.utcfromtimestamp(GPStoUTC(GPSSec))
        print

        reason = IsThunderstormEvent(GPSSec) | IsThunderstormEvent2(GPSSec)
        if(reason):
            fout.write(str(runNumber) + "\t" + str(radioId) + "\t" + str(SDId) + "\t" + str(GPSSec) + "\t" + str(reason) + "\t" + str(dt) + "\n")
            print "---------------"
            print "Thunderstorm Event found: ", runNumber, radioId
        if(reason):
            pass
            PlotEFieldData(GPSSec, runNumber, radioId, " Thunderstorm event: " + bin(reason), "output")

    fout.close()
