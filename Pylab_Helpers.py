import inspect, re
import math
import os

from scipy import optimize

import matplotlib.pyplot as plt
import numpy as np


def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)


def fit_chi2(func, datax, datay, datayerror, p_init=None):
    datayerror[datayerror == 0] = 1
    popt, cov = optimize.curve_fit(func, datax, datay, sigma=datayerror,
                                     p0=p_init)
#     print optimize.curve_fit(func, datax, datay, sigma=datayerror,
#                                p0=p_init, full_output=True)
    y_fit = func(datax, *popt)
    chi2 = np.sum((y_fit - datay) ** 2 / datayerror ** 2)
    if(len(datax) != len(popt)):
        chi2ndf = 1. * chi2 / (len(datax) - len(popt))
    else:
        chi2ndf = np.NAN
    func2 = lambda x: func(x, *popt)
    print "result of chi2 fit:"
    print "\tchi2/ndf = %0.2g/%i = %0.2g" % (chi2, len(datax) - len(popt), chi2ndf)
    print "\tpopt = ", popt
    print "\tcov = ", cov
    return popt, cov, chi2ndf, func2


def fit_line(datax, datay, datayerror=None, p_init=None):
    if datayerror is None:
        datayerror = np.ones_like(datay)
    func = lambda x, p0, p1: x * p1 + p0
    return fit_chi2(func, datax, datay, datayerror, p_init=p_init)


def fit_pol2(datax, datay, datayerror=None, p_init=None):
    if datayerror is None:
        datayerror = np.ones_like(datay)
    func = lambda x, p0, p1, p2: x ** 2 * p2 + x * p1 + p0
    return fit_chi2(func, datax, datay, datayerror, p_init=p_init)

#     popt, cov = optimize.curve_fit(lambda x, p0, p1: x * p1 + p0, datax, datay,
#                                    sigma=datayerror)
#     y_fit = popt[1] * datax + popt[0]
#     chi2 = np.sum((y_fit - datay) ** 2 / datayerror ** 2)
#     chi2ndf = 1. * chi2 / (len(datax) - 2)
#     func = lambda x: popt[1] * x + popt[0]
#     return popt, cov, chi2ndf, func


def fit_gaus(datax, datay, datayerror, p_init=None):
    func = lambda x, p0, p1, p2: p0 * (2 * np.pi * p2) ** -0.5 * np.exp(-0.5 * (x - p1) ** 2 * p2 ** -2)
#     if(p_init == None):
#         p_init = [datay.max()*(2 * np.pi * datay.std())**0.5, datay.mean(), datay.std()]
    return fit_chi2(func, datax, datay, datayerror, p_init=p_init)


def plot_fit_stats(ax, popt, cov, chi2ndf, posx=0.95, posy=0.95, ha='right',
                   significant_figure=True, color='k', funcstring='', parnames=None):
    textstr = ''
    if(funcstring != ''):
        textstr += funcstring + "\n"
    textstr += "$\chi^2/ndf=%.2g$" % chi2ndf

    if(len(popt) == 1):
        parname = "p0"
        if(parnames):
            parname = parnames[0]
        textstr += "\n$%s = %.2g \pm %.2g$" % (parname, popt[0], np.squeeze(cov) ** 0.5)
    elif(len(cov) == 1):
        for i, p in enumerate(popt):
            parname = "p%i" % i
            if(parnames):
                parname = parnames[i]
            textstr += "\n$%s = %.2g \pm %s$" % (parname, p, 'nan')
    else:
        for i, p in enumerate(popt):
            parname = "p%i" % i
            if(parnames):
                parname = parnames[i]
            if(significant_figure):
                import SignificantFigures as serror
                textstr += "\n$%s = %s \pm %s$" % ((parname,) + (serror.formatError(p, cov[i, i] ** 0.5)))
            else:
                textstr += "\n$%s = %.2g \pm %.2g$" % (parname, p, cov[i, i] ** 0.5)
    props = dict(boxstyle='square', facecolor='wheat', alpha=0.5)
    ax.text(posx, posy, textstr, transform=ax.transAxes, fontsize=14,
                verticalalignment='top', horizontalalignment=ha,
                multialignment='left', bbox=props, color=color)


def plot_fit_stats2(ax, popt, cov, chi2ndf, posx=0.95, posy=0.95):
    from MatplotlibTools import Figures
    plt.rc('text', usetex=True)
    table = Figures.Table()

    table.addValue("$\chi^2/ndf$", chi2ndf)
    if(len(cov) == 1):
        for i, p in enumerate(popt):
            table.addValue("p%i" % i, p)
    else:
        for i, p in enumerate(popt):
            table.addValue("p%i" % i, p, cov[i, i] ** 0.5)
    props = dict(boxstyle='square', facecolor='wheat', alpha=0.5)
    print table.getTable()
    ax.text(posx, posy, r'$%s$' % table.getTable(), transform=ax.transAxes, fontsize=14,
                verticalalignment='top', horizontalalignment='right',
                multialignment='left', bbox=props)


def plot_hist_stats(ax, data, weights=None, posx=0.05, posy=0.95, overflow=None,
                    underflow=None, rel=False,
                    additional_text="", additional_text_pre="",
                    fontsize=22, color="k", va="top", ha="left",
                    median=True, quantiles=True, mean=True, std=True, N=True):
    data = np.array(data)
    tmean = data.mean()
    tstd = data.std()
    if weights is not None:
        def weighted_avg_and_std(values, weights):
            """
            Return the weighted average and standard deviation.

            values, weights -- Numpy ndarrays with the same shape.
            """
            average = np.average(values, weights=weights)
            variance = np.average((values - average) ** 2, weights=weights)  # Fast and numerically precise
            return (average, variance ** 0.5)
        tmean, tstd = weighted_avg_and_std(data, weights)

    textstr = additional_text_pre
    if (textstr != ""):
        textstr += "\n"
    if N:
        textstr += "$N=%i$\n" % data.size
    import SignificantFigures as serror
    if mean:
        if weights is None:
            textstr += "$\mu = %s \pm %s$\n" % serror.formatError(tmean,
                                                tstd / math.sqrt(data.size))
            # textstr += "$\mu = %.4g$\n" % mean
        else:
            textstr += "$\mu = %.2g$\n" % tmean
    if median:
        tweights = np.ones_like(data)
        if weights is not None:
            tweights = weights
        from astrotools import stat
        if quantiles:
            q1 = stat.quantile_1d(data, tweights, 0.16)
            q2 = stat.quantile_1d(data, tweights, 0.84)
            median = stat.median(data, tweights)
            median_str = serror.formatError(median, 0.05 * (np.abs(median - q2) + np.abs(median - q1)))[0]
            textstr += "$\mathrm{median} = %s^{+%.2g}_{-%.2g}$\n" % (median_str, np.abs(median - q2),
                                                                       np.abs(median - q1))
        else:
            textstr += "$\mathrm{median} = %.2g $\n" % stat.median(data, tweights)
    if std:
        if rel:
            textstr += "$\sigma = %.2g$ (%.1f\%%)\n" % (tstd, tstd / tmean * 100.)
        else:
            textstr += "$\sigma = %.2g$\n" % (tstd)
    if(overflow):
        textstr += "$\mathrm{overflows} = %i$\n" % overflow
    if(underflow):
        textstr += "$\mathrm{underflows} = %i$\n" % underflow

    textstr += additional_text
    textstr = textstr[:-1]

    props = dict(boxstyle='square', facecolor='w', alpha=0.5)
    ax.text(posx, posy, textstr, transform=ax.transAxes, fontsize=fontsize,
            verticalalignment=va, ha=ha, multialignment='left',
            bbox=props, color=color)


def get_graph(x_data, y_data, filename=None, show=False, xerr=None, yerr=None, funcs=None,
               xlabel="", ylabel="", title="", fmt='bo', fit_stats=None,
               xmin=None, xmax=None, ymin=None, ymax=None, hlines=None,
               **kwargs):
    fig, ax1 = plt.subplots(1, 1)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax1.set_title(title)
    ax1.errorbar(x_data, y_data, xerr=xerr, yerr=yerr, fmt='bo', **kwargs)
    if(not xmin is None):
        ax1.set_xlim(left=xmin)
    if(not xmax is None):
        ax1.set_xlim(right=xmax)
    if(not ymin is None):
        ax1.set_ylim(bottom=ymin)
    if(not ymax is None):
        ax1.set_ylim(top=ymax)

    if(funcs):
        for func in funcs:
            xlim = np.array(ax1.get_xlim())
            xx = np.linspace(xlim[0], xlim[1], 100)
            if('args' in func):
                ax1.plot(xx, func['func'](xx), *func['args'])
                if('kwargs' in func):
                    ax1.plot(xx, func['func'](xx), *func['args'], **func['kwargs'])
            else:
                ax1.plot(xx, func['func'](xx))
    if(hlines):
        for hline in hlines:
            xlim = np.array(ax1.get_xlim())
            ax1.hlines(hline['y'], *xlim, **hline['kwargs'])
    if(fit_stats):
        plot_fit_stats(ax1, *fit_stats)
    plt.tight_layout()
    if(show):
        plt.show()
    return fig, ax1


def save_graph(filename=None, **kwargs):
    fig, ax1 = get_graph(**kwargs)
    if(filename and filename != ""):
        fig.savefig(filename)
    plt.close(fig)


def get_histograms(histograms, bins=None, xlabels=None, ylabels=None, stats=True,
                   fig=None, axes=None, histtype=u'bar', titles=None,
                   weights=None, figsize=4,
                   stat_kwargs=None,
                   kwargs={'facecolor': '0.7', 'alpha': 1, 'edgecolor': "k"}):
    N = len(histograms)
    if((fig is None) or (axes is None)):
        if(N == 1):
            fig, axes = get_histogram(histograms, bins=bins, xlabel=xlabels, ylabel=ylabels, title=titles,
                                      stats=stats, weights=weights)
            return fig, axes
        elif(N <= 3):
            fig, axes = plt.subplots(1, N, figsize=(figsize * N, figsize))
        elif(N == 4):
            fig, axes = plt.subplots(2, 2, figsize=(figsize * 2, figsize * 2))
        elif(N <= 6):
            fig, axes = plt.subplots(2, 3, figsize=(figsize * 3, figsize * 2))
        elif(N <= 8):
            fig, axes = plt.subplots(2, 4, figsize=(figsize * 4, figsize * 2))
        elif(N <= 9):
            fig, axes = plt.subplots(3, 3, figsize=(figsize * 3, figsize * 3))
        elif(N <= 12):
            fig, axes = plt.subplots(3, 4, figsize=(figsize * 4, figsize * 3))
        elif(N <= 16):
            fig, axes = plt.subplots(4, 4, figsize=(figsize * 4, figsize * 4))
        elif(N <= 20):
            fig, axes = plt.subplots(4, 5, figsize=(figsize * 5, figsize * 4))
        elif(N <= 25):
            fig, axes = plt.subplots(5, 5, figsize=(figsize * 5, figsize * 5))
        elif(N <= 30):
            fig, axes = plt.subplots(5, 5, figsize=(figsize * 6, figsize * 5))
        elif(N <= 35):
            fig, axes = plt.subplots(5, 7, figsize=(figsize * 7, figsize * 5))
        else:
            print "WARNING: more than 35 pads are not implemented"
            raise "WARNING: more than 35 pads are not implemented"
    shape = np.array(np.array(axes).shape)
    n1 = shape[0]
    n2 = 1
    if(len(shape) == 2):
        n2 = shape[1]
    axes = np.reshape(axes, n1 * n2)

    for i in xrange(N):
        xlabel = ""
        if xlabels:
            if(type(xlabels) != np.str):
                xlabel = xlabels[i]
            else:
                xlabel = xlabels
        ylabel = "entries"
        if ylabels:
            if(len(ylabels) > 1):
                ylabel = ylabels[i]
            else:
                ylabel = ylabels
        tbin = 10
        title = ""
        if titles:
            title = titles[i]
        if bins is not None:
            if(type(bins) == np.int):
                tbin = bins
            else:
                if (isinstance(bins[0], float) or isinstance(bins[0], int)):
                    tbin = bins
                else:
                    tbin = bins[i]
        if(len(histograms[i])):
            tweights = None
            if weights is not None:
                if (isinstance(weights[0], float) or isinstance(weights[0], int)):
                    tweights = weights
                else:
                    tweights = weights[i]
            n, binst, patches = axes[i].hist(histograms[i], bins=tbin,
                                             histtype=histtype,
                                             weights=tweights, **kwargs)
            axes[i].set_xlabel(xlabel)
            axes[i].set_ylabel(ylabel)
            ymax = max(axes[i].get_ylim()[1], 1.2 * n.max())
            axes[i].set_ylim(0, ymax)
            axes[i].set_xlim(binst[0], binst[-1])
            axes[i].set_title(title)
            underflow = np.sum(histograms[i] < binst[0])
            overflow = np.sum(histograms[i] > binst[-1])
            if isinstance(stats, bool):
                if stats:
                    if(stat_kwargs is None):
                        plot_hist_stats(axes[i], histograms[i], weights=tweights,
                                        overflow=overflow, underflow=underflow)
                    else:
                        plot_hist_stats(axes[i], histograms[i], weights=tweights,
                                        overflow=overflow, underflow=underflow, **stat_kwargs)
            else:
                if(stats[i]):
                    if(stat_kwargs is None):
                        plot_hist_stats(axes[i], histograms[i], weights=tweights,
                                        overflow=overflow, underflow=underflow)
                    else:
                        plot_hist_stats(axes[i], histograms[i], weights=tweights,
                                        overflow=overflow, underflow=underflow, **stat_kwargs)
    fig.tight_layout()
    return fig, axes


def get_histogram(data, bins=10, xlabel="", ylabel="entries", weights=None,
                  title="", stats=True, show=False, stat_kwargs=None, funcs=None, overflow=True,
                  ax=None, kwargs={'facecolor':'0.7', 'alpha':1, 'edgecolor':"k"}):
    """ creates a histogram using matplotlib from array """
    if(ax is None):
        fig, ax1 = plt.subplots(1, 1)
    else:
        ax1 = ax

    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax1.set_title(title)
    n, bins, patches = ax1.hist(data, bins, normed=0, weights=weights, **kwargs)
    if(funcs):
        for func in funcs:
            xlim = np.array(ax1.get_xlim())
            xx = np.linspace(xlim[0], xlim[1], 100)
            if('args' in func):
                ax1.plot(xx, func['func'](xx), *func['args'])
                if('kwargs' in func):
                    ax1.plot(xx, func['func'](xx), *func['args'], **func['kwargs'])
            else:
                ax1.plot(xx, func['func'](xx))
    ax1.set_ylim(0, n.max() * 1.2)
    ax1.set_xlim(bins[0], bins[-1])

    if stats:
        if overflow:
            underflow = np.sum(data < bins[0])
            overflow = np.sum(data > bins[-1])
        else:
            underflow = None
            underflow = None
        if(stat_kwargs is None):
            plot_hist_stats(ax1, data, overflow=overflow, underflow=underflow, weights=weights)
        else:
            plot_hist_stats(ax1, data, overflow=overflow, underflow=underflow, weights=weights, **stat_kwargs)
    if(show):
        plt.show()
    if(ax is None):
        return fig, ax1


def save_histogram(filename, *args, **kwargs):
    fig, ax = get_histogram(*args, **kwargs)
    fig.savefig(filename)
    plt.close(fig)


def varname(p):
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\bvarname\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            return m.group(1)


def make_dir(path):
    if(not os.path.isdir(path)):
        os.makedirs(path)


def get_marker(i):
    colors = ["b", "g", "r", "c", "m", "k"]
    markers = ["o", "^", "D", "s"]
    return colors[i % len(colors)] + markers[i / len(colors)]


def get_marker2(i):
    colors = ["b", "g", "r", "c", "m", "k"]
    markers = ["o", "^", "D", "s"]
    return colors[i % len(colors)] + markers[i % len(colors)]


def get_color(i):
    colors = ["b", "g", "r", "c", "m", "k"]
    return colors[i % len(colors)]




