# Simulation of noisepower for one single lst day
import os
import numpy as np
from scipy import constants

from astrotools import coord
from astrotools import healpytools as healpy
from AntennaPattern.antenna_pattern import AntennaInterpolation


def galactic_noise(latitude, freq, lsts, antenna_model, antenna_pattern_path=None,
                   LFMap_path=None, output_folder=None, datacorrection=False):
    """
    Calculates galactic noise for given geographical latitude, frequency [MHz],
    array of local sidereal times [rad],
    zenith and azimuth angles [rad] in auger system (E = 0, N = pi/2, W = pi, S = -pi/2)
    Requieres path to LFmap data file and path where to save the output
    datacorrection=False does not use correcting factors for antenna pattern
    datacorrection=True uses correcting factors for antenna pattern
    """

    # define azimuth and zenith binned in 1 degree steps (Auger coordinates!)
    az = np.arange(-180., 180., 1.)
    az = np.deg2rad(az)
    zen = np.arange(0., 90., 1.)
    zen = np.deg2rad(zen)

    # load antenna model
    if(antenna_model == "LPDA"):
        print("loading LPDA antenna model")
        small_black_spider = AntennaInterpolation("SmallBlackSpider_ground2", antenna_pattern_path)
        antenna_model_ns = small_black_spider
        antenna_model_ew = small_black_spider
    if(antenna_model == "butterfly"):
        print "loading butterfly antenna model"
        antenna_model_ns = AntennaInterpolation("Butterfly_ground2_North")
        antenna_model_ew = AntennaInterpolation("Butterfly_ground2_East")

    # deviation of magnetic north from geographical north
    magnetic_deviation = -0.0607375

    # find all possible angle combinations
    M = np.meshgrid(az, zen)
    azs = M[0].flatten()
    zens = M[1].flatten()

    # spherical element for each angle-tuple
    delta_omega = ((azs + np.deg2rad(0.5)) - (azs - np.deg2rad(0.5))) * \
                    np.sin(zens) * ((zens + np.deg2rad(0.5)) - (zens - np.deg2rad(0.5)))

    # read in LFMap for specific frequency (in equatorial coordinates)
    filename = os.path.join(LFMap_path, "LFmap_%.1f_EQ.txt" % (freq))
    N, ras, decs, intensities = np.loadtxt(filename, unpack=True)
    ras = np.deg2rad(ras)  # conversion from degree to radians
    decs = np.deg2rad(decs)

    # create healpix map
    n_side = 2 ** 6
    npix = healpy.nside2npix(n_side)
    h_pixels = healpy.vec2pix(n_side, *coord.ang2vec(ras, decs))
    noisemap = np.zeros(npix)
    counter = np.zeros(npix)
    for i in xrange(len(ras)):
        counter[h_pixels[i]] += 1
        noisemap[h_pixels[i]] += intensities[i]
    noisemap /= counter

    # convert map from brightness temperature into spectral radiance [W/sr/m^2/Hz] using Rayleigh-Jeans law
    noisemap *= 2 * (freq * 1e6) ** 2 * constants.k / constants.c ** 2
    intensities *= 2 * (freq * 1e6) ** 2 * constants.k / constants.c ** 2
    noise_powers_antenna_ch1 = np.zeros(len(lsts))
    noise_powers_antenna_ch2 = np.zeros(len(lsts))

    orientation_azimuth = magnetic_deviation
    if not datacorrection:
        VEL_theta_ch2, VEL_phi_ch2 = antenna_model_ew.get_antenna_response_vectorized(freq, zens, azs - orientation_azimuth)
        orientation_azimuth = magnetic_deviation + np.pi * 0.5
        VEL_theta_ch1, VEL_phi_ch1 = antenna_model_ns.get_antenna_response_vectorized(freq, zens, azs - orientation_azimuth)
    else:
        VEL_theta_ch2, VEL_phi_ch2 = antenna_model_ew.get_antenna_response_datacorrected_vectorized(freq, zens, azs - orientation_azimuth)
        orientation_azimuth = magnetic_deviation + np.pi * 0.5
        VEL_theta_ch1, VEL_phi_ch1 = antenna_model_ns.get_antenna_response_datacorrected_vectorized(freq, zens, azs - orientation_azimuth)
    magnitude_ch1 = np.abs(VEL_theta_ch1) ** 2 + np.abs(VEL_phi_ch1) ** 2
    magnitude_ch2 = np.abs(VEL_theta_ch2) ** 2 + np.abs(VEL_phi_ch2) ** 2

    print "Simulating noise local coordinates f = %.1f MHz" % (freq)

    import time
    t = time.time()
    Z0overZL = 120. * np.pi / 50.
    # azs += np.pi * 0.5
    hourangles_test, decs_test = coord.altaz2hourangledec(0.5 * np.pi - zens, azs, latitude)  # this transformation of zenith angles is necessary because we initialized them using the auger convertion with 0 [deg] being the zenith
    for i, lst in enumerate(lsts):
#         ras_test, decs_test = coord.altaz2eq(0.5 * np.pi - zens, azs, latitude, lst)  # this transformation of zenith angles is necessary because we initialized them using the auger convertion with 0 [deg] being the zenith
        ras_test = lst - hourangles_test
        pix_test = healpy.vec2pix(n_side, *coord.ang2vec(ras_test, decs_test))
        power = noisemap[pix_test]

        noise_powers_antenna_ch1_temp = Z0overZL * np.sum(0.5 * power * delta_omega * magnitude_ch1 * np.cos(zens))
        noise_powers_antenna_ch2_temp = Z0overZL * np.sum(0.5 * power * delta_omega * magnitude_ch2 * np.cos(zens))
        noise_powers_antenna_ch1[i] = noise_powers_antenna_ch1_temp
        noise_powers_antenna_ch2[i] = noise_powers_antenna_ch2_temp
    print "loop through lst took %.1f seconds" % (time.time() - t)
    # convert noise powers from [W/Hz] to [W/MHz]
    noise_powers_antenna_ch1 *= 1e6
    noise_powers_antenna_ch2 *= 1e6
    if output_folder is not None:
        np.savetxt(os.path.join(output_folder, "galactic_noise_simulation_%.1f.out" % (freq)),
                   np.c_[lsts, noise_powers_antenna_ch1, noise_powers_antenna_ch2],
                   '%1.4e', delimiter=' ', newline='\n',
                   header='f = %.1f MHz \n lst[rad] - noise power ch1[W/MHz] - noise power ch2[W/MHz]' % (freq))

    return noise_powers_antenna_ch1, noise_powers_antenna_ch2


def galactic_noise_pixel(latitude, freq, lsts, antenna_model, antenna_pattern_path=None,
                         LFMap_path=None, output_folder=None, datacorrection=False):
    """
    Calculates galactic noise for given geographical latitude, frequency [Hz],
    array of local sidereal times [rad],
    zenith and azimuth angles [rad] in auger system (E = 0, N = pi/2, W = pi, S = -pi/2)
    Requieres path to LFmap data file and path where to save the output
    datacorrection=False does not use correcting factors for antenna pattern
    whereas datacorrection=True uses correcting factors
    Simulation iterates pixels not angles
    """

    # load antenna model
    if(antenna_model == "LPDA"):
        print("loading LPDA antenna model")
        small_black_spider = AntennaInterpolation("SmallBlackSpider_ground2", antenna_pattern_path)
        antenna_model_ns = small_black_spider
        antenna_model_ew = small_black_spider

    if(antenna_model == "butterfly"):
        print "loading butterfly antenna model"
        antenna_model_ns = AntennaInterpolation("Butterfly_ground2_North")
        antenna_model_ew = AntennaInterpolation("Butterfly_ground2_East")

    # deviation of magnetic north from geographical north
    magnetic_deviation = -0.0607375

    # read in LFMap for specific frequency (in equatorial coordinates)
    filename = os.path.join(LFMap_path, "LFmap_%.1f_EQ.txt" % (freq))
    N, ras, decs, intensities = np.loadtxt(filename, unpack=True)
    ras = np.deg2rad(ras)  # conversion from degree to radians
    decs = np.deg2rad(decs)

    # create healpix map
    n_side = 2 ** 6
    npix = healpy.nside2npix(n_side)
    h_pixels = healpy.vec2pix(n_side, *coord.ang2vec(ras, decs))
    noisemap = np.zeros(npix)
    counter = np.zeros(npix)
    for i in xrange(len(ras)):
        counter[h_pixels[i]] += 1
        noisemap[h_pixels[i]] += intensities[i]
    noisemap /= counter

    # convert map from brightness temperature into spectral radiance [W/sr/m^2/Hz] using Rayleigh-Jeans law
    noisemap *= 2 * (freq * 1e6) ** 2 * constants.k / constants.c ** 2
    intensities *= 2 * (freq * 1e6) ** 2 * constants.k / constants.c ** 2
    noise_powers_antenna_pix_ch1 = np.zeros(len(lsts))
    noise_powers_antenna_pix_ch2 = np.zeros(len(lsts))

    # get directions of pixels
    ras_pix, decs_pix = coord.vec2ang(healpy.pix2vec(n_side, range(npix)))

    print "Simulating noise local coordinates f = %.1f MHz" % (freq)
    Z0overZL = 120. * np.pi / 50.

    for i, lst in enumerate(lsts):
        alt, az = coord.eq2altaz(ras_pix, decs_pix, latitude, lst)  # transform equatorial coordinates to local coordinates

        # deselect directions below horizon
        mask = alt > 0
        zenith = 0.5 * np.pi - alt[mask]
        orientation_azimuth = magnetic_deviation

        if not datacorrection:
            VEL_theta_pix_ch2, VEL_phi_pix_ch2 = antenna_model_ew.get_antenna_response_vectorized(freq, zenith, az[mask] - orientation_azimuth)
            orientation_azimuth += np.pi * 0.5
            VEL_theta_pix_ch1, VEL_phi_pix_ch1 = antenna_model_ns.get_antenna_response_vectorized(freq, zenith, az[mask] - orientation_azimuth)

        else:
            VEL_theta_pix_ch2, VEL_phi_pix_ch2 = antenna_model_ew.get_antenna_response_datacorrected_vectorized(freq, zenith, az[mask] - orientation_azimuth)
            orientation_azimuth += np.pi * 0.5
            VEL_theta_pix_ch1, VEL_phi_pix_ch1 = antenna_model_ns.get_antenna_response_datacorrected_vectorized(freq, zenith, az[mask] - orientation_azimuth)

        magnitude_pix_ch1 = np.abs(VEL_theta_pix_ch1) ** 2 + np.abs(VEL_phi_pix_ch1) ** 2
        magnitude_pix_ch2 = np.abs(VEL_theta_pix_ch2) ** 2 + np.abs(VEL_phi_pix_ch2) ** 2
        noise_powers_antenna_pix_ch1_temp = Z0overZL * np.sum(magnitude_pix_ch1 * 0.5 * noisemap[mask] * \
                                                              np.cos(zenith)) * healpy.nside2pixarea(n_side)
        noise_powers_antenna_pix_ch2_temp = Z0overZL * np.sum(magnitude_pix_ch2 * 0.5 * noisemap[mask] * \
                                                              np.cos(zenith)) * healpy.nside2pixarea(n_side)
        noise_powers_antenna_pix_ch1[i] = noise_powers_antenna_pix_ch1_temp
        noise_powers_antenna_pix_ch2[i] = noise_powers_antenna_pix_ch2_temp

    # convert noise powers from [W/Hz] to [W/MHz]
    noise_powers_antenna_pix_ch1 *= 1e6
    noise_powers_antenna_pix_ch2 *= 1e6
    if output_folder is not None:
        np.savetxt(os.path.join(output_folder, "galactic_noise_simulation_pixel_%.1f.out" % (freq)),
                   np.c_[lsts, noise_powers_antenna_pix_ch1, noise_powers_antenna_pix_ch2],
                   '%1.4e', delimiter=' ', newline='\n',
                   header='Simulation of expected noise power in pixel sceme \n f = %.1f MHz \n lst[rad] - noise power ch1[W/MHz] - noise power ch2[W/MHz]' % (freq))

    return noise_powers_antenna_pix_ch1, noise_powers_antenna_pix_ch2


if __name__ == "__main__":
    # position of Kathy Turner https://www.auger.unam.mx/AugerWiki/AeraSysintPositions
    lat = np.deg2rad(-35.1104447)
    lon = np.deg2rad(-69.53775804)

    # antenna_model = "butterfly"
    antenna_model = "LPDA"

    # read in LFMap for specific ifrequency (in equatorial coordinates)
    freq = 45.5
    ifrequency = freq * 1e6
    lfmap_path = "/home/cglaser/software/LFmap/data/"
    filename = "/home/cglaser/software/LFmap/data/LFmap_%.1f_EQ.txt" % (ifrequency / 1e6)

    lsts = np.linspace(0, 2 * np.pi, 144)

    noise_powers_antenna_ch1, noise_powers_antenna_ch2 = galactic_noise(lat, freq, lsts,
                                                                              antenna_model, LFMap_path=lfmap_path,
                                                                              datacorrection=False)

    noise_powers_antenna_ch1_pix, noise_powers_antenna_ch2_pix = galactic_noise_pixel(lat, freq, lsts,
                                                                              antenna_model, LFMap_path=lfmap_path,
                                                                              datacorrection=False)

    import matplotlib.pyplot as plt

    a, b, c = np.loadtxt("galactic_noise_simulation_45.5.out", unpack=True)
    fig, ax = plt.subplots(1, 1)
    ax.plot(lsts * 12 / np.pi, b, "-r", label="channel 1")
    ax.plot(lsts * 12 / np.pi, c, "-b", label="channel 2")
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch1, "or", label="channel 1")
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch2, "ob", label="channel 2")
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch1_pix, "dr", label="channel 1")
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch2_pix, "db", label="channel 2")
    ax.legend()
    plt.tight_layout()

    fig, ax = plt.subplots(1, 1)
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch1_pix / noise_powers_antenna_ch1)
    ax.plot(lsts * 12 / np.pi, noise_powers_antenna_ch2_pix / noise_powers_antenna_ch2)

    plt.show()

























