import numpy as np
import xml.etree.ElementTree as ET
import os


def P2R(magnitude, phase):
    return magnitude * np.exp(1j * phase)


def interpolate_linear(x, x0, x1, y0, y1):
    if (x0 == x1):
        return y0
    return y0 + (y1 - y0) * (x - x0) / (x1 - x0)


def interpolate_linear_vectorized(x, x0, x1, y0, y1):
    x = np.array(x)
    mask = x0 != x1
    result = np.zeros_like(x, dtype=np.complex)
    denominator = x1 - x0
    result[mask] = y0[mask] + (y1[mask] - y0[mask]) * (x[mask] - x0[mask]) / denominator[mask]
    result[~mask] = y0[~mask]
    return result


def interpolate_linear_VEL(x, x0, x1, y0, y1):
    result = {}
    result['theta'] = interpolate_linear(x, x0, x1, y0['theta'], y1['theta'])
    result['phi'] = interpolate_linear(x, x0, x1, y0['phi'], y1['phi'])
    return result


class AntennaInterpolation():

    def __init__(self, antenna_model, path=None):
        if path is None:
            auger_offline_root = os.getenv("AUGEROFFLINEROOT")
            antenna_filename = os.path.join(auger_offline_root,
                                            "share/auger-offline/config/",
                                            antenna_model + ".xml")
        else:
            antenna_filename = os.path.join(path, antenna_model + ".xml")
        antenna_file = open(antenna_filename, "r")
        antenna_data = "<antenna>" + antenna_file.read() + "</antenna>"
        root = ET.fromstring(antenna_data)

        self.frequencies_node = root.find("./frequency")
        self.frequencies = np.array(self.frequencies_node.text.strip().split(), dtype=np.float)
        theta_node = root.find("./theta")
        thetas = np.deg2rad(np.array(theta_node.text.strip().split(), dtype=np.float))
        phi_node = root.find("./phi")
        phis = np.deg2rad(np.array(phi_node.text.strip().split(), dtype=np.float))

        self.frequency_lower_bound = self.frequencies[0]
        self.frequency_upper_bound = self.frequencies[-1]

        # determine theta angles
        self.n_theta = 0
        first_iteration = True
        theta_0 = thetas[0]
        self.theta_angles = []
        for i, theta in enumerate(thetas):
            if (first_iteration and theta == theta_0 and i != 0):
                self.n_theta = i
                first_iteration = False
            if (first_iteration):
                self.theta_angles.append(theta)
            else:  # check with remaining theta angles if theta angles were imported correctly
                if (self.theta_angles[i % self.n_theta] != theta):
                    print("error in importing zenith angles")
                    raise Exception("error in importing zenith angles")
        self.theta_angles = np.array(self.theta_angles)
        self.theta_lower_bound = self.theta_angles[0]
        self.theta_upper_bound = self.theta_angles[-1]

        # determine self.n_phi
        """ The size of the phi and theta list of the antenna-model xml file is the product of the
         * number of theta samples and the number of phi samples. Thus, knowing self.n_theta, self.n_phi can be
         * calculated.
         * */ """
        n_angles = len(phis)
        self.n_phi = n_angles / self.n_theta
        if (self.n_phi * self.n_theta != n_angles):
            print("error in determining number of azimuth samples")
            raise Exception("error in determining number of azimuth samples")

        # read in phi angles
        self.phi_angles = []
        self.phi_lower_bound = phis[0]
        self.phi_upper_bound = phis[-1]
        for iPhi in xrange(self.n_phi):
            self.phi_angles.append(phis[iPhi * self.n_theta])
        self.phi_angles = np.array(self.phi_angles)

        self.n_freqs = len(self.frequencies)

        theta_amps = np.zeros((self.n_freqs, n_angles))
        theta_phases = np.zeros((self.n_freqs, n_angles))
        phi_amps = np.zeros((self.n_freqs, n_angles))
        phi_phases = np.zeros((self.n_freqs, n_angles))

        for iFreq, freq in enumerate(self.frequencies):
            freq_string = "%.2f" % freq
            theta_amp_node = root.find("./EAHTheta_amp[@idfreq='%s']" % freq_string)
            if(theta_amp_node is None):
                freq_string = "%.1f" % freq
            theta_amp_node = root.find("./EAHTheta_amp[@idfreq='%s']" % freq_string)
            theta_amps[iFreq] = np.array(theta_amp_node.text.strip().split(), dtype=np.float)

            theta_phase_node = root.find("./EAHTheta_phase[@idfreq='%s']" % freq_string)
            theta_phases[iFreq] = np.deg2rad(np.array(theta_phase_node.text.strip().split(" "), dtype=np.float))

            phi_amp_node = root.find("./EAHPhi_amp[@idfreq='%s']" % freq_string)
            phi_amps[iFreq] = np.array(phi_amp_node.text.strip().split(), dtype=np.float)

            phi_phase_node = root.find("./EAHPhi_phase[@idfreq='%s']" % freq_string)
            phi_phases[iFreq] = np.deg2rad(np.array(phi_phase_node.text.strip().split(), dtype=np.float))
        VEL_thetas = P2R(theta_amps, theta_phases)
        VEL_phis = P2R(phi_amps, phi_phases)

        self.VEL = {}
        self.VEL_theta = np.zeros([self.n_freqs, self.n_theta, self.n_phi], dtype=np.complex)
        self.VEL_phi = np.zeros([self.n_freqs, self.n_theta, self.n_phi], dtype=np.complex)
        for iFreq, freq in enumerate(self.frequencies):
            for iPhi, phi in enumerate(self.phi_angles):
                for iTheta, theta in enumerate(self.theta_angles):
                    if (phi != phis[iPhi * self.n_theta + iTheta]):
                        print "phi angle has changed during theta loop"
                        print phi, self.phi_angles[iPhi * self.n_theta + iTheta]
                        raise Exception("phi angle has changed during theta loop")

                    VEL_phi = VEL_phis[iFreq][iPhi * self.n_theta + iTheta]
                    VEL_theta = VEL_thetas[iFreq][iPhi * self.n_theta + iTheta]
                    key = (iFreq, iTheta, iPhi)
                    self.VEL[key] = {}
                    self.VEL[key]['theta'] = VEL_theta
                    self.VEL[key]['phi'] = VEL_phi
                    self.VEL_theta[iFreq][iTheta][iPhi] = VEL_theta
                    self.VEL_phi[iFreq][iTheta][iPhi] = VEL_phi

    def get_antenna_response(self, freq, theta, phi):
        if(phi < 0):
            phi += 2 * np.pi
        if(phi >= 2 * np.pi):
            phi -= 2 * np.pi
        if ((freq < self.frequency_lower_bound or freq > self.frequency_upper_bound) or (phi < self.phi_lower_bound or phi > self.phi_upper_bound) or (theta < self.theta_lower_bound or theta > self.theta_upper_bound)):
            print "theta lower bound", self.theta_lower_bound, theta, self.theta_upper_bound
            print "phi lower bound", self.phi_lower_bound, phi, self.phi_upper_bound
            print "freq lower bound", self.frequency_lower_bound, freq, self.frequency_upper_bound
            print "theta, phi or frequency out of range, returning (0,0j)"
            print freq, self.frequency_lower_bound, self.frequency_upper_bound
            return (0, 0)

        iTheta_lower = int(np.floor((theta - self.theta_lower_bound) / (self.theta_upper_bound - self.theta_lower_bound) * (self.n_theta - 1)))
        theta_lower = self.theta_angles[iTheta_lower]
        iTheta_upper = int(np.ceil((theta - self.theta_lower_bound) / (self.theta_upper_bound - self.theta_lower_bound) * (self.n_theta - 1)))
        theta_upper = self.theta_angles[iTheta_upper]
        iPhi_lower = int(np.floor((phi - self.phi_lower_bound) / (self.phi_upper_bound - self.phi_lower_bound) * (self.n_phi - 1)))
        phi_lower = self.phi_angles[iPhi_lower]
        iPhi_upper = int(np.ceil((phi - self.phi_lower_bound) / (self.phi_upper_bound - self.phi_lower_bound) * (self.n_phi - 1)))
        phi_upper = self.phi_angles[iPhi_upper]
        iFrequency_lower = int(np.floor((freq - self.frequency_lower_bound) / (self.frequency_upper_bound - self.frequency_lower_bound) * (self.n_freqs - 1)))
        frequency_lower = self.frequencies[iFrequency_lower]
        iFrequency_upper = int(np.ceil((freq - self.frequency_lower_bound) / (self.frequency_upper_bound - self.frequency_lower_bound) * (self.n_freqs - 1)))
        frequency_upper = self.frequencies[iFrequency_upper]
        # lower frequency bound
        # theta low
        VEL_freq_low_theta_low = interpolate_linear_VEL(
            phi, phi_lower, phi_upper,
            self.VEL[(iFrequency_lower, iTheta_lower, iPhi_lower)],
            self.VEL[(iFrequency_lower, iTheta_lower, iPhi_upper)])

        # theta up
        VEL_freq_low_theta_up = interpolate_linear_VEL(
            phi, phi_lower, phi_upper,
            self.VEL[(iFrequency_lower, iTheta_upper, iPhi_lower)],
            self.VEL[(iFrequency_lower, iTheta_upper, iPhi_upper)])

        VEL_freq_low = interpolate_linear_VEL(theta, theta_lower, theta_upper,
                                              VEL_freq_low_theta_low, VEL_freq_low_theta_up)

        # upper frequency bound
        # theta low
        VEL_freq_up_theta_low = interpolate_linear_VEL(
            phi, phi_lower, phi_upper,
            self.VEL[(iFrequency_upper, iTheta_lower, iPhi_lower)],
            self.VEL[(iFrequency_upper, iTheta_lower, iPhi_upper)])

        # theta up
        VEL_freq_up_theta_up = interpolate_linear_VEL(
            phi, phi_lower, phi_upper,
            self.VEL[(iFrequency_upper, iTheta_upper, iPhi_lower)],
            self.VEL[(iFrequency_upper, iTheta_upper, iPhi_upper)])

        VEL_freq_up = interpolate_linear_VEL(theta, theta_lower, theta_upper,
                                             VEL_freq_up_theta_low, VEL_freq_up_theta_up)

        interpolated_VEL = interpolate_linear_VEL(freq, frequency_lower, frequency_upper,
                                                  VEL_freq_low, VEL_freq_up)
        return interpolated_VEL

    def get_antenna_response_vectorized(self, freq, theta, phi):
        phi[phi < 0] += 2 * np.pi
        phi[phi >= 2 * np.pi] -= 2 * np.pi
        if((np.sum(freq < self.frequency_lower_bound) or np.sum(freq > self.frequency_upper_bound)) or
           (np.sum(phi < self.phi_lower_bound) or np.sum(phi > self.phi_upper_bound)) or
           (np.sum(theta < self.theta_lower_bound) or np.sum(theta > self.theta_upper_bound))):
            print "theta lower bound", self.theta_lower_bound, theta, self.theta_upper_bound
            print "phi lower bound", self.phi_lower_bound, phi, self.phi_upper_bound
            print "freq lower bound", self.frequency_lower_bound, freq, self.frequency_upper_bound
            print "theta, phi or frequency out of range, returning (0,0j)"
            print freq, self.frequency_lower_bound, self.frequency_upper_bound
            return (0, 0)

        iTheta_lower = np.array(np.floor((theta - self.theta_lower_bound) / (self.theta_upper_bound - self.theta_lower_bound) * (self.n_theta - 1)), dtype=np.int)
        theta_lower = self.theta_angles[iTheta_lower]
        iTheta_upper = np.array(np.ceil((theta - self.theta_lower_bound) / (self.theta_upper_bound - self.theta_lower_bound) * (self.n_theta - 1)), dtype=np.int)
        theta_upper = self.theta_angles[iTheta_upper]
        iPhi_lower = np.array(np.floor((phi - self.phi_lower_bound) / (self.phi_upper_bound - self.phi_lower_bound) * (self.n_phi - 1)), dtype=np.int)
        phi_lower = self.phi_angles[iPhi_lower]
        iPhi_upper = np.array(np.ceil((phi - self.phi_lower_bound) / (self.phi_upper_bound - self.phi_lower_bound) * (self.n_phi - 1)), dtype=np.int)
        phi_upper = self.phi_angles[iPhi_upper]
        iFrequency_lower = np.array(np.floor((freq - self.frequency_lower_bound) / (self.frequency_upper_bound - self.frequency_lower_bound) * (self.n_freqs - 1)), dtype=np.int)
        frequency_lower = self.frequencies[iFrequency_lower]
        iFrequency_upper = np.array(np.ceil((freq - self.frequency_lower_bound) / (self.frequency_upper_bound - self.frequency_lower_bound) * (self.n_freqs - 1)), dtype=np.int)
        frequency_upper = self.frequencies[iFrequency_upper]
        # lower frequency bound
        # theta low
        VELt_freq_low_theta_low = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_theta[iFrequency_lower, iTheta_lower, iPhi_lower],
            self.VEL_theta[iFrequency_lower, iTheta_lower, iPhi_upper])
        VELp_freq_low_theta_low = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_phi[iFrequency_lower, iTheta_lower, iPhi_lower],
            self.VEL_phi[iFrequency_lower, iTheta_lower, iPhi_upper])

        # theta up
        VELt_freq_low_theta_up = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_theta[iFrequency_lower, iTheta_upper, iPhi_lower],
            self.VEL_theta[iFrequency_lower, iTheta_upper, iPhi_upper])
        VELp_freq_low_theta_up = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_phi[iFrequency_lower, iTheta_upper, iPhi_lower],
            self.VEL_phi[iFrequency_lower, iTheta_upper, iPhi_upper])

        VELt_freq_low = interpolate_linear_vectorized(theta, theta_lower,
                                                      theta_upper,
                                                      VELt_freq_low_theta_low,
                                                      VELt_freq_low_theta_up)
        VELp_freq_low = interpolate_linear_vectorized(theta, theta_lower,
                                                      theta_upper,
                                                      VELp_freq_low_theta_low,
                                                      VELp_freq_low_theta_up)

        # upper frequency bound
        # theta low
        VELt_freq_up_theta_low = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_theta[iFrequency_upper, iTheta_lower, iPhi_lower],
            self.VEL_theta[iFrequency_upper, iTheta_lower, iPhi_upper])
        VELp_freq_up_theta_low = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_phi[iFrequency_upper, iTheta_lower, iPhi_lower],
            self.VEL_phi[iFrequency_upper, iTheta_lower, iPhi_upper])

        # theta up
        VELt_freq_up_theta_up = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_theta[iFrequency_upper, iTheta_upper, iPhi_lower],
            self.VEL_theta[iFrequency_upper, iTheta_upper, iPhi_upper])
        VELp_freq_up_theta_up = interpolate_linear_vectorized(
            phi, phi_lower, phi_upper,
            self.VEL_phi[iFrequency_upper, iTheta_upper, iPhi_lower],
            self.VEL_phi[iFrequency_upper, iTheta_upper, iPhi_upper])

        VELt_freq_up = interpolate_linear_vectorized(theta, theta_lower, theta_upper,
                                                     VELt_freq_up_theta_low,
                                                     VELt_freq_up_theta_up)
        VELp_freq_up = interpolate_linear_vectorized(theta, theta_lower, theta_upper,
                                                     VELp_freq_up_theta_low,
                                                     VELp_freq_up_theta_up)

        interpolated_VELt = interpolate_linear(freq, frequency_lower,
                                               frequency_upper,
                                               VELt_freq_low,
                                               VELt_freq_up)
        interpolated_VELp = interpolate_linear(freq, frequency_lower,
                                               frequency_upper,
                                               VELp_freq_low,
                                               VELp_freq_up)
        return interpolated_VELt, interpolated_VELp

    def get_antenna_response_datacorrected_vectorized(self, freq, theta, phi):
        import inspect
        path = os.path.dirname(inspect.getfile(inspect.currentframe()))
        freqs, zeniths, factor = np.loadtxt(os.path.join(path, "SimMeasRatio-LPDA-EW.txt"), unpack=True)
        zeniths = np.deg2rad(zeniths)
        freqs_unique = np.unique(freqs)
        zeniths_unique = np.unique(zeniths)
        factors = np.ones_like(theta)
        for i in xrange(len(theta)):
            if (theta[i] > np.deg2rad(70)):
                continue
            f = freqs_unique[np.argmin(np.abs(freqs_unique - freq))]
            t = zeniths_unique[np.argmin(np.abs(zeniths_unique - theta[i]))]
            factors[i] = factor[(freqs == f) & (zeniths == t)]
        tmp = self.get_antenna_response_vectorized(freq, theta, phi)
        return tmp[0], tmp[1] * factors

if __name__ == "__main__":

    magnetic_deviation = -0.0607375

    small_black_spider = AntennaInterpolation("SmallBlackSpider_ground2")

    # for east-west antenna do
    freq = 35.
    theta = 0.1
    phi = 0.8
    orientation_azimuth = magnetic_deviation
    print "antenna response (SmallBlackSpider East-west) for freq %.1f MHz, theta %.1f deg, phi %.1f deg is:" % (freq, np.rad2deg(theta), np.rad2deg(phi))
    print small_black_spider.get_antenna_response(freq, theta, phi - orientation_azimuth)

    # for north-south antenna do
    orientation_azimuth = magnetic_deviation + np.pi / 2
    print "antenna response (SmallBlackSpider north-south) for freq %.1f MHz, theta %.1f deg, phi %.1f deg is:" % (freq, np.rad2deg(theta), np.rad2deg(phi))
    print small_black_spider.get_antenna_response(freq, theta, phi - orientation_azimuth)

    # for east-west antenna do
    butterfly_south = AntennaInterpolation("Butterfly_ground2_East")
    orientation_azimuth = magnetic_deviation
    print "antenna response (Butterly East-west) for freq %.1f MHz, theta %.1f deg, phi %.1f deg is:" % (freq, np.rad2deg(theta), np.rad2deg(phi))
    print butterfly_south.get_antenna_response(freq, theta, phi - orientation_azimuth)

    butterfly_north = AntennaInterpolation("Butterfly_ground2_North")
    # for north-south antenna do
    orientation_azimuth = magnetic_deviation + np.pi / 2
    print "antenna response (Butterly north-south) for freq %.1f MHz, theta %.1f deg, phi %.1f deg is:" % (freq, np.rad2deg(theta), np.rad2deg(phi))
    print butterfly_north.get_antenna_response(freq, theta, phi - orientation_azimuth)

    for theta in np.deg2rad(np.arange(0, 45, 3)):
        for phi in np.deg2rad(np.arange(0, 7.5, 7.5)):
            VEL = small_black_spider.get_antenna_response(freq, theta, phi)
            print("%.0f %.0f %.2f" % (np.rad2deg(theta), np.rad2deg(phi), abs(VEL['theta'])))

    thetas = np.linspace(0, np.pi * 0.5, 90)
    phis = np.ones(90) * np.pi * 0.2
    tmp = small_black_spider.get_antenna_response_vectorized(freq, thetas, phis)
    VELphi = np.abs(tmp[1])  # magnitude of phi component

    tmp = small_black_spider.get_antenna_response_datacorrected_vectorized(freq, thetas, phis)
    VELphi2 = np.abs(tmp[1])  # magnitude of phi component





#     import matplotlib.pyplot as plt
#     import mpl_toolkits.axisartist.floating_axes as floating_axes
#     from matplotlib.transforms import Affine2D
#     import mpl_toolkits.axisartist.angle_helper as angle_helper
#     from matplotlib.projections import PolarAxes
#     from mpl_toolkits.axisartist.grid_finder import FixedLocator, MaxNLocator, DictFormatter
#     fig = plt.figure(1, figsize=(8, 4))
#
#     tr_scale = Affine2D().scale(np.pi / 180., 1.)
#
#     tr = tr_scale + PolarAxes.PolarTransform()
#     # tr = PolarAxes.PolarTransform()
#
#     grid_locator1 = angle_helper.LocatorHMS(4)
#     tick_formatter1 = angle_helper.FormatterDMS()
#
#     grid_locator1 = FixedLocator(np.arange(0, 90, 15.)[::-1])
#     grid_locator2 = MaxNLocator(9)
#
#     ra0, ra1 = 0, 90
#     cz0, cz1 = 0, 13.5
#     grid_helper = floating_axes.GridHelperCurveLinear(tr,
#                                         extremes=(ra0, ra1, cz0, cz1),
#                                         grid_locator1=grid_locator1,
#                                         grid_locator2=grid_locator2,
#                                          tick_formatter1=tick_formatter1,
#                                         # tick_formatter2=None,
#                                         )
#
#     ax1 = floating_axes.FloatingSubplot(fig, 111, grid_helper=grid_helper)
#     fig.add_subplot(ax1)
#
#     # adjust axis
#     ax1.axis["left"].set_axis_direction("bottom")
#     ax1.axis["right"].set_axis_direction("top")
#
#     ax1.axis["bottom"].set_visible(False)
#     ax1.axis["top"].set_axis_direction("bottom")
#     ax1.axis["top"].toggle(ticklabels=True, label=True)
#     ax1.axis["top"].major_ticklabels.set_axis_direction("top")
#     ax1.axis["top"].label.set_axis_direction("top")
#
#     ax1.axis["right"].label.set_text(r"VEL")
#     ax1.axis["top"].label.set_text(r"zenith [deg]")
#
#     # create a parasite axes whose transData in RA, cz
#     aux_ax = ax1.get_aux_axes(tr)
#
#     aux_ax.patch = ax1.patch  # for aux_ax to have a clip path as in ax
#     ax1.patch.zorder = 0.9  # but this has a side effect that the patch is
#                         # drawn twice, and possibly over some other
#                         # artists. So, we decrease the zorder a bit to
#                         # prevent this.
#
#
#     aux_ax.scatter(np.rad2deg(np.pi * 0.5 - thetas), VELphi)
#
#     tmp = small_black_spider.get_antenna_response_vectorized(55, thetas, phis)
#     aux_ax.scatter(np.rad2deg(np.pi * 0.5 - thetas), np.abs(tmp[1]))
#     plt.show()
#
#     fig, ax = plt.subplots()
#     ax.plot(thetas, np.abs(tmp[0]))
#
#
#
#







